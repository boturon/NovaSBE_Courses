# Articles 34-37

Free Movement of Goods

Whereas Article 34 will apply to discriminatory provisions and also to indistinctly applicable measures, Article 35 will, it seems, apply only if there is discrimination.

# Article 288

The principal types of EU legal act are set out in Article 288 TFEU, and were analysed in an earlier chapter. All binding forms of EU law are capable of direct effect, and while other types of non-binding law are not said to have direct effect, they are influential in other ways and may have what has become known as indirect effect.

# Articles:
 - 267 - ECJ has jurisdiction
 - 258 - Commission sue MS
 - 250 - MS vs MS
 - 28-30 - quotas
 - 34-37 - MEQRs
