# Direct Effect

## Van Gend en Loos (Case 26/62) [1963] ECR 13

Van Gend en Loos was a ground-breaking judgment. The strong interventions made on behalf of three governments, half of the existing Member States, indicated that the concept of direct effect, understood as the immediate enforceability by individual applicants of those provisions in national courts, probably did not accord with the understanding of those states of the obligations they assumed when they created the EEC. They argued that international treaties were really just a compact between states and did not give rise to rights that individuals could enforce in national courts. They also contended that the sole method of enforcing Community law was through an action under what is now Article 258 TFEU, brought by the Commission. The ECJ nonetheless held that Treaty Articles could in principle have direct effect. [pp 183]

****

# Directives

## Pubblico Ministero v Tullio Ratti (Case 148/78) [1979] ECR 1629

The ECJ sought to promote the legal effectiveness of directives even in the absence of their implementation. It held that directives could in principle have direct effect. It gave three reasons for this, two in Van Duyn, and the third in Ratti. The third rationale, articulated in the Ratti case, is the estoppel argument: Member States were precluded by their failure to implement a directive properly from refusing to recognize its binding effect in cases where it was pleaded against them. Thus, the argument is as follows. The Member State should have implemented the directive. If it had done so the individual would have been able to rely on the national implementing law. The Member State had committed a wrong by failing to implement the directive, and could not rely on that wrongdoing so as to deny the binding effect of the directive itself after the date for implementation. Where necessary, a conflicting national law should be disapplied. [pp 193]

*a) take note of what the citizen can do when a deadline elapses and the State does NOT transpose a directive into national law*
*b) take note of what a citizen must NOT do BEFORE the deadline elapses for the State to transpose a Directive into national Law)*


## Becker v Finanzamt Münster-Innenstadt (Case 8/81) [1982] ECR 53

The general principle is that the direct effect of a directive operates from the deadline specified for implementation of the directive. [pp 194]

The Court has accordingly held that provisions of a directive could be relied on against tax authorities (the judgments in Case 8/81 Becker [1982] ECR 53). [pp 197]

*take note that a MS MAY request an EXTENSION of the time-limit to implement a Directive)*


## Dori v Recreb Srl (Case C–91/92) [1994] ECR I–3325

20. As the Court has consistently held since its judgment in Case 152/84 Marshall v Southampton and South-West Hampshire Health Authority [1986] ECR 723, paragraph 48, a directive cannot of itself impose obligations on an individual and cannot therefore be relied upon as such against an individual. [paragraph 20]

The ECJ has nonetheless continued to insist that directives should only have vertical direct effect, despite widespread academic criticism and numerous opinions given by Advocates General in favour of horizontal direct effect. The ruling in Marshall was confirmed ten years later in the Dori case. The vertical/horizontal distinction requires the ECJ to delineate what is to be regarded as the ‘state’ for these purposes. The ECJ has taken a broad view of the ‘state’ against which directives could be enforced. The Foster case remains the primary ruling. [pp 197]

The ECJ has consistently refused to depart from the clear Marshall/Dori rulings that a directive cannot be invoked by an individual so as to impose a direct obligation on another individual. [pp 210]

****

# Broad Concept of State

## Marshall v Southampton and South-West Hampshire Area Health Authority (Teaching) (Case 152/84) [1986] ECR 723

The ECJ had thus far expanded direct effect. In Marshall it however held that the direct effect of a directive could be pleaded against the state, but not against an individual. [pp 194]

Helen Marshall was dismissed after 14 years’ employment by the respondent health authority on the ground that she had passed 60, and the Authority’s policy required female employees to retire at 60 and male employees at 65. National legislation imposed no obligation on women to retire at 60, but neither did it prohibit employers from discriminating on grounds of sex in retirement matters. Marshall argued however that her dismissal violated the 1976 Equal Treatment Directive, and the national court asked whether she could rely on the Directive against the Health Authority. Advocate General Slynn suggested that to give ‘horizontal effect’ to directives by allowing them to impose obligations directly on an individual would ‘totally blur the distinction between directives and regulations’ established by the Treaty. [pp 195]

48. With regard to the argument that a directive may not be relied upon against an individual, it must be emphasized that according to Article 189 of the EEC Treaty, the binding nature of a directive, which constitutes the basis for the possibility of relying on the direcitve before a national court, exists only in relation to ‘each Member State to which it is addressed’. It follows that a directive may not of itself impose obligations on an individual and that a provision of a directive may not be relied upon as such against such a person. [paragraph 48]


## Foster v British Gas plc (Case C–188/89) [1990] ECR I–3313

The vertical/horizontal distinction requires the ECJ to delineate what is to be regarded as the ‘state’ for these purposes. The ECJ has taken a broad view of the ‘state’ against which directives could be enforced. The Foster case remains the primary ruling. [pp 197]

The plaintiffs were employed by British Gas, whose policy it was to require women to retire at 60 and men at 65. British Gas was at the time a nationalized industry with responsibility for and a monopoly of the gas-supply system in Great Britain. The plaintiffs sought to rely on the provisions of the 1976 Equal Treatment Directive and the House of Lords asked the ECJ whether British Gas was a body of the kind against which the provisions of the Directive could be invoked. [pp 197]

20. It follows from the foregoing that a body, whatever its legal form, which has been made responsible, pursuant to a measure adopted by the State, for providing a public service under the control of the State and has for that purpose special powers beyond those which result from the normal rules applicable in relations between individuals, is included in any event among the bodies against which the provisions of a directive capable of having direct effect may be relied upon. [paragraph 20]

It is evident from paragraph 20 that the Court considered a company in the position of British Gas to be an organ of the state. However, it is not entirely clear what kind of control the state must have over a body for it to be part of the state. Foster provides no authoritative definition, but merely indicates that a body which has been made responsible for providing a public service under the control of the state is included within the EU definition of a public body. Subsequent case law initially left it in the hands of national courts to apply the loose criteria articulated in Foster, but the ECJ in other cases ruled that a particular body clearly satisfied those criteria for the purposes of invoking a directive against it. [pp 197]

****

# Free Movement of Goods

## Procureur du Roi v Dassonville (Case 8/74) [1974] ECR 837

The seminal early judicial decision on the interpretation of measures having equivalent effect (MEQRs) is Dassonville. [pp 639]

Belgian law provided that goods bearing a designation of origin could only be imported if they were accompanied by a certiﬁcate from the government of the exporting country certifying their right to such a designation. Dassonville imported Scotch whisky into Belgium from France without being in possession of the certiﬁcate from the British authorities. The certificate would have been very difficult to obtain in respect of goods which were already in free circulation in a third country, as in this case. Dassonville was prosecuted in Belgium and argued by way of defence that the Belgian rule constituted an MEQR. [pp 639]

Two aspects of the ECJ’s reasoning should be noted. First, it is clear from paragraph 5 that the crucial element in proving an MEQR is its effect: a discriminatory intent is not required. The ECJ takes a broad view of measures that hinder the free flow of goods, and the definition does not even require that the rules actually discriminate between domestic and imported goods. Dassonville thus sowed the seeds which bore fruit in Cassis de Dijon, where the ECJ decided that Article 34 could apply to rules which were not discriminatory. Secondly, the ECJ indicates, in paragraph 6, that reasonable restraints may not be caught by Article 34. This is the origin of what became known as the 'rule of reason'.

5. All trading rules enacted by member states which are capable of hindering, directly or indirectly, actually or potentially, intra-community trade are to be considered as measures having an effect equivalent to quantitative restrictions. [paragraph 5]


## Rewe-Zentrale AG v Bundesmonopolverwaltung für Branntwein (Cassis de Dijon) (Case 120/78) [1979] ECR 649

The possibility that Article 34 could be applied to indistinctly applicable rules was also apparent in Dassonville. The definition of an MEQR in paragraph 5 did not require a measure to be discriminatory. The seeds that were sown in Directive 70/50 and Dassonville came to fruition in the seminal Cassis de Dijon case. [pp 647]

The applicant intended to import the liqueur 'Cassis de Dijon' into Germany from France. The German authorities refused to allow the importation because the French drink was not of sufficient alcoholic strength to be marketed in Germany. Under German law such liqueurs had to have an alcohol content of 25 per cent, whereas the French drink had an alcohol content of between 15 and 20 per cent. The applicant argued that the German rule was an MEQR, since it prevented the French version of the drink from being lawfully marketed in Germany. [pp 647]

The significance of Cassis de Dijon can hardly be overstated, and it is therefore worth dwelling upon the result and the reasoning. In terms of result the Court’s ruling in Cassis affirmed and developed the Dassonville judgment. It affirmed paragraph 5 of Dassonville: what is now Article 34 could apply to national rules that did not discriminate against imported products, but which inhibited trade because they were different from the trade rules applicable in the country of origin. The fundamental assumption was that when goods had been lawfully marketed in one Member State, they should be admitted into any other state without restriction, unless the state of import could successfully invoke one of the mandatory requirements. The Cassis judgment encapsulated therefore a principle of mutual recognition, paragraph 14(4). [pp 649]

A distinction can however be drawn, as Weatherill and Beaumont note, between what may be termed dual-burden rules and equal-burden rules. Cassis is concerned with dual-burden rules. State A imposes rules on the content of goods. These are applied to goods imported from state B, even though such goods have already complied with the trade rules in state B. Cassis prevents state A from imposing its rules in such instances unless they can be saved by the mandatory requirements. Equal-burden rules are those applying to all goods, irrespective of origin, which regulate trade in some manner. They are not designed to be protectionist. These rules may have an impact on the over- all volume of trade, but there will be no greater impact for imports than for domestic products. [pp 651]


## Keck and Mithouard (Cases C–267 & 268/91) [1993] ECR I–6097

Keck and Mithouard (K & M) were prosecuted in the French courts for selling goods at a price which was lower than their actual purchase price (resale at a loss), contrary to a French law of 1963 as amended in 1986. The law did not ban sales at a loss by the manufacturer. K & M claimed that the French law was contrary to Community law concerning free movement of goods. [pp 654]

It is clear that the decision was based in part upon the distinction between dual-burden rules and equal-burden rules (paragraphs 15 to 17). Cassis-type rules relating to the goods themselves were within Article 34 because they would have to be satisfied by the importer in addition to any such provisions existing within its own state (paragraph 15). Such rules were by their very nature likely to impede access to the market for imported goods. Rules concerning selling arrangements, by way of contrast, imposed an equal burden on all those seeking to market goods in a particular territory (paragraph 17). They did not impose extra costs on the importer, their purpose was not to regulate trade (paragraph 12), and they did not prevent access to the market. They were therefore not within Article 34, provided that they affected in the same manner in law or fact domestic and imported goods (paragraph 16). [pp 655]

40. In Keckk at paragraph 16, the Court held that national measures restricting or prohibiting certain selling arrangements are not covered by Article 30 . . . so long as they apply to all traders operating within the national territory and as long as they affect in the same manner, in law and fact, the marketing of domestic products and of those from other Member States. [pp 657]

## Schwarz v Burgermeister der Landeshauptstadt Salzburg (Case C–366/04) [2005] ECR I–10139

asdfs

****

#  Pasta Case-Drei Glocken & Reverse Discrimination

## Drei Glocken v USL Centro-Sud (Case 407/85) [1988] ECR 4233

There were numerous cases applying Cassis to various trade rules.

## Reverse Discrimination

Conversely, nationals who have never exercised the freedom to move within the EU will have no EU law claim against their state. This gives rise to the curious phenomenon of ‘reverse discrimination’ whereby nationals of a Member State find themselves disadvantaged by comparison with other EU nationals within the same Member State. In the Belgian Social Security case, the Flemish Government, a federated entity of the Belgian state, had enacted a scheme of care insurance that was available only to those working and residing in either the Dutch-speaking region or the bilingual region of Brussels- Capital. The ECJ ruled that this constituted a restriction under Articles 45 and 59 TFEU, since ‘migrant workers, pursuing or contemplating the pursuit of employment or self-employment in one of those two regions, might be dissuaded from making use of their freedom of movement and from leaving their Member State of origin to stay in Belgium, by reason of the fact that moving to certain parts of Belgium would cause them to lose the opportunity of eligibility for the benefits which they might otherwise have claimed’. The ECJ insisted that any EU national working in either of these two regions must be eligible for the scheme, regardless of where in Belgium they resided, with the exception of Belgian nationals living in the French- or German-speaking region who had never exercised their freedom to move. The Court ruled that EU law ‘clearly cannot be applied to such purely internal situations’.88 The consequence is that a Spanish national establishing herself in the French-speaking region and working in the Dutch-speaking region will be able to join the insurance scheme, while her Belgian colleague and neighbor, who has worked and lived all his life in Belgium, will not. [pp 778]

****

# Rules of Entry and Exit to and from the EU

## Entry - Article 49

Any European State which respects the values referred to in Article 2 and is committed to promoting them may apply to become a member of the Union. The European Parliament and national Parliaments shall be notified of this application. The applicant State shall address its application to the Council, which shall act unanimously after consulting the Commission and after receiving the consent of the European Parliament, which shall act by a majority of its component members. The conditions of eligibility agreed upon by the European Council shall be taken into account.

The conditions of admission and the adjustments to the Treaties on which the Union is founded, which such admission entails, shall be the subject of an agreement between the Member States and the applicant State. This agreement shall be submitted for ratification by all the contracting States in accordance with their respective constitutional requirements.


## Exit - Article 50

1. Any Member State may decide to withdraw from the Union in accordance with its own constitutional requirements.
2. A Member State which decides to withdraw shall notify the European Council of its intention. In the light of the guidelines provided by the European Council, the Union shall negotiate and conclude an agreement with that State, setting out the arrangements for its withdrawal, taking account of the framework for its future relationship with the Union. That agreement shall be negotiated in accordance with Article 218(3) of the Treaty on the Functioning of the European Union. It shall be concluded on behalf of the Union by the Council, acting by a qualified majority, after obtaining the consent of the European Parliament.
3. The Treaties shall cease to apply to the State in question from the date of entry into force of the withdrawal agreement or, failing that, two years after the notification referred to in paragraph 2, unless the European Council, in agreement with the Member State concerned, unanimously decides to extend this period.
4. For the purposes of paragraphs 2 and 3, the member of the European Council or of the Council representing the withdrawing Member State shall not participate in the discussions of the European Council or Council or in decisions concerning it. A qualified majority shall be defined in accordance with Article 238(3)(b) of the Treaty on the Functioning of the European Union.
5. If a State which has withdrawn from the Union asks to rejoin, its request shall be subject to the procedure referred to in Article 49.
