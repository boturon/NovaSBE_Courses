
# EXAM

## Direct effect and no horizontal direct effect

> What is the difference between vertical direct effect and horizontal direct effect?

i. The doctrine of ‘direct effect’ applies in principle to all binding EU law including the Treaties, sec- ondary legislation, and international agreements. The most problematic issues concern directives and international agreements.

ii. The meaning of direct effect remains contested. In a broad sense it means that provisions of binding EU law which are sufficiently clear, precise, and unconditional to be considered justiciable can be invoked and relied on by individuals before national courts. There is also a ‘narrower’ or classical concept of direct effect, which is defined in terms of the capacity of a provision of Union law to confer rights on individuals.

iii. While directives can be enforced directly by individuals against the state after the time limit for their implementation has expired (vertical direct effect), resulting where necessary in the disapplication of conflicting domestic law, the ECJ has ruled that they cannot of themselves impose obligations on individuals (no horizontal direct effect). The rationale for this limitation on direct effect of directives is however contestable.

[pp180]

## Directives
