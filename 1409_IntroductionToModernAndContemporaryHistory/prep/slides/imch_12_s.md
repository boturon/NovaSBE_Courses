*The end of openness: World War I*

Main topics:
- World War I interrupted the globalisation process of the second half of the nineteenth century
- Its impact would be lasting: because of the war itself, but then also for being at the origin of the first communist state ever, in Russia, and also for being at the origin of the fascist regimes in response to communism, something that would lead to World War II
- The aftermath of World War II would lead to the expansion of communism around the world – so, the origin of it all is World War I
- Contingency played again a great role in the outbreak of the war, meaning that things could have happened otherwise.

Showing again that nothing is predetermined, the seemingly unstoppable movement in the direction of ever more inclusive political and economic institutions was brutally interrupted with the outbreak of World War I in 1914. How the war started also shows the strength of contingency, for the events leading to its outbreak could have not happened.

Two elements played a crucial role in the process:
- The dissatisfaction of certain people with economic growth and globalisation
- The radical changes in the international relation of forces in Europe, leading to the expansion of nationalism.

Starting with economic growth and openness: even if it is true that both processes bring aggregate prosperity, they also bring discontent to certain sections of society – people whose activities become obsolete thanks to growth or are replaced by foreign goods or services.

The question is if they are strong enough to influence the political process in such a way as to hamper the process of growth or to close frontiers.

Moving to changes in international relations in Europe, some decisive events took place in the second half of the nineteenth century: perhaps the most important was the unification of Germany in 1871: with it, a new geopolitical reality was born that was very difficult to accomodate in the existing balance of power in Europe.


# Germany

Germany would enter the process of unification during the 1860s. The engine of German unification would be Prussia, under king William I, of the House of Hohenzollern, crucially assisted by his Chancellor, Otto von Bismarck; the process would culminate in the proclamation of the II German Empire (Reich) in 1871. Nationalism was the big idea behind German unification: the idea was that it did not make sense for people that felt they belonged to the same national group to be divided in different political units. The creation of the German Empire was probably the most important geopolitical event in nineteenth century Europe. Germany became soon the most important country in continental Europe, giving rise to the so-called “German problem”, which would dominate Europe during the nineteenth and twentieth centuries.


# Austro-Hungarian Empire

Also worth mentioning is the case of the Austro-Hungarian Empire, just because it was there where the war started. What happened in the Austrian Empire was the opposite of what happened in Germany. Nationalism was at work here as well, but in a different way. The nationalistic revolts (most important among them the Czech and Hungarian ones) within the empire led to the concession of increased autonomy and even to the creation of the Dual Monarchy in 1867, joining the Austrian and the Hungarian crowns. The result was the creation of the Austro-Hungarian Empire, a quite anomalous entity in the nationalistic environment of the second half of the nineteenth century in Europe: eleven nationalities existed within it, many of them with strong nationalistic tendencies.

****

In the decades prior to the war, international tensions in Europe followed a more or less clear pattern:
- Germany had become a huge European power and wanted to become a great imperial overseas power, threatening British domination
- France felt threatened by Germany but was unable to fight it. So, it coalised with Russia and tried to seduce Britain
- As for Britain it kept an ambiguous position, moving from ally to ally in order to keep the balance of power in its favour: in one moment or another, Germany, France and Russia were Britain’s allies.

The power of contingency was clear in the way the war started; the trigger was a dramatic but still minor event, if we think of its enormous consequences: the killing of the heir to the throne of the Austro-Hungarian Empire by a Serb terrorist organization, in July 1914. Suspecting that the kingdom of Serbia was involved in the assassination, the empire attacked Serbia. This put in movement the diplomatic alliances existing in Europe, again in a quite contingente manner. Soon, this minor event had drawn the most important European countries into a general war.

This was a most paradoxical war: it was particularly brutal but, at the same time, quite tedious in strategical terms - despite major battles, little strategic progress was made by the contenders until the end of the conflict. And it ended with no clear military defeat of Germany.

The war evolved through three main phases:
1. The 1914 German offensive against France and its failure;
2. The spreading of the war to the peripheral theaters of conlict
3. The withdrawal of Russia and the entry of the US in the war in 1917, leading to the end of the conflict.

The two most consequential events of the war were two: the US’s entry in the war and the Soviet Revolution in Russia.

The US entered the war thanks to the pressure of President Woodrow Wilson, contradicting the traditional American position of not getting involved in European affairs; President Wilson thought the conflict between the Europeans was too painful to watch. His idea was to end the war and create a new framework for international relations in Europe. The US lacked a motive, however. But that motive appeared when Germany supported a Mexican bandit and terrorist that threatened American interests in Mexico: Pancho Villa.

As for the Soviet Revolution in Russia, it played in favour of Germany, since the Soviet programme implied the withdrawal of Russia from the war, which eventually took place in March 1918, with the signing of the Brest-Litovsk Treaty between Germany and Soviet Russia.

Again, there was no preordained communist victory in Russia, but it happened – and thanks to it, the world would change forever. As Germany proved unable to win decisively even under those circumstances, the presence of the US on the side of the allies made the German army believe that it would never win the war. So, Germany surrendered in November 1918. The important American presence allowed President Wilson to suggest a strong framework for peace. This framework was given in his famous Fourteen Points, of which the most important were the definition of the frontiers of Europe according to the principles of nationality and ethnic homogeneity; also important was the creation of an international structure to solve conflicts, the League of Nations. The allies accepted President Wilson’s suggestions.

These principles, however, would never be entirely applied, as a compromise with more realist and punishment principles combined with them to create the reality of post-war Europe.

• In 1919 and 1920 three treaties were signed between the victors and the losers: the Treaty of Versailles (dealing with Germany), the Treaty of St. German-en-Laye (dealing with the Austro-Hungarian Empire) and the Treaty of Sèvres (dealing with the Ottoman Empire). They were quite harsh on the losers, with severe amputations of territory and population. Germany, for instance, lost 13% of its prewar territory and 10% of its population. But what is most important is that the end of the war brought a drastic reconfiguration of the European continent.


# Consequences

It was largely because of the territorial solutions found at the end of World War I that World War II started. Two other major of the consequences of the war were: the creation of the first communist state in history and the first military participation of the US in Europe.
