*Western Preeminence*

Main topics:
- Western ideas, concepts and principles predominate in the world today
- But it was not always like that: until the success of European expansion in the fifteenth and sixteenth centuries the West (Europe, to be more precise) was not clearly ahead in any respect in relation to other cultural areas (mostly Islam and China)
- The Discoveries changed that picture
- The question is: are the Discoveries a sign of the cultural and civilization dynamism of Europe or were they the event at the root of that predominance? Put another way: is Western preeminence explainable by European dynamism before the Discoveries or were the Discoveries the essential element that explains Western preeminence?

The contemporary world is defined by the preeminence of the West:
- The main military and economic power is Western
- The economic principles we think today are conductive to prosperity are of Western origin (the free market, international exchange)
- The basic unit of international geopolitics (the nation-state) is of Western origin
- The political regime that is considered most satisfactory (liberal democracy) is Western

But it was not always like that:
- Until the fifteenth and sixteenth centuries Western ascendance was not clear and it was only after the nineteenth century that it became unquestionable
- If it were easy to compare civilizations, maybe it would become visible how in the fourteenth century European civilization could not be considered more sophisticated than the civilizations of the Middle East or China

Middle East:
- Islam was born in the Arabian Peninsula in the seventh century and had a most notable expansion throughout the Mediterranean Sea and other regions to the East from its original area
- This expansion had two great phases, the first by the Arabs, the second by the Turks (after the decline of the first phase)
- Throughout all this time Islam revealed a very high degree of cultural wealth, development and sophistication that, mostly in the start of the period under consideration, was superior to that of Europe


# China

If it were possible to bet, many of us looking at the comparative level of development of China in relation to other regions of the world, would say China was the place where the most sophisticated civilisation of the future would be born.

China had a stable civilisation and form of government since the third century before Christ. Until the fifteenth century China had technical and scientific knowledge unrivalled in the world (paper, printing, gun powder, the magnetic compass).

China even had its own expansion programme in the Pacific and Indian oceans. In the fifteenth century, however, Chinese development and expansion stopped, coinciding with Western ascendance.

The fifteenth century is, hence, the period that marks the beginning of the clear ascendance of the West. But we have to consider previous signs. Since the Middle Ages that, although in a relatively low profile manner, Europe was giving signs of its dynamism.

Until the year 1000 Europe was mostly ravaged by foreign attacks. From that year onwards it became able to protect itself better from those attacks and start its own attacks outside of the continent.


# European world expansion

European expansion marks an important break with the continent’s past; so something was going on in Europe in the Middle Ages and the subsequent period.

Was it chance that allowed the Europeans to expand and predominate or was there some underlying aspect of European civilisation that allowed for it? A big issue of course is: how it was possible to finance the Discoveries?

I will mention two explanations for what happened then:
- The one by E.L. Jones
- And the one by Kenneth Pomeranz

## E.L. Jones

E.L. Jones (in the book The European Miracle) posits that Europe was already richer than most of the world in the fifteenth and sixteenth centuries. He stresses the European institutional and environmental structures.

In institutional terms: the lesser interference of political power in economic activity, something that gave economic agents confidence enough that they would not be expropriated of their efforts. This was crucial to allow for stronger investment and capital accumulation.

But the European advantages were also important in what respects the natural environment: this environment was more favourable to economic growth thanks to a lesser intensity of natural catastrophes.

This lesser intensity of natural catastrophes had an impact on growth that was not direct but indirect; its main impact was on the demographic behaviour of European populations: basically, the lower propensity for climate disasters meant that the Europeans were able to adopt a demographic behaviour that put less pressure on the existing resources, including capital.

Demographic adjustment in Europe was done by birth control rather than by mortality: Europeans adjusted births to the existing resources – in times of prosperity they married earlier and have children earlier; in times of crisis they married later and have children later; many times they would not even marry (celibacy increased).

In Asia, on the contrary, the demographic regulator was mortality: population would accumulate until a natural disaster (which were more frequent in Asia) would reduce drastically population density and reestablish balance with existing resources. The result was higher capital density in Europe and, consequently, higher income per capita.

## Kenneth Pomeranz

As for Kenneth Pomeranz (in the book The Great Divergence), he tries to combine an explanation like that of E.L. Jones (stressing internal European features) with the benefits of expansion itself.

To Pomeranz, internal European dynamism was crucial for what happened, but then colonial exploration reinforced that dynamism and led to the great divergence of Europe in relation to the rest of the world.
