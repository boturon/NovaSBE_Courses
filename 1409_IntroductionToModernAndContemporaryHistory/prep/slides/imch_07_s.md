*The first inclusive institutions: the Glorious Revolution (1642-1688)*

Main topics:
-  Britain was the first country to adopt modern inclusive political and economic institutions – it happened in the 1688 Glorious
-  This revolution resulted from a confrontation between the crown and the parliament, ending with the parliament’s victory: the king would now be checked by civil society through the parliament – the first constitutional
-  These inclusive institutions are what explains why Britain was the first country to enter into the process of modern economic
-  But things could have gone otherwise: in a first phase, Britain became a military dictatorship, only later it became the first liberal country in history – the power of contingency.

The crucial moment of institutional innovation in world history was that of the occurrence of the liberal revolutions in Europe and the United States of America between the seventeenth century and the nineteenth century. And the first of these revolutions was the English Revolution, sometimes also called the Glorious Revolution – in reality a longer process starting with the 1642- 1651 civil war and concluding with the Glorious Revolution properly speaking in 1688.

Not by chance, England (or Great Britain, better put) was the first country to enter into the age of Modern Economic Growth through the Industrial Revolution between the eighteen and the nineteenth centuries.

At the origin of this revolution is an old conflict opposing the king and the parliament, mostly over the issue of taxes to pay for the king’s military activities. We are not talking of a parliament of the same sort we have today, but still an institution that granted some representation of society.

The origins of the situation were quite old: we could date it from 1215, when a significant part of the English lords were able to impose on king John a document called the Magna Carta (the “Great Charter”), which basically forced the king to consult them any time he wanted to raise taxes or introduce new ones; this was consolidated in 1265, when the first parliament was elected.

The limits in relation to modern parliaments were many. Perhaps most importantly, the electoral base was extremely narrow (aristocrats, some religious figures and some rich men), the parliament would be called and dismissed at the king’s will, and the king retained a series of other arbitrary powers, especially with respect to justice.

These were the two institutions whose long history of conflict exploded in the seventeenth century.

The civil war erupted in 1642, when Charles I, of the Stuart dynasty, was forced to call the parliament in order to finance its wars with Scotland: unable to win the wars, Charles progressively lost the parliament’s support; conflict broke out when the parliament refused to dismiss at the king’s orders. This first phase of the civil war would come to an end in 1649 with the victory of parliament, the decapitation of Charles I and the creation of a republic, the Commonwealth, under the leadership of Oliver Cromwell. Again showing the power of contingency, the Commonwealth was not a liberal regime but rather a sort of military dictatorship.

This dictatorship would come to an end with the restoration of the monarchy in 1660, but conflict would resume about 30 years later; the parliament would then invite in 1688 the Stadtholder of Holland, William of Orange, to side with it in a confrontation with James II.

The main elements of the changes introduced by the Glorious Revolution were condensed in the 1689 Bill of Rights:
- The king could not suspend existing laws
- Could not interfere with the functioning of judicial courts
- There would be no standing army without the parliament’s authorisation
- No war could be financed without the parliament’s decision - The parliament should be elected
- The election of parliaments did not depend of the king’s will.

Thanks to these changes, England became the first modern constitutional monarchy in history.

The English institutions resulting from the Glorious Revolution were the first modern inclusive political institutions, resulting in the first modern economic institutions – it is no surprise that the Industrial Revolution appeared in England little after these changes.

The Glorious Revolution:
- Abolished domestic monopolies that limited the range of markets - Increased the security of property rights
- Changed the nature of property rights (especially in agriculture: enclosures)
- Protected innovation (patent law)
- Promoted a financial revolution.

That these changes occurred in England has to do with previous developments:
- The Magna carta created a prototype charter of rights
- The Black Death moved power away from lords in favour of lower classes
- The king was not able to monopolise colonial trade (contrary to what happened in Spain or Portugal) and a large and powerful merchant group appeared
- All of this created a vast coalition going from large merchants to small peasants that got together in the seventeenth century in favour of the changes of the revolution

Still, contingency: the same coalition also led to Cromwell – nothing is predetermined.
