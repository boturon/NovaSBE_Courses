*The end of Western preeminence? The rise of other powers*

Main topics:
- The Cold War ended in the late-1980s and early-1990s with the defeat of the communist side of the confrontation: this was not a military defeat but rather an economic one: communist countries could not keep up with the material standards of Western countries
- Once the Cold War ended, many of the former communist countries entered into the world economy giving rise to a new age of globalisation, on a scale totally unprecedented
- One of the main consequences of this process is that Western countries started losing importance in the world economy, with new powers rising
- One big question remains: is this reversal going to last and be completed? Have non-Western countries the means to sustain growth at the same pace?

The Cold War was a global confrontation that had many dramatic episodes through which each of the sides claimed various victories between the 1950s and the 1980s. But it all came to an end in the second half of the 1980s: the communist world started collapsing in economic terms, under an increased military pressure from the new American president: Ronald Reagan.

The new Soviet leader, Mikhail Gorbachev, tried to reform the communist system, but these attempts ended in collapse: in 1989 the population of Berlin destroyed the wall dividing the city, in 1991 the USSR itself disappeared as a country.

As this large number of countries joined the world economy, we entered into a new age of globalisation. Russia became a mass supplier of commodities to the world economy; the majority of the East European countries joined the European Union, which is an enormous trade arrangement; China became the manufacture of the world.

Never have international transactions been so important for the world economy than in the last twenty to thirty years – precisely since the colapse of the communist world.

One of the most important effects of this new wave of globalisation was that many of these newcomers became quite big players on the world scene. This meant that Western countries started losing their relative position in the world economy, as the newcomers were able to grasp the opportunities offered by their entry into the world market. There are various signs of loss of importance of the West: one, very significant, is its decline in terms of economic weight. Another is the massive recovery of the ground lost in previous centuries by countries such as China or India.

If this is true for the West as a whole, it is also true for individual Western economies. If today the US is still the biggest economy in the world, and if many Western economies are close to the top, the predictions are that in a few years from now China will be the biggest economy, mostly accompanied by other developing economies, with only the US (as far as Western countries go) remaining among the top league.

The big question here is: is this irreversible? Is growth in the non-Western economies sustainable? If we believe in the ideas presented in this semester, that you need inclusive political and economic institutions, as opposed to extractive economic institutions, then most non-Western countries need much reform to acquire such institutions.

An interconnected problem with which we will deal in a future class is if inclusive institutions in Western countries are not becoming extractive under the pressure of a series of economic, social and political phenomena.
