*Parallel inclusiveness and exclusiveness: the Cold War*

Main topics:
- The Cold War was the separation of the world into two wide spheres of influence: a Western one and a Soviet one
- In the Western part there was in many places (although not in others) a return to the old inclusive institutions, something that also led to a return to a globalised world
- In the Soviet sphere of influence there was no return to inclusive institutions and, also, no return to globalisation
- In the West this was also the period with the most spectacular economic growth episode ever.

The Second World War was fought on the side of the allies in the name of democracy and against fascism. However, this idea implied a contradiction in the alliance, for the USSR was one of the most authoritarian regimes ever. Thus, as the Second World War ended, the ideological differences (liberal democracy vs communism) between the members of the alliance became clear; the result was the Cold War.

The Cold War meant that part of the world returned to political and economic inclusive institutions while another remained under exclusive political and economic institutions.

It all started in Europe, along the lines of occupation of the armies of the two sides.

The part with inclusive institutions corresponded to Western Europe, North America, Australia and Japan. The part with non-inclusive communist institutions corresponded to the USSR, Eastern Europe and a growing number of countries, the most important of which was China, after the 1949 communist revolution. In between there was the large ideological battlefield of the rest of the world, where a series of conflicts happened in order to establish if capitalist or communist institutions would prevail – in many of the non-communist parts institutions were not inclusive as well.

A crucial reason for the return of inclusive institutions to Western countries was the US’s willingness to provide an international framework of cooperation and support, contrary to what had happened after World war I. This was visible in 1947, with the Marshall Plan, the foundational moment of European cooperation that ultimately led to the creation of the European Union.

But the international framework provided by the US had other elements:
- The North Atlantic Treaty Organisation (NATO)
- The General Agreement on Trade and Tariffs (GATT)
- The International Monetary Fund (IMF)
- The World Bank.

Although not along the same lines as in the West, the USSR also tried to provide an integrative framework for the communist world: the American framework tried to basically coordinate market forces; the Soviet framework suppressed market forces and coordinated the economies through the State:
- The Council for Mutual Economic Aid (COMECON) was created in 1949 (a response to the Marshall Plan)
- The Treaty of Friendship, Co-operation and Mutual Assistance, normally known as the Warsaw Pact was signed in 1955.

The difference was that East European economies were initially organised along autarkic lines, meaning that foreign trade represented little in their economies; some changed happened from the mid-1950s onwards, but still foreign trade represented less in the communist world than in the West.
