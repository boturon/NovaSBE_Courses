*A brief outline of the History of China (960-1911)*

Main topics:
- An outline of Chinese history since 960 until the twentieth century to understand why Pomeranz is correct when saying that it is necessary to look to separate parts of China and Europe but not correct when saying that in the eighteenth century China was on an equal footing to Europe
- The high days of China had stopped already in the thirteenth century; in those years there was a shock (the “mongol interregnum”) from which China never really recovered: stagnation and decline followed
- In the eighteenth century there was no part of China that was ready to cope with Britain in the process called the Industrial Revolution.

Much of Pomeranz’s point of view comes from the widely agreed idea that China reached high levels of development much before the West.

The idea may be widely agreed but what the recent data on GDP per capita show is that there is a crucial chronology to it: Chinese development and advantage over the West peaked somewhere in the period we call in Europe the Middle Ages (coinciding in China roughly with the Song Dynasty period: 960-1276).

What followed was a stabilisation at that high level of development, first, and then a decline, from the fifteenth century onwards, precisely at the time Pomeranz suggests levels of development between China and Europe were comparable; all of this happened while western countries progressively overtook China in terms of development.

So Pomeranz is right in calling attention for high levels of development outside of Europe before the industrial revolution but wrong in the picture concerning China: he fails to understand that what China displayed in the seventeenth century were the final years of a long period of stagnation at a high level coming from a few centuries before and that the eighteenth century (before the industrial revolution in Europe) is one of decline in China.

We will use the traditional dynastical division of Chinese History, just for commodity, and start from the period corresponding to the European Middle Ages until the nineteenth century:
1. The Song Dynasty (960-1279)
2. The “Mongol interregnum” or the Yuan Dynasty (1279-1382)
3. The Ming Dynasty (1368-1644)
4. The Qing Dynasty (1644-1911)


# The Song Dynasty (960-1276)

The period of the Song Dynasty corresponds to the high point of Chinese history, but this was also in a large part a result of quite old processes:
- One can say the Song were the culmination of ways of governing that were perfected throughout more than 1,000 years – since the third century before Christ
- One can also say that technology reached a very high level, with a large number of notable innovations, but all resulting from a long evolution
- The same can be said for the notable economic feats, which made of China the richest part of the Medieval world.

Administration:
- Centralisation of political power in the emperor’s hands
- A highly efficient tax system
- A merit-based civil service staffed by commoners rather than aristocrats.

Technology:
- Invention of gunpowder
- Invention of the magnetic compass
- Invention of paper
- Invention of movable-type printing
- Shipbuilding.

Economy:
- Expansion of small-holding ownership and tenancy (with new methods of rice cultivation)
- Expansion and protection of markets
- The first banknote monetary system
- International commercial activity with early joint-stock ventures.

All this was reflected in many cultural and scientific dimensions (mathematics, philosophy, literature…) and way of living (vast growth of cities, some of them reaching the one million mark of population, such as Kaifeng and Hangzhou – note that one million was the whole of England’s population in the same period).


# The “Mongol interregnum” or the Yuan Dynasty (1279-1382)

However, the Song dynasty would come to an end in a very traumatic manner that would mark the history of China forever: through the Mongol invasion of Genghis Khan and his descendants (from 1211 to 1279).

This is the rough equivalent to the so-called barbarian invasions of the Roman Empire in Europe, leading to a lasting deterioration of life conditions; the difference lies in the fact that, after a few centuries, Europe was able to reach the highest points in its history while China is still looking for a return to the greatness of old.


# The Ming Dynasty (1368-1644)

The “Mongol interregnum” (sometimes also known as the Yuan Dynasty period, as the Mongols adopted this name from 1271 onward) would last for about a century (1279-1382) to be replaced then by the ascent of a new Chinese dynasty (and the last in actual ethnic terms), the Ming.

This was no return to the greatness of old but rather the maintenance of a stable situation of high level of development – and that was the Ming problem, precisely, because many things were happening outside of China, especially in Europe.

The administrative and social features of China were generally maintained (despite some changes), and then a certain territorial expansion occurred.

Despite being little progressive by comparison to the Song, the Ming Dynasty would be marked by a few important features of Chinese history:
- Beijing was definitely converted into the empire’s capital
- The Forbidden City acquired its final (and current form)
- The same happened with the Great Wall.

Most interestingly, the Ming would be involved in a series of long-distance navigation voyages, which constitute the Chinese equivalent to the European Discoveries voyages:
- The great figure here is admiral Zheng He, whose first voyage dates from 1405, ten years before the Portuguese expedition to Ceuta (1415)
- The programme only lasted until 1433 but covered a wide area throughout the Indian and Pacific oceans
- As the programme ended the Europeans started reaching Asia’s shore through their own and incredibly successful programmes, showing again the relative decline of China and ascent of Europe.


# The Qing Dynasty (1644-1911)

The last Chinese imperial dynasty was again a foreign one, originating from Manchuria. The fundamental administrative features of the Chinese Empire were kept during the Qing Dynasty, which was associated with three important processes.

First, the virtual final territorial form of China, through military conquest.

Second, a process of increasing agricultural production in a first period, both through expansion of cultivated area and increasing productivity, followed by a period of decline of productivity. This second period is normally explained by a seemingly uncontrolled growth of population, reaching lands of ever lower quality, and progressive fragmentation of agricultural property, leading to smaller agricultural units of increasingly lower productive capacity.

Third, a growing domination of China by foreign powers, especially European but with a contribution from Russia and Japan as well, leading to the eventual disappearance of the Empire and its replacement by the Republic in 1911. This is a process that developed fundamentally from the Opium Wars (1841 and 1856) onward, leading to the signature of a series of treaties with foreign powers that virtually handed to them the government of large tracts of China.


# Main features of traditional Chinese society helpful or harmful to economic growth?

## State and Government

China was the first place in History having something similar to a modern State: the Empire was a vast administrative structure, staffed by bureaucrats that were selected by merit (and also progressed in their career by merit) and who ran complex affairs of Government, from the tax system to the canal system; Chinese bureaucrats were efficient in protecting property rights and allowing markets to function – all of this was positive for growth. But these bureaucrats formed in themselves an elite increasingly separate from the realities of the world and they were mostly concerned with property rights in agriculture, attributing lesser importance to industry and commercial activities – less positive for growth

In order to understand this we need to understand the traditional division of society.

This division was based on the “four occupations” principle. The four occupations were:
- The Scholars (the gentry participating in the government structure), below the Emperor: their power derived mostly from participation in government rather than the economic power of running an agricultural estate (although being landowners, they were not landowners on the same scale as European aristocrats)
- The Peasants: relatively small landowners or tenants, which constituted about 80% to 90% of the population: although a large number of the farmers were free farmers, some had to work to aristocrats in a form akin to serfdom
- The Artisans
- And finally, the Merchants This was a relatively rigid hierarchical structure but that allowed for some mobility.

This was a relatively rigid hierarchical structure but that allowed for some mobility. Mobility was especially noticeable in the Song and Ming dynasties, with various merchants being able to reach higher rungs in the social scale, namely in the bureaucratic service.

But the Qing dynasty seems to be associated with an increasing rigidification of society, corresponding to increasing dominance of the government over society, which was unable to display the dynamism found in European societies.

So, we can conclude by a rigidification of Chinese society from the seventeenth century onwards, making it unable to deal with economic change in an innovative way.
