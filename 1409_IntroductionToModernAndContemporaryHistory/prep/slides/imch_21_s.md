*The end of Western preeminence? The challenge to inclusive institutions*

Main topics:
- The distributional effects of globalisation have political impact: globalisation has a positive aggregate economic effect but also generates winners and losers
- This was true in the First Age of Globalisation and is true now in the second, and in both cases there have been political consequences, which have raised threats to the Western inclusive institutions
- The rise of parties and politicians coming from anti-democratic traditions is a clear phenomenon in Western countries; the question is how far will they go: will they really destroy Western institutions or will the be absorbed in some wave of reform?

Although it is possible to make the case that globalization has a positive aggregate effect on the economies participating in it, this does not mean that everybody benefits from it. Globalisation has distributional effects: some people win, some people lose – certain activities are relocated to other parts of the world, other activities simply become obsolete, others become ever more necessary; aggregate improvements hide losses of specific groups.

We have seen that in the current wave of globalization one of the biggest losers has been a large section of the middle class in Western countries: besides the problems this raises for them, it also seems to start having an impact on the democratic inclusive institutions of those countries.

**(slide 323 the elephant)**

The gains of the emerging countries’ middle classes are not independent from the losses of the Western countries’ middle classes: this inverted destiny has to do with the strong relocation of manufacturing and services activities from Western countries to emerging countries. The western middle classes got used to build their careers around life-time employment and a series of additional benefits associated with it: unionisation, health plans, unemployment insurance, paid holidays...

But how does the decline of these groups affect the political system? The Western political systems after World War II were built around the satisfaction of these groups: most centre-left political parties represented the political power of these workers: Labour in the UK, SPD in Germany, the Socialist Party in France, the Democratic Party in the US, etc.

Western political systems were organized in a non-violent confrontation between these parties and the parties defending the interests of other groups: property owners, shop owners, service workers; normally these were parties on a Christian Democratic tradition – narrow margins defined the victory of one or the other side and policies did not change dramatically. The decline of the industrial middle class has brought the decline of most centre- left parties, which have been replaced in the sympathies of this group by either more radical left-wing or right-wing parties; another phenomenon has been a large swing in voting.

Parties that used to be on the fringe of the system became now important players: UKIP in the UK, Front Nationale in France, Podemos in Spain, Syriza in Greece; in Portugal also, although in a slightly different way: Communist Party and Left-Wing Bloc, etc.

What characterises the social programmes that have been associated with the central parties in Western democracies is their national nature: while the forces of globalisation are international in nature, the Welfare State is made of national programmes.

So, one of the features of the recent political reactions is a certain from of nationalism: demands for protectionism or for curbing immigration. Another feature of these reactions is a certain from of anti-elitism: this comes from the idea (which, as we have seen, is basically correct) that only a small number of people, that have become increasingly richer, have benefited from globalisation.

The problem with this sort of feeling is that they have increased the support for parties that come from anti-democratic traditions, thus increasing the threats for the survival of the Western democratic systems – this means that these parties are a threat not only to globalisation but to those democratic systems as well.

History allows to understand that we have already been here: the First Age of Globalisation also brought political instability, precisely because it was associated with winners and losers: the decline in agricultural prices, thanks to imports from America and Australia, brought social devastation to many parts of Europe; the destruction of certain industrial activities that became obsolete thanks to imports from countries that produced them at much lower prices, made a large number of people redundant.

The political reaction was strong, with the rise of protectionism and nationalism and the eventual end of the First Age of Globalisation with the outbreak of World War I.

The question is how far will the reaction go: the threats to inclusive institutions are growing; what we do not know is if they will really change the institutions.
