*Inclusiveness interrupted I: communism and fascism*

Main topics:
- World War I was not just responsible for the interruption of the First Age of Globalisation, it was also responsible for the installation of communism in Russia, something that led to fascism and World War II
- In itself, communism in Russia created non-inclusive political institutions and extractive economic institutions, but, by leading to the fascist reaction in the West, it also was at the origin of non-inclusive political institutions and extractive economic institutions in Western Europe
- We will assess the nature of the communist institutions in Russia and also the institutions of the first fascist regime, in Italy, and how they represented a break from the process coming from the previous centuries.

World War I put in motion a new historical drift that sent western countries away from the inclusive institutions that had been consolidated in the nineteenth century.

Two factors played a major role in the deepening of that drift:
- One was the situation of crisis of most economies, which did not adapt easily to the new postwar circumstances; the war had introduced many distortions in the functioning of those economies and in the relationship between them. In this respect, the return of the US to a certain isolationism, showing a lack of will to coordinate the western economies was very important – the clearest sign of American isolationism was the refusal to participate in the League of Nations, an institution designed and proposed by the US themselves;
- The other factor was the survival of communism in Russia: due to the nature of its economy, the USSR seemed not to be facing the same sort of problems as the capitalist economies; additionally, the USSR started sponsoring communist parties in Europe, deepening the social and political chaos of European countries. In this context, communism looked like a threat to liberal and capitalist countries, something that generated various reactions – the most radical of those reactions was a political movement that proposed to combat communism on communism’s own terms: fascism.

In the two decades following the end of World War I (1920s and 1930s) Europe was covered by non-inclusive political and economic institutions.


# Communism in Russia

Since fascism presented itself as a reaction to communism, we should start by looking at the first communist regime ever: Russia. As we have seen, communism as a regime appeared in Russia in 1917, still during World War I. We need to understand the context: the Russian Empire was one the most outdated regimes in Europe. The successive defeats of the Russian army in the First World War led to a series of political movements willing to overthrow the tsarist regime.

1917 was a year of revolution in Russia, and it all started with a liberal-oriented revolution, in February: if this revolution had been successful to the end, Russia could have converged to the sort of institutions existing in Europe: the party leading this revolution (the Kadets, of Constitutional Democrats) were inserted in that family. However, soon the revolution would be highjacked by the communist party, which was able to create an alternative institution to the parliament where the Kadets predominated: the Petrograd Soviet, leading to the creation of a situation of dual power (parliament vs Soviet).

The situation could not be clarified in a peaceful way and it was not: in October, the Bolsheviks conducted a coup d’État and took power. A series of events followed (including a civil war between 1918 and 1922) through which the Bolsheviks consolidated their position in power.

In the 1920s and 1930s the communist regime would acquire its form, and its authoritarian, non-inclusive nature became quite clear: this was a particularly brutal regime, especially under Stalin (secretary-general of the party between 1922 and 1953).

All typical liberal freedoms were suppressed (freedom of association, of speech, etc.), a brutal political police was installed (the Cheka) as well as a vast network of concentration camps (the gulag); repression was ferocious in the early 1930s – throughout the entire stalinist period, millions died.

Besides these dramatic political changes, there were also dramatic economic changes: this was the first regime to suppress almost entirely private property (it survived in a residual way only in agriculture and only in certain circumstances) and the first to planify entirely the economy, leaving no room for markets. In a context of crisis in the West, the Soviet solution looked like a threat or an alternative, depending on the person.

Political reactions followed: the most radical was fascism.


# Political reactions followed: the most radical was fascism

Fascism grew out of the political confusion and desillusionment in Italy after the First World War: Italy did not get what it wanted in territorial terms. And it grew also from the economic and social crisis: with the economic situation in a mess, the communists were very active from 1919 to 1922, making strikes, occupying farms and factories. Fascists created the Fascii di Combattimento, squadrons designed to fight directly strikers and communists.

The political situation was so volatile that power was practically granted to Mussolini by king Vittorio Emmanuelle II, after the fascists’ March on Rome (October 1922).

Mussolini’s proposal was to fight communism on its own terms: using the same brutality; it is interesting that Mussolini started by being one of the main communist leaders of Italy, and fascism is to a certain extent a splinter of communism. Even if Mussolini proceeded by steps, a full-blown dictatorship was installed during the 1920 and 1930s, with the regime radicalising after Hitler’s rise to power in Germany. The climax of this process happened in 1940, when Italy entered World War II as Germany’s ally.
