*The First Age of Globalisation*

Main topics:
- The adoption of liberal institutions across Europe and in some countries outside of Europe allowed for an unprecedented period of international openness in the second half of the nineteenth century
- We are used to talk of the world today as being a “globalised world”, as if this was an absolute novelty
- However, the second half of the nineteenth century is a period we can call the First Age of Globalisation; as a matter of fact, the circulation of goods, services, and factors of production (labour and capital) around the world reached then an unprecedented scale and, once it was interrupted in 1914 (with the outbreak of World War I), it would take a long time to return to previous levels.

In 1913, the volume of international trade in the world was twenty five times bigger than in 1800, and the period of stronger growth took place between 1840 and 1873, with an annual growth rate of 6%, much faster than the rate of growth of the world economy.

Europe concentrated the largest part of these new trade flows. But there were also important connections to other parts of the world, particularly America.

It was not just trade that grew, it was also the circulation of capital and people: big flows of labour took place; part of them (a smaller part) occurred inside Europe; the other part (the largest) occurred from Europe to America; and big flows of capital also occurred, most of them originating in the most developed part of Europe, in the direction of the less developed part of Europe and various countries outside of Europe.

The second half of the nineteenth century is important for bringing big political and technological innovations that changed the world.

The political innovation was the adoption of free trade policies in various European countries, particularly in Britain, something that (due to its status as the most developed economy of the time) had enormous repercussions in the rest of the world. Free trade ideas were growing in Britain since the appearence of the first modern economists at the end of the eighteenth century (Adam Smith and, then, David Ricardo). Even if free trade ideas became increasingly more popular, the French Revolutionary Wars did not allow for its implementation; but once the conflict was over, the ground was prepared for their adoption.

The crucial moment here occurred in January 1846, with the aboliton of the Corn Laws, the central piece of British protectionism; this abolition was followed by the abolition in 1849 of the Navigation Acts; these Acts limited the access of British ports only to British ships: the joint abolition of these two pieces of legislation corresponded to the inauguration of the free trade age in Britain.

The following moment of great relevance was the signature of the Cobden-Chevalier treaty in 1860, between France and Britain, in which France assumed the compromise of reducing significantly the level of protection in relation to British goods – this was not the full adoption of free trade, but it was nevertheless an important opening.

As important as the remotion of these political obstacles to international circulation was the remotion of natural obstacles; this was achieved thanks to a true transportation revolution: canals, railways, steam ships, all of that contributed for a notable reduction of transport prices.

By 1830 Europe and significant parts of the US were covered by a large network of canals for the transportation of merchandise; between 1830 and 1850 railways would boom both in Europe and the US; and from the 1860s on steam power would start to be used in transatlantic shipping, with an enormous reduction in travel time. Another important innovation was the invention of the telegraph.

Besides the circulation of goods, this period would also be marked by the growth of the circulation of factors of production: labour and capital.

In what concerns labour, the biggest movement occurred from Europe to America; aproximately 50 million people emmigrated from Europe (from Germany, Poland, Hungary, Scandinavia, Russia, Italy, the Iberian Peninsula) to America: about 35 million to North America; and about 15 million to South America.

In what refers to the circulation of capital, it took place along the growth of international trade and was, consequently, dependent of both institutional and technological innovations; in the latter case the invention of the telegraph was crucial, as it was the fundamental technical instrument for the integration of capital markets.

Capital circulated mostly with origin in Britain, but also France (and, later, Germany), in the direction of the less developed countries of Europe, and in the direction of America, both north and south; inernational capital financed mostly railways and canals ,besides other services, such as telephones, public transportation, etc.

The prosperity brought by the First Age of Globalisation and the degree of integration of the world it represented made many people believe that nothing would disrupt such a world. But this was not true – there is one fundamental phenomenon we need to understand with respect to globalisation: while it may bring an aggregate increase in growth and wealth, it has distributional effects; there are winners and losers, and the tensions between these two groups of people may be strong enough to lead to full disruption. In a way this is what happened then, when World War I started in 1914, largelly as a result of mounting tensions across Europe.
