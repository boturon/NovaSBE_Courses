*Inclusive institutions spread: the French Revolution*

Main topics:
- Initially, the rest of Europe did not follow Britain in its institutional change; the French Revolution brought change to France but also to most of Western Europe: that was because of the French Revolutionary Wars
- These were ideological wars: on the one side the ideas of revolution, on the other the ideas of tradition
- But the path was convoluted: the original liberal ideas were distorted and the immediate conclusion of the revolution (in 1815) meant the rise to power of a dictator: Napoleon
- The main contribution of the French Revolution lies in the diffusion of revolutionary ideas throughout Europe.

In the French Revolution we have a case where liberal ideas took paths that went much beyond their original limits, as they had been present in both the English and the American revolutions. The immediate conclusion of the revolution (in 1815) was not in the sense of improving over the liberal legacy. Rather, its main contribution lies in the diffusion of revolutionary ideas throughout Europe at the point of a gun (mostly thanks to Napoleon).

The French revolution was fostered by the legacy of the English and American ones. Both had shown the paths that representative principles could follow. France, the craddle of many of the new ideas, still had a much antiquated regime – all of this happening in the context of a rapidly growing British economy (Industrial Revolution).

But it all started thanks to more immediate and less ideological problems, in particular the problem of war (the Seven Years War) and how to pay for it. The French king called the Estates General in the Summer of 1789 to ask for financing, but the Estates General demanded, in exchange, that the king answered positively to the demands of the various social groups.

In the process, the Estates General promoted themselves to the status of constituent assembly (i.e. an assembly to write a Constitution) and called it the National Constituent Assembly; it was this assembly that ignited the whole revolutionary process. This would lead to a political explosion that erupted in July 1789 (14 July: the storming of the Bastille).

The National Assembly was reponsible for the abolition of the structures of the Ancien Régime, namelly the special rights and tributes of the nobles and the Church – but we have also in the early stages of the revolution an increasing radicalisation of the political process, which was conducted by ever more violent groups.

Such radicalism got increasingly more serious until 1794, with the peak of violence being reached between 1792 and 1794, during the so-called Jacobin period, a period in which the guillotine was widely used as a political instrument.

In 1794 the process calmed down and started moving in a conservative direction; in 1799, Napoleon reached power in a coup d’état and created the Consulate (a triumvirate); in 1802 he became First Consul for life (a dictator in everything but name) and in 1804 he became emperor.

A most important feature of the French Revolution, besides internal violence, was external violence, namely against the enemies that soon declared themselves to be against it. These enemies were more or less closely associated with the French aristocrat émigrés.

Since 1792 France would be at war with a large part of its neighbors. The most important of them were Russia, Prussia, Sweden, Austria, many German states, Spain, and Britain.

A new sort of conflict was created. This was no longer the traditional sort of war between conflicting dynasties for the control of territory. It was an ideological war. On the one side the revolution; on the other the old monarchical order.

Among the many French military leaders, one acquired enormous notoriety, General Napoleon Bonaparte. His successes granted him huge popularity, and it was this popularity that he used to reach power and consolidate it from 1799 onward.

This marked the beginning of an entire new phase not only for the French Revolution but for Europe as a whole. In internal terms, Napoleon created an autocratic regime. In external terms, he embarked on an outstanding military adventure that would change the face of Europe.

# Napoleon

The effect of the presence of the Napoleonic armies in Europe was enormous, destroying the old institutions in the occupied countries and bringing modern institutions and law, mostly through the Napoleonic Code.

When he was defeated in Waterloo in June 1815, Europe was not the same; and even if the Treaty of Vienna tried to bring back old institutions it proved to be a failure.

A series of revolutions erupted against the reinstatement of these old principles. The results of these revolutions were ambiguous: on the one hand, many countries reformed their institutions, leading to the introduction of the electoral principle in places where it did not exist, to the enlargement of the right to vote and to the creation of new representative assemblies.

Even if these reforms fell short of what many revolutionaries wanted, the truth is that fundamental liberal changes were introduced. By 1850 most of Western Europe became fundamentally liberal (despite the many concessions to tradition), thus closing the circle initiated with the English Revolution.

Inclusive political and economic institutions had spread throughout Europe, with the exception of the Austrian and Russian empires, which were able to restore full dynastic powers at the end of the French Invasions
