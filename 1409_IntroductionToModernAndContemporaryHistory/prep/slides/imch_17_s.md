*China’s rebirth: twentieth-twenty-first centuries*

Main topics:
- During the second half of the nineteenth century and the early twentieth century, China had to deal with the question of the West: inferior people that ended up dominating China
- This led to the actual adoption of Western ideas: nationalism, republicanism and, later on, communism
- The republic was installed in 1911, communism in 1949
- This was the basis for the transformation of China in the late-1970s and early- 1980s: a mix between communist and capitalist ideas.

As historian Stephen Haw says: “All of the most important events in China after the Opium Wars were influenced, to a larger or lesser extent, by its relationship with the West”. Traditionally, the Chinese saw westerners as barbaric people – so it was with great shock that they looked at the defeats with Western countries in the Opium Wars and the subsequent domination of large parts of China.

The same applied to Japan, a former tributary country that had defeated China in the 1894-1895 war and also occupied a part of Chinese territory – we have to note that Japan was, since the second half of the nineteenth century, the most westernized country in Asia.

Humiliation led to interest: what was it that these barbarians had that made them dominate the greatest civilization on earth? A series of ideas started flowing into China by the late nineteenth century and the early twentieth century.

This ideas interacted with a double problem related with the empire:
1. The ruling Qing dynasty was responsible for the decline of China; so, republican ideas started appearing, especially as they became so strong after World War I (end of Russian, German, Austro-Hungarian and Ottoman empires)
2. The Qing dynasty was foreign: the Qing were Manchus not Chinese – this led to a Chinese nationalistic movement against those foreign rulers, a movement that linked with the republican ideas.

A series of events threw China in disarray throughout the second half of the nineteenth century and the first decade of the twentieth:
- The Taiping Rebellion (1853-1864): a rebellion against the Qing and entirely inspired by Western ideas, including Christian ideas – a sort of civil war (20 to 40 million dead)
- In the last decades of the nineteenth century, the Chinese emperor accepted the collaboration of westerners in government and started importing Western technology in a deliberate way; reforms inspired by Western ideas started circulating within the court
- This led to the Boxer Rebellion (1898-1901), a large movement against the importance of foreigners in the empire’s government: the court divided between those in favor of the Boxers and those against, the latter with the help of the European occupying powers, which actually stationed at the gates of Beijing (1901): the empire continued accepting Western cooperation.

It was from this situation that the nationalist-republican movement was born, led by Sun Yat-sen: Sun was educated in America and lived in Europe for some years in the early twentieth century – he was completely westernized.

A series of dramatic events led to the overthrowing of the Qing dynasty and the installation of the republic in 1911, which was only completed in 1928, with the country divided for more than a decade – a system that existed for more than 2,000 years, the Chinese Empire, just vanished. Sun died in 1925 and was followed by Chiang Kai-shek as the main nationalistic leader (the Kuomintang was the nationalist party), but then a new idea appeared: communism, inspired by what had happened in Russia and even financed by the USSR.

China divided now between the Kuomintang and the communist party, led by Mao Zedong – a civil war ensued that only ended in 1949, after World War II, when the communists reached power: the People’s Republic of China was officially established on 1 October 1949.

China became an orthodox communist country, and even more radical than the original, through various episodes:
- The Great Leap Forward: a brutal industrialization program starting in 1958 that was progressively shown to be a failure
- The Cultural Revolution: a radical program of communisation of Chinese society, between 1966 and the early 1970s, from which a large number of detentions in reeducation camps and actual assassinations resulted.

As these radical policies proved to be a failure, a reform program started being applied after 1976, mostly inspired by Deng Xiaoping: between 1976 and the early 1980s, a sort of private property was reintroduced in agriculture, State- owned industrial companies were given autonomy, and special areas were created that were authorized to attract foreign investment. These reforms are the basis for modern China: a combination between communist and capitalist organization.

We are still talking of extractive institutions, but with greater autonomy being given to economic agents – the big question is if these institutions are sustainable or if strong economic growth is sustainable.
