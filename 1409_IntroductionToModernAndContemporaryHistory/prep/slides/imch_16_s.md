*Beyond Europe and North America*

Main topics:
- European expansion had an enormous impact around the world, none the less because of its sheer dimension
- But Europeans did not, most of the time, introduce the kind of inclusive institutions that were being developed in some parts of Europe; quite on the contrary, they relied mostly on extractive institutions
- Most of these institutions already existed there – what the Europeans did was to create some specific blends of extractive institutions thanks to the interaction between the two types of institutions: European and local
- The situation did not improve much when the Europeans left those parts of the world.

Due to the fact that European expansion reached the four corners of the world, it affected the local reality of practically all parts of it: western preeminence also became clear thanks to this expansion, which was felt either through formal empires or other forms of influence. The strength of the European presence was such that many persons blame it for having introduced extractive economic institutions in those places.

The truth is more complex: inclusive institutions are an European creation; so, most of the world had extractive institutions, independently of what the Europeans brought; what happened was that the contact with the Europeans gave rise to particular sorts of extractive institutions – sometimes equally pernicious, sometimes worse than what existed before.

Inclusive institutions may be a European creation, but the Europeans did not use them in most areas of the world, when they ruled those areas as imperial powers. This was felt differently, however, in different parts of the world.


# Starting with Latin America

This is a part of the world where the imperial powers (Portugal and Spain, for most of the territory) did not even have themselves inclusive institutions; what they did was to insert their own extractive institutions with the local ones and create highly extractive ones.

The Spanish started by plundering the local rich civilisations (the Aztecs, the Maya, and the Inca). Then, from Argentina and Chile to Mexico, they ran the continent through the practice of forced labour – something that had both a Spanish and local blueprint; the survivors of the pre-colombian civilisations were affected by it but also the local “indians” – much of the latter were exterminated when they resisted.

In Brazil, the Portuguese built the colony on the back of massive numbers of slave labour imported from Africa, also eliminating most of the Indians.

The fact that these extractive methods were used by the Spanish and the Portuguese should not let us forget, however, that extractive institutions already existed there – so, it is not a question of the Europeans introducing extractive institutions; it is a question of specific forms of extractive institutions being created in the interaction between Europeans and locals.


# This was especially visible in the case of Africa

Many times the Europeans are blamed for slavery in Africa, but this is not fair, for slavery already existed in Africa when the Europeans got there, while it had already disappeared in Europe for a few centuries.

What the Europeans did was to use that local institution in order to create a vast multicontinental trade, spreading it to parts of the world where it did not exist, such as America.

Again, local institutions were extractive – it was not something that the Europeans introduced; slavery was there; the novelty resulted from the interaction between the Europeans and the locals: and the local elite was more than happy to collIaborate with the Europeans.

And we could talk of other parts of the world where this type of interaction had specific effects. Now, a very interesting aspect of these parts of the world was how independence did not improve on the conditions existing during imperial times.


# In Latin America

Independece came in the sequence of the French Invasions of the Iberian Peninsula; the Iberian crowns were weakened and the process ended with the introduction of liberal institutions there.

The local American elites of Spanish origin did not react to the weakening of the Iberian monarchies but rather to their attempt to introduce liberal institutions in America – so, independence in much of Spanish America was an attempt by local elites to preserve the existing extractive institutions – this had a lasting effect throughout the nineteenth and twentieth centuries, and until today.


# In Africa the story was different but with equally lasting consequences

Slavery ended up by being abolished during the nineteenth century (slave trade within the British Empire in 1807; slavery within the empire in 1834). But that did not end the practices of forced labour, which continued in plantations within Africa.

For various decades during the nineteenth century this was made through a good understanding with local elites without much formal ruling, but then came the Scramble for Africa in the 1880s and 1890s, when European powers divided arbitrarily Africa’s territory between their own zones of influence. It was from these zones of influence that independence movements came in the twentieth century.

What is notable in the post-Independence countries created in the 1950s, 1960s and 1970s is the replication by the new elites of the extractive practices left by the imperial powers. This was perhaps made even worse by the fact that the frontiers of Scramble for Africa were totally arbitrary, creating the ground for the constant inter-ethnic confrontations that mark Africa’s life until today.


# Conclusion

In Latin America: “There is a clear logic to the persistence of the extractive institutions. […] The same groups continued to dominate the economy and the politics for centuries” (Acemoglu and Robinson, 2012, p. 362).

In Africa: “Colonial authorities built extractive institutions in the first place, and the postindependence African politicians were only too happy to take up the baton for themselves” (Acemoglu and Robinson, 2012, p. 343).
