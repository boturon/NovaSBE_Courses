*History of tomorrow I*

Main topics:
- In the early twenty-first century the old problems of the past seem almost solved, especially the three most important ones: famine, war, and plague
- But this only means new problems are showing up on the horizon
- Men feel so powerful today that they think they can become as powerful as God, ending with such questions as death and unhappiness
- Has Man become too powerful for his own good?

In the early twenty-first century we are entering a completely different age from the past; three big questions have marked all the course of human history up to today:
- Famine
- Plague
- War.

These three aspects have been so persistent throughout history that we have organised our way of understanding the world as if they were inevitable. All sciences and religions have had these three aspects as the centre of their structure.

But in the early twenty-first century they do not seem anymore inescapable phenomena. It is not that they have disappeared, it is rather that they have become manageable challenges: in the past we have prayed the gods to get rid of them or to win over them – now we know how to deal with them, how to avoid them.

As Yuval Noah Harari says: “In the early twenty-first century, the average human is far more likely to die from bingeing at McDonald’s than from drought, Ebola or an al-Qaeda attack.” This idea may seem outrageous, so it is better to take a closer look at each of the old ghosts in turn.


# Famine

Until recently, even in the now advanced societies, humans lived on the edge of the poverty line, below which people die of hunger. During the twentieth century, tehcnological, economic and political developments have increasingly prevented such situations – it is not that those situations do not exist; it is that they are deliberately caused by political agents. And in developed countries no one dies from hunger; if anything people die from obesity or overeating.


# Plague

Infecto-contagious diseases were, for most of history, an inevitability: we all know of the “Black Death”, of course, but episodes of such sort were recurrent; the last episode of such kind in the Western world was the so-called “Spanish Flu”, in 1919. And killing diseases were not restricted to these large virulent outbursts but included also more restricted attacks by viruses and bacteria – for instance, a large contribution to the death rate of historical societies were the “childhood diseases”, such as smallpox or measles. Advances in medicine in the twentieth century have, however, made such epidemics and diseases almost disappear completely from the Western world and reduce their incidence in non-Western countries.

Infant mortality, which used to be the norm up to the mid-twentieth century became now practically an accident in Western countries. More people die from cancer or coronary diseases rather than infecto-contageous ones, or die simply of old age. Such epidemic diseases as AIDS or Ebola are now controlled in most of the world.


# War

Wars have also been understood as an inevitability in history but have lately become much less common. The worst wars ever in history took place in the first half of the twentieth century and the deadliest of them finished just about 70 years ago, which is not such a long time. Of course, there are still wars going on around the world, but their frequency has declined massively, and the number of people dying from it has also declined massively: “Sugar is now more dangerous than gunpowder” (Yuval Noah Harari, Homo Deus, p. 17).

So, if these long-run tragedies have become under control (and have even disappeared to a certain extent), does it mean all problems have disappeared? No. It just means that new problems have appeared.

And probably the biggest new problem is the one resulting, precisely, from the ability to control the old problems; because, controlling the old problems means that Man is in a certain way taking the place that God used to have in the past: when the old problems were seen as inevitable they were seen as being part of God’s plan (we were then in God’s hands); now that we control those problems we are, consequently, replacing God. Meaning that Man may have become too powerful for Man’s good himself. And this can be seen in many ideas that have now become the fundamental points of Man’s future agenda.

What strange new ideas are now appearing in this new agenda?. One is immortality: many important people are now seriously talking of the possibility of men becoming immortal – and what is interesting is that these people are putting the issue in technical terms, rather than in mystical terms: immortality, for these people, is just a medical issue rather than a religious one. Google has a project, called Calico, the purpose of which is to eliminate death; Paypal’s CEO (Peter Thiel) keeps repeating that his purpose is to remain on earth forever. Whatever we might think of such ideas, they exist and they are becoming mainstream, and they display a willingness on the part of Man (or at least some men) to replace God.

The other big topic on Man’s future agenda is perpetual happiness. And this comes also with advances in medicine: nowadays, there are pharmaceuticals that allow to control practically all of our humours, and we just take them: analgesics for pain, anti-depressants for depression, Ritalin for lack of attention, recreational drugs for various moments of bliss. Again, this is Man playing God: what this means is that we do not need to find situations in which we feel happy, we just need to take the appropriate drug to feel happy. But the effect of drugs passes quickly, so we need to keep on taking them to replicate the same feeling.

So, Man is in search of an upgrade: from man to god. And there are three paths now being developed leading in that direction: biological engineering, cyborg engineering and the engineering of non-organic beings (or robots).

You might feel this is far-fetched, but think of the following:
- Biological engineering: it is now possible to detect and correct genetic malformations of children to be born
- Cyborg engineering: non-organic body parts are being increasingly used to deal with physical handicaps
- Robots are an increasing reality of our everyday life, in firms and even at home.

The problem, as Yuval Harari says, is that such a change might represent the end of what Man is today: Homo sapiens being replaced by Homo deus – enormous problems may arise from that.
