*A brief outline of the History of Japan (1600-1890s)*

Main topics:
- Japan provides a contrasting history to that of China in its ability to face the challenges of the modern age: the essential difference was its ability to transform its institutions in order to create a flexible society and a country open to foreign influence
- Japan followed cycles of openness and closing that ended in the nineteenth century in a radical transformation of the closure principles that had prevailed since the seventeenth century, during the Tokugawa period
- This transformation was the Meiji Restoration, which made of Japan the first constitutional monarchy of Asia.

Japan stands in stark contrast with China in its ability to reform thoroughly in order to cope with the challenges of a world dominated by the West. Small differences with China led to a totally different institutional drift. The original Japanese culture is almost completely derivative from China’s (reading and writing, religion, political ideology, technology). But two things differentiated the two countries and put them on a completely different path: the internal structure of society, which was more decentralized than in China, and the relationship with foreigners.

The first foreign challenge put to the Japanese was when the Portuguese arrived on Japan’s shores in 1543; Japan was passing through its “Sengoku Period” (or “warring states”). The initial reaction by the Japanese elites was quite welcoming.

In order to understand this we need to understand some aspects of Japanese society: Japan was similar to Europe in the fact that it was divided into many political units, each of them controlled by its own samurai or shogun.

There was an emperor at the top of society, like in China, but he was far from being as powerful as the Chinese one; the empire was divided into smaller domains (called han) – it was there that the samurai dominated, while involving themselves in more or less permanent wars between each other. Samurai or shogun were similar to European feudal lords, perhaps with even stronger grasp over the society underneath.

Thus, many adopted Western (essentially Portuguese) ideas and technology: conversion to Catholicism was vast; adoption of fire guns; however, others did not.

There were two fundamental challenges to the Japanese social order put by the foreign influences:
- The challenge to the traditional regime of obedience
- The democratization in the use of weapons promoted by the introduction of guns.

And there was a reaction: a centralisation of power in the hands of a higher shogun – the Tokugawa family, which reached power in 1600 (the emperor still existed, but his power was only formal). The Tokugawa were quite radical in their approach to the suppression of foreign influence.

The principle of Sakoku (isolation from foreign countries), implying:
- Radical suppression of Catholicism
- Prohibition of foreign vessels, from the 1630s onwards, to reach Japanese shores (with just a few exceptions) and of Japanese vessels to leave Japan.

And all of this coincided with a rigidification of Japanese society, with the creation of a sort of caste system (Shinō-Kōshō), with a hierarchy not that different from the one existing in China:
- the samurai on top (increasingly a service aristocracy, like the Chinese bureaucrats, rather than a landed class or a warring class), then
- the peasants, then
- the merchants and, finally,
- the marginals (like the Indian untouchables).

These were rigid classes – you could not pass from one to the other.

As the shoguns were not, in reality, economically independent, they started to get dependent on the merchants, while extracting from the peasants – the result was an increasing prosperity in time of the merchants, who, despite getting ever more rich, were still seen as belonging to the lower ranks of society: these would constitute the mass of reformers later on.

Also, some got close connections with some of the shoguns – as the autonomy of the shoguns in relation with the Tokugawa centre was high, some of these shogunates started to confront the central power in many respects, including contact and trade with foreigners.

For instance, the Satsuma and Chōshū shogunates started having foreign relations with Britain in 1862 and 1863, independently of the isolationist central policy of the Tokugawa. Soon, these two joined forces with other shogunates and started plotting the overthrowing of the Tokugawa in order to replace what existed with a more open society. So, in 1868 they plotted a revolution consisting in the restoration of the power of the emperor, in the person of emperor Meiji – but this was more than just a restoration.

The revolutionaries proposed the creation of a (two chamber) freely elected parliament. Then they embarked on a thorough revolution, which abolished the Shinō-Kōshō system: people could move from class to class; this implied abolishing shogunates and introducing individual property rights as well as liberty to follow any kind of profession.

In 1890 they adopted a constitution, thus making of Japan the first Asian constitutional monarchy. Throughout all this period they introduced foreign models in practically all aspects of government.

This left Japan prepared to adopt the challenging novelties coming from the West. Japan is, thus, one further example of how relatively similar conditions in relation to other places (in this case China) led to a totally different outcome
