*Paths to backwardness: the Iberian Peninsula and Eastern Europe*

Main topics:
- All of Europe was subject to the same critical junctures, the Back Death and the Discoveries, but the their impact was different in different countries
- The Discoveries could have launched the Iberian countries (Portugal and Spain) into the path of modern economic growth; instead, they were a curse that determined their relative backwardness for a long time
- For Britain, they were the instrument to rise to the top
- The Black Death, in its turn, determined Eastern Europe’s backwardness while giving an edge to Western Europe; later, the French invasions were successful in changing institutions in Western Europe but not in Eastern Europe


# The Iberian countries

The Iberian countries could have been the birth of modern civilisation: they were the expansion pioneers, attracting resources and creating such a vast power in the fifteenth and the sixteenth centuries that everything could have started there. But it did not: it all goes back to institutional development, precisely related with the Discoveries.

In 1688, while England concluded its Glorious Revolution, Spain and Portugal deepened the absolutist traits of their institutions; but one century earlier, nothing much differentiated, apparently, the two areas, with the exception that Spain had the largest empire on earth (having absorbed the Portuguese one between 1580 and 1640). It is this empire that helps explain why the different institutional evolutions.

These empires simply gave the ruling dynasties an enormous amount of resources far from the control of the countries’ societies: silver, gold, many agricultural goods and simple income extraction. Spain and Portugal had their own equivalent of the English parliament: these were the respective courts (cortes), as they were called.

But the resources coming from the empires allowed the respective crowns to circumvent the approval of the courts for many things: whereas in England, due to lack of the same amount of overseas resources, the crown had to establish a permanent relationship with the parliament, the same did not happen in Spain and Portugal – furthermore, both Iberian crowns monopolised trade with the colonies.

Spain was created in 1469, through the merger of the crowns of Castille and Aragon; then came 1492, a sort of miraculous date for Spain, for it was the year of the entire conquest of the Iberian Peninsula from previous Muslim rulers and also the year Columbus reached America – these three things are the basis of the enormous power built in the following centuries.

Precisely, the riches of the empire allowed the new Spanish crown to strengthen its grip over society: the rule of the “Catholic Kings” was marked by serious disrespect of property rights, starting with the expulsion of the jews in 1492 (and appropriation of their assets) and continuing with the expulsion of the Moriscos in 1609.

Thanks to various marriages, Spanish kings ruled over large parts of Europe; Charles V (1516-1556) ruled over the Holy Roman Empire (Germany, parts of France, and other territories in Central Europe), the Netherlands and parts of Italy; his son, Philip II (1556-1598) kept some of these and added Portugal and its empire: Portugal would never truly recover from this episode

To sum up, the empire gave means for the Spanish crown to exert its absolutism with few restrictions: the last time the courts gathered was in 1664, and they would only convene again almost 150 later, after the napoleonic invasions.

“The consequences of these extractive political and economic institutions in Spain were predictable. During the seventeenth century, while England was moving toward comercial growth and then rapid industrialisation, Spain was tailspinning toward widespread economic decline” (Acemoglu and Robinson, 2012, p. 221).

“The persistence and the strengthening of absolutism in Spain, while it was being uprooted in England, is another example of small diferences mattering during critical junctures. The small diferences were in the strengths and nature of representative institutions; the critical juncture was the discovery of the Americas. The interaction of these sent Spain on a very different institutional path from England” (Acemoglu and Robinson, 2012, p. 222).


# Eastern Europe

Eastern Europe was the land of serfdom (and of the absolutism that went with it); the French invasions could have dismantled these extractive economic and political institutions, but the truth is that the ultimate winners of the French Revolutionary Wars (besides Britain) were the absolute rulers of the Austrian, Russian and Prussian empires.

At the end of the war they could reinstate their power; the fact that they controlled vast stretches of the European continent meant that Eastern Europe was not touched by revolution the same way Western Europe was.

If Napoleon’s victory had been complete institutional change could have come to Eastern Europe • As it did not happen, this was a missed opportunity that allowed those countries to retain their institutions.

Contingency was, again, important: had the war gone in another direction and things could have happened otherwise.
