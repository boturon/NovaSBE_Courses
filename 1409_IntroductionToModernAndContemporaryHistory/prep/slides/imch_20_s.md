*The end of Western preeminence? The rise of inequality*

Main topics:
- Among the most important social and economic changes of the twentieth century was equalization of incomes – this was a fundamental phenomenon that helped consolidate the inclusive institutions of the West
- The process was, however, far from linear. Most of the decline in inequality took place from 1914 to 1950, for reasons related with the two world wars and the Great Depression; then, inequality was maintained at a low level, probably due to progressive taxation and redistribution promoted by governments
- However, after the 1980s, inequality started rising again in Western countries. Why? And is this having an impact on the inclusiveness of the institutions of most Western countries?

One of the defining features of the many social and economic changes of the modern age in Western countries is income equalization. This is mostly true of the twentieth century, during which there was a significant redistribution of income from rich to poor. But this was far from a linear and continuous process; it was mostly concentrated in the period from 1914 to the 1950s. After that, there was a stabilization and, from the 1980s onward, a resurgence of income inequality.

Chronology of income redistribution:
- Late nineteenth century-early twentieth century: High income inequality
- 1914-1950s: Fast decline of income inequality
- 1950s-1980s: Stabilization or mild decline
- 1980s-today: Income inequality is on the rise.


# Hypotheses explaining the process

## Kuznets’ curve:

Kuznets suggested inequality followed an inverted-U curve; the levelling of income was connected with economic growth and industrialization: initially inequality would rise due to the passage of labour from low-wage agriculture into high-wage industry; later on, as the process was completed, the wage differential would disappear, leading to income leveling.

Additionally, economic growth allowed for rich countries to build Welfare States, able to redistribute income from rich to poor, thus further helping to reduce inequality.

The data on top incomes (showed above) raises doubts on the Kuznets’ hypothesis: Kuznets believed the process was a gradual one, but the data tell us that it was sudden, concentrated around the period 1914-1950s.

The main explanation for the decline in inequality is that there was a large redistribution from capital income into wage income. And capital income declined because capital owners incurred severe shocks in their assets due to the combined impact of the First World War, the Great Depression and the Second World War: capital assets were destroyed by the wars, they were devalued thanks to inflation and many capital owners went bankrupt during the Depression.

The question, then, is why did not income inequality return to pre-First World War levels once these shocks had passed. The answer seems to be: progressive taxation. As a matter of fact, progressive taxation allowed for income inequality to stay low after the Second World War; this is what we might call the “New Deal Coalition”: the fruit of economic growth is equally distributed by rich and poor, thanks to the existing redistribution mechanisms of the Welfare State.

However, the recent rise in inequality puts this idea into question; although being true that taxation has become less progressive, it is still highly progressive. This raises questions on its efficiency as well as the Welfare State’s to promote equality in the face of redistribution from wage earners to capital earners.

There is a connection between this phenomenon and globalisation: globalisation has led to important redistribution of income in all countries involved, but not in a simple way.

The people affected in a positive and negative way are not necessarily those you would expect; a summary of the distribution would be:
- People in very poor countries were negatively affected by globalisation
- As you move up the income scale the improvement is visible and is strongest among the middle classes of emerging economies
- The situation deteriorates dramatically to the middle classes of rich economies
- It improves also dramatically among the rich people of rich economies.

Reasons:
- Increased skill premium, raising the wages of educated people
- Increased return to investment, raising capital’s share
- Increased productivity in emerging countries, threatening the working classes of rich countries, which can no longer live under protectionism

Although clearly beneficial for a large part of the poor of the world, this process has put under threat the social balance of Western countries, as expressed in the “New Deal coalition”.

The question is: is the process also threatening the inclusiveness of Western institutions, especially in the reaction of the working and middle classes of Western countries?

These social groups, as they feel threatened by the competition and success of workers in poor countries, they start asking for protection and think of authoritarian solutions to implement their plans.

Will the process continue or stop at some point?
