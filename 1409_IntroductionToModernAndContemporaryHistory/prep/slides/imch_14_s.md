*Inclusiveness interrupted II: communism, fascism and World War II*

Main topics:
- We will look into the institutions of the most violent of the fascist regimes, the German one, and how its actions led to World War II
- World War II was a continuation of the interruption of inclusive institutions, in a process that had started with World War I and continued with communism in Russia and fascism in various parts of Europe
- The end of the war was mixed in terms of the return to inclusive political and economic institutions: in the part of the world dominated by Western powers, there was a return to those sorts of institutions; in the part dominated by the Soviet Union there was no return to those sorts of institutions.

The Italian fascist experience was just the first of a series of similar experiences that spread throughout Europe in the 1920s and 1930s. Of course, the most famous of those experiences was that of Nazi Germany: The rise of Nazism in Germany is explained by the sense of defeat in the country, in the sequence of the humiliation of the Treaty of Versailles. Many political movements exploited these ideas. One of them was the DAP, later to be renamed to NSDAP (the Nazi party).

Also important was the economic crisis: first, the hyperinflation crisis until 1924, due to the reparations clause of the Treaty of Versailles; then, the 1929 crisis, in the sequence of the October 1929 Wall Street crash: Germany had become extremely dependent on American foreign investment – the 1930s crisis is mostly a US and German crisis.

Hitler rose to power in the sequence of the 1929 crisis, on an agenda to restore German territory and solve the economic crisis: he won the March 1933 elections, leading president Hindenburg to nominate him chancellor. Hitler did not follow Mussolini’s relatively moderate approach – from the start the regime was very brutal. This brutality reached its climax when, during World War II, the programme of incarceration of Jews and other ethnic minorities was transformed into the Final Solution, a programme of extermination.


# World War II

Besides its internal aspects, one of the most important consequences of nazism was the outbreak of World War II.

World War II can largelly be seen as a consequence of the problems created during World War I, in particular those related to the conditions imposed on Germany after defeat and those related to the disappearence of the Austro-Hungarian Empire. Both things created new geopolitical conditions in Europe, also related to the survival of the Soviet Revolution in Russia.

Largely because of that survival, the principles of self-determination and ethnic homogeneity proposed by President Wilson in 1918 were not followed; instead, arbitrariness prevailed.

These countries contradicted clearly the principle of ethnic homogeneity. None of them was ethnically uniform (except for Austria, although its inhabitants were Germans separated from Germany). The concern of the winners of the war was to create viable countries that could constitute a barrier against Bolshevism from Russia, even if this went against the principles of self-determination.

Since 1935 Hitler started altering the conditions of the Treaty of Versailles: in that year he hold a referendum in the Saar, after which the region returned to Germany; he started rearming, and in 1936 he entered the demilitarised region of Rhineland. In 1938 he entered Austria and annexed it to Germany (the Anschluss). In that same year he invited the Sudeten Germans (in a region inside Czeckoslovakia) to join Germany as well.

This led to the famous Munich Conference, in which France and Britain obtained the promise on Hitler’s part that, if the Sudetenland was transferred to Germany by negotiation, Hitler would not use military force against Czeckoslovakia; but he did so.

Then Hitler threatened Poland; but this time Britain and France stood by Poland; when Hitler invaded Poland, they declared war on Germany. This is how the Second World War started.

World War II took place in four main phases:
1. The invasion of Poland (September 1939) and the declaration of war of France and Britain against Germany
2. The “phony war”: the seven months between the invasion of Poland and the invasion of France (May 1940), a time during which little happened
3. The Battle of France (May-June 1940) and Britain alone against Germany (June 1940-June 1941)
4. The USSR’s (June 1941) and US’s (December 1941) entry in the war.

The invasion of Poland was well succeeded, but there was a subsequent period of seven months in which little happened: the phony war. Germany wanted to invade France but postponed the operation about twenty nine times. Finally it decided to attack France. The result was a shock. In less than one month, France was under German domination.

With the German victory in France, the war would enter its most critical period. Almost all the continent was under German domination. Russia had signed a non- aggression pact with Germany in 1939 (Hitler had cautiously begun exploring the possibility of a thaw in relations with Stalin. Several brief diplomatic exchanges in May 1939 fizzled by the next month. But in July, as tensions continued building across Europe and all major powers were feverishly casting about for potential allies, Hitler’s foreign minister dropped hints to Moscow that if Hitler invaded Poland, the Soviet Union might be permitted some Polish territory) and most of the other countries were under German occupation or control; only Britain lasted to fight Hitler. This period was critical, for Britain had the temptation of making peace with Hitler. However, Churchill, the Prime-minister, made his position prevail and Britain continued at war..

The year that changed the course of the war was 1941, due to two main reasons: first, Russia’s entry in the war in June, when Hitler launched Operation Barbarossa, a full blown invasion of the USSR; second, the US’s entry in the war, following the Japanese attack on Pearl Harbor in December.

The scale of the war was now entirely different than in the year before and Germany was fighting two giant powers. Now it was almost impossible for Germany to win.

It was only in 1943 that the Soviet-Anglo-American allies were able to have the first military successes in Europe; the decisive battles were, in Italy, the Battle of Monte Cassino, and in Russia the Battle of Kursk. These two battles, particularly that of Kursk mark the definite turning point of the war; from then on it was just a question of time for Germany and Japan to be defeated.

The final blow to the German war effort was given when the Anglo-Americans were finally able to mount an invasion of France, in June 1944, in the now famous D-Day, the largest land invasion in History. Caught between the Anglo-American troops, from the West, and the Red Army, from the East, Germany was unable to resist: between April and May 1945, Germany surrendered.

But there was still the war between Japan and the US in the Pacific; here the Americans were facing a huge resistence in an extremely difficult terrain; after months of horrific battles of little strategic value, the Americans resorted to the final weapon to end the war: the atomic bomb; two of these bombs were launched, in Hiroshima and Nagasaki; after it, Japan surrendered, and the war was finally over.

World War II was fought by the allies in the name of democracy, but this involved a contradiction, since on the side of the allies there was a regime completely alien to democracy: the USSR. Soon that contradiction was impossible to solve and the two sides (US-UK-France vs USSR) started clashing over the future of Europe. That was the beginning of the so-called Cold War, which divided Europe and the world in two sides: the future of inclusive institutions depended of that divide.
