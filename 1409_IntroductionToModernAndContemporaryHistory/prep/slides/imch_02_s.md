*The Great Divergence*

Main topics:
- Kenneth Pomeranz’s challenge to the ideas of European exceptionalism (most famously E.L. Jones)
- Europe and Asia were on an equal footing in the beginning (early- nineteenth century), according to this author
- The Great Divergence is explained by a combination of internal conditions and colonial expansion; one without the other would never lead Europe to break with the Malthusian Trap; China and Japan had the same internal conditions but did not expand externally

Modern economic growth is a recent phenomenon by the standards of mankind. Also, it is not universal, rather on the contrary: modern economic growth was accompanied by a sharp divergence between those economies that entered into the process and those that were unable to do so.

To put it in a simple way, a small number of economies started growing very rapidly, but most of the economies of the world were left behind.

To be more precise, the area of the world that entered into the process of modern economic growth was Western Europe, somewhere between the seventeenth and the eighteenth centuries, followed slightly later by some countries in North America and Australia. The remaining areas of the world were notable for not having done it then, and some have not been able to do it even today. This divergence calls for an explanation.

As we have seen in the previous class, there are authors (such as E.L. Jones) who think that the explanation for European growth lies in a very old differentiation between Europe and the rest of the world in such aspects as institutions, the environment and demography; according to Jones, thanks to these features, Europe was already a richer economy before the seventeenth and eighteenth centuries.

But there are also other authors who suggest that the explanation for European growth lies in the fact that Europeans started exploring other areas outside of Europe, from the period of the Discoveries onwards, and that that offered the continent the necessary means for economic expansion.

Kenneth Pomeranz tries to combine these two explanations. According to this author the part of the colonial exploration was essential, but only because it combined with the internal circumstances of Europe, which were favourable to economic growth.

Pomeranz, however, says that those circumstances, although existing in Europe, were not specific to Europe; it was their combination with colonial exploration that created the difference between Europe and the other parts of the world.

Pomeranz says that the institutions that were favourable to growth, such as developed markets and protection of property rights, were not a European exclusive and could have been found in other parts of the globe, particularly in various parts of Asia.

On the one hand, that type of institutions was already quite developed in Europe much before the eighteenth century (at least since the Middle Ages); on the other hand, it also existed in Asia (in China or Japan, for instance) much before the same period

Besides, according to Pomeranz, the units of analysis normally used open a series of questions: there is very little in common between the Netherlands and the Ukraine, although both are in Europe; and there is more in common between Britain and the delta of the Yangzi river in China.

Additionally, to compare China or India with Britain is not adequate: China and India would be more easily comparable with Europe as a whole; and, as we can compare the Netherlands with the delta of the Yangzi, it is difficult to compare it with other Chinese regions; to sum up, the traditional geographical units of analysis require a reassessment.

Even if you could not find growth-promoting institutions in all of China you could find it in such a region as the Yangtzi delta, very much like you could find those sorts of institutions in Britain but not Europe as a whole.

This means that, according to Pomeranz, it is not at all clear that Europe possessed in the eighteenth century a unique set of features able to explain its future success; many of those features existed elsewhere.

The best comparison is the one we can make between parts of Europe and parts of Asia, not between Europe and Asia as a whole.

According to Pomeranz, and contrary to what E.L. Jones says, there is very little evidence showing that Europe had a higher income than Asia before the eighteenth century; E.L. Jones sustained that such difference was based on a higher capital accumulation in Europe; Pomeranz denies that such a difference existed.

An important argument by E.L. Jones is that in Europe families had adopted since long a demographic regime of low population growth, which granted higher wages and higher capital accumulation.

According to Pomeranz, not only we cannot find higher capital accumulation but we cannot find either a different demographic regime: according to him, for instance, it is possible that the fertility rates in Japan were even lower than in Europe.

This leads to another important aspect that has been presented to explain the industrial revolution, which is that the higher wages existing in Europe, as a consequence of more scarcity of labour, had conducted to higher capital accumulation.

The problem is that, according to Pomeranz, it is not possible to find (as in the cases of fertility and capital accumulation) those higher wages in Europe.

In fact, still according to Pomeranz, Europe faced then strong ecological pressures (deforestation, bad weather, overutilisation of land); without the discovery and exploration of America it would have been impossible to overcome these problems and Europe would have fallen again in the Malthusian Trap.

Thus, according to Pomeranz, Europe had an important set of constraints; this was not what distinguished Europe from Asia.

In the seventeenth century (right before the great European growth) there was no room for further extensive growth in both Europe and Asia: both regions were on the verge of decreasing returns and of entering into a new malthusian episode.

So, in the eighteenth century, Western Europe, China, and Japan were on an equal footing in terms of initial conditions for entering into the process of modern economic growth. But it was Europe that took flight.

This is where the external dimension enters: Europe started using America’s rich resources. America offered:
1. new types of food with a much higher caloric content than it was possible to find in Europe; and
2. industrial raw materials.

Recent data, however, has questioned this picture:
1. Chinese GDP per capita had a declining trend from the fifteenth century onwards, especially pronounced after the eighteenth century; so, precisely at the time Pomeranz thinks there were similar conditions for fast growth in Europe and in Asia, Asia’s richest country displays negative growth
2. What is more, this is happening when the northern countries of Europe are entering a fast pace of growth: this is first true of Holland (fifteenth century) and then of Britain (eighteenth century)
3. From then until the eighteenth century, China is only comparable with the poorest parts of Europe but no longer with the richest ones.
4. The data also show a “Little Divergence” within Europe, making northern countries grow faster than southern ones from the fifteenth century onwards and inverting the relative levels of GDP per capita: southern Europe was richer than northern Europe until the fifteenth century

So, Pomeranz is vindicated in the sense that there was no general great divergence between Europe and Asia, but he is not vindicated in the sense that there are no signs in China of keeping up with what is happening in some parts of Europe
