*Other paths to inclusiveness: North America (and Oceania)*

Main topics:
- Outisde of Europe there were other ways to arrive at the same sort of inclusive institutions that spread throughout Europe
- The most famous case was that of the United States of America, with its 1776 Declaration of Independence and 1787 Constitution
- This set a model that was then followed in the most important parts of the English-speaking world (Canada, Australia, and New Zealand)
- The success of this model has to do with the combination between European liberal ideas and some democratic features of the these colonial societies.

Before looking into the revolution we need to understand some aspects of pre- independence American society and institutions.

The first idea of the English crown and the early colonists in North America was to repeat the exploitative methods followed by the Spanish in Central and South America. The problem for them (and the ultimate luck of the North Americans) was that the English found it impossible to repeat the Spanish approach in the new territories.

The English could not find there such sophisticated civilisations as the Aztecs, the Maya or the Inca, that provided a hierarchical institutional framework on which to lever extraction. They could not find either silver and gold.

We have to understand the following: England was still a second rate international power by the sixteenth and early seventeenth centuries – it was interested in North America because that was what the big powers (Spain and Portugal) had left behind.

Due to the lack of resources, the English failed in their new colonies, such as Virginia, Carolina, Maryland or New Hampshire, the same sort of exploitation of the Spanish further south So, after some initial extractive attempts they ended up by having a composite solution: on the one hand they replicated the extractive institutions of the Caribbean, by importing a large number of American slaves.

But on the other hand they created primitive democratic institutions for the colonists; progressively, each of these colonies reached a government structure where there was a governor on top but where a representative assembly had vast powers. Additionally, elections for these assemblies were open to a large part of the colonist male population (in some of the colonies to all the male population). So a democratic background existed already in America before the revolution and this background was fundamental for the success of liberal and democratic institutions there.

Another thing we need to take into consideration in order to understand the outbreak of the American revolution was that the democratic structure applied only in the colonies – at the scale of the whole empire the American colonies were ruled in an authoritarian way by the English crown.

The process leading to the outbreak of the revolution started because of British involvement in the Seven Years War with France, leading to the same sort of financial problems that afflicted the French crown and led to the outbreak of the French Revolution in 1789.

The British crown had a difficult problem: the strength of the Westminster parliament; so, it tried to circumvent it through the imperial relationship, looking to the American colony as a source of taxes that were politically very difficult to collect in the mainland. “No taxation without Representation” is one of the most famous slogans of the American revolutionaries and expresses the demand by colonial subjects to have their rights on an equal par with those of mainland Britain. Note that it does not express an independentist idea. Indeed, what the American revolutionaries wanted in the beginning was not independence but the full adoption in the colony of those ideas that ruled England since the revolution of the seventeenth century.

The relationship between the English crown and the colonists deteriorated increasingly as the crown kept on asking for more taxes. The tipping moment in which the revolt acquired its independentist nature was the episode known as the Boston Tea Party of 1774. Two years later, in 1776, the American colonists finally declared unilateral independence to Britain.

A war ensued which ended with the colonists’ victory in 1783: in 1787 a Constitution would be signed in Philadelphia and in 1789 Congress would sign a Bill of Rights – the Declaration of Independence, the 1787 Constitution, and the 1789 Bill of Rights are the foundational documents of American inclusive institutions.

Still, some aspects were left to be solved. The most serious of them was, no doubt, that of slavery; this was a major contradiction with the democratic principles of the revolution – a question that lasted practically a century to be solved, with the 1860-65 civil war.

The United States functioned as a framework that was repeated in other British colonies: Canada, Australia and New Zealand
