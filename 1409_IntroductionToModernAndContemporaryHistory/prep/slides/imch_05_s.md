*Inclusive and extractive institutions*

Main topics:
- The crucial aspect giving different countries the advantage in terms of growth and prosperity is the nature of their institutions: if they have inclusive political and economic institutions they will grow and prosper; if they have non-inclusive political institutions and extractive economic institutions they will not grow and prosper
- Secure property rights are necessary but not sufficient; you need furthermore institutions to be inclusive so that everyone feels that his/her efforts are being rewarded
- This requires centralisation of power in a State dedicated to preserve political pluralism and economic inclusiveness.

We will follow closely Daron Acemoglu and James A. Robinson, Why Nations fail, 2012 “Countries differ in their economic success because of their different institutions, the rules influencing how the economy works, and the incentives that motivate people” (Acemoglu and Robinson, 2012, p. 73).

What are “inclusive economic institutions”? Secure property rights are a fundamental element, which comes associated with various ancillary aspects: efficient and enforced law, efficient public services, and freedom to contract and exchange.

But none of this is enough: you can have all these aspects in an exclusive rather than inclusive way. An example: in colonial Latin America and the Caribbean, property rights were secure to large colonial landowners and their economic activities; these could rely on colonial institutions to let markets work and protect their income stream and fortune; as for indigenous people, they could expect to see their income extracted; some of these societies were even slave societies.

These are extractive economic institutions; inclusive economic institutions are the ones that spread the benefits of economic activity through the majority of the population.

Inclusive economic institutions will allow people to start businesses and not reserve business only to certain people in society, will allow workers to move to the jobs where they are more productive and better paid and will allow inefficient firms to go bankrupt and be replaced by efficient firms.

The opposite happens with extractive institutions: firms will be the preserve of a tiny elite, workers will be forced to work in certain activities even if not the best for them and the economy; inefficient firms will be protected by bankruptcy because the institutions protect them independently of providing what society needs or even when they are harmful to society.

Inclusive institutions should also provide the supply of generalised education, which is an essential instrument to technology, a crucial aspect of economic growth.  Education allows for people outside the prevailing elite to acquire some of its instruments – education works thus as a level playing field; but it also works as an engine to attract talent – in non-inclusive institutions, talent outside of the elite would never be noticed; this is important because it enlarges the pool of talent available and is an incentive to technological innovation.

All of this points to the importance of one essential agent, which can be the source of the best and the worst in society: the State; only the State is able to impose order, prevent theft and fraud, enforce contracts, and provide public services. The matter is to put the State functioning in an inclusive or an exclusive way: the State can provide all those things for a small elite or provide them for the generality of the population.

And this depends on the political process and how inclusive political institutions exist or not: inclusive political institutions lead to inclusive economic institutions – as well as exclusive political institutions lead to extractive economic institutions.

The political process is about who will control the State and how the State will function – it’s the State that implements inclusive or extractive institutions. In certain cases the political process results in a closed elite controlling the State for its own benefit; in other cases it results in the State being controlled by an open elite ruling for the benefit of most of the population.

So, a strong State is crucial, but must be a State bent on promoting pluralism and inclusiveness; a strong State not dedicated to that will produce exclusive and extractive institutions:
- “In North Korea and colonial Latin America those who can wield this power will be able to set up economic institutions to enrich themselves and augment their power at the expense of society. In contrast, political institutions that distribute power broadly in society and subject it to constraints are pluralistic. Instead of being vested in a single individual or a narrow group, political power rests with a broad coalition or a plurality of groups” (Acemoglu and Robinson, 2012, p. 80)
- “We will refer to political institutions that are sufficiently centralised and pluralistic as inclusive political institutions. When either of these conditions fails, we will refer to the institutions as extractive political institutions” (Acemoglu and Robinson, 2012, p. 81).

The process leading to inclusive institutions can fail either because the resulting institutions are not inclusive or simply because an efficient State or central power does not result from it. Centralisation may fail to appear because clans, tribes or groups are able to keep a sphere of power in their own interest – without a centralised State there is no law and order.

This leads to one further important difference: you can have extractive institutions generating growth for a while and extractive institutions that are totally unable to provide any form of growth.

Examples:
- Extractive institutions can generate growth when elites directly allocate resources to high-productivity sectors that they control: for example, the sugar-producing, slave-based economies of the Caribbean in seventeenth and eighteenth centuries – the rights of the elite were protected but not of the majority of the population, who were slaves; another example is the Soviet Union in the twentieth century
- The problem of these places was that at a certain moment there was a need to switch to other activities in order to keep prosperity, and the elites failed to spot what these activities were: growth was not sustained
- Then there is the case of extractive institutions that do not generate any form of growth; this happens when they are not well established: say Somalia in the twenty-first century.

Finally, we get to one last element of importance: that of critical junctures, i.e. moments in which existing institutions are destroyed and there is the possibility of building new ones: either these new institutions are inclusive or they are exclusive. In these critical junctures a large role is played by contingency: no future is preordained, everything depends on historical events that can go in one direction or another, depending on the relative strength of the forces in presence and on the outcome of political confrontation. And the fact that they are one or the other leads to an historical path that is crucial for future long-term developments.
