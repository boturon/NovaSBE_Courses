*Japan in the twentieth century: a different path in Asia*

Main topics:
- Japan had already been put on a path of westernized inclusive institutions since the Meiji Restoration of the second half of the nineteenth century
- But a new wave of reforms would come at the end of World War II, thanks to Japan´s military defeat: the American occupation authorities introduced a series of democratization political and economic reforms that made of Japan the first truly democratic non-Western country
- This was the institutional environment that allowed for the post-war Japanese “economic miracle” – thanks to it, Japan became one of the most developed countries in the world.

The institutional changes in the second half of the nineteenth century – the Meiji Restoration of 1868 and subsequent reforms – made of Japan a very different country in Asia, a country modeled on European institutions, that was able to deal with the Western challenge, contrary to (for instance) China.

In the last decades of the nineteenth century and the first half of the twentieth century, Japan became a regional superpower, successfully defeating China and Russia in two wars that gave the country an edge in the region.

In 1894-95, victory over China gave Japan control of Taiwan, and in 1905 victory over Russia gave Japan the control over a large part of Manchuria, over Korea and the Sakhalin islands.

This was the beginning of a vast expansion throughout Asia, especially during the 1930s and, then, World War II. This was accompanied by the transformation of Japan into a sort of authoritarian regime along the model of the European fascist regimes.

Authoritarianism, nationalism and imperialism were parts of the same package, related to to the 1930s crisis and the protectionist policies followed by Western countries: as the former champions of free trade and globalization became increasingly more protectionist, Japan followed suit and tried to build an Asian empire in order to be economically autonomous. The process reached its maximum point during World War II when, due to the weakness of the Western empires, Japan occupied large amounts of southeast Asia: in 1937, Japan declared war on China and in 1940 it signed the Axis Pact with fascist Germany and Italy.

Although these changes in the first half of the twentieth century put Japan on a path of extractive institutions, they still show the singular nature of Japan’s path, quite different from the rest of Asia and very similar to Europe. All of this would end up in disaster for Japan with its defeat in World War II – but this was defeat that would have long-term positive consequences.

In December 1941 Japan made the US enter the war after the famous Pearl Harbor attack; in August 1945 the US defeated Japan, in the sequence of a series of military advances in Japanese territory and the launching of two atomic bombs over two Japanese cities: Hiroshima and Nagasaki.The US occupied Japan and introduced a series of sweeping reforms that, once again, changed radically Japanese institutions.

The American occupation authorities drafted a new constitution in February 1946, which was approved by the old parliament in 1947.

Japan became a truly democratic country thanks to this new constitution:
- The emperor became simply a symbol of the unity of the state, without any real political power
- All adults, male and female, were allowed to vote (male adults could vote since 1925, although the authoritarian regime of the 1930s had limited political freedoms)
- Furthermore, the constitution prevented Japan from having a standing army (although it could organize the so-called self-defense forces).

All this was accompanied by a series of additional reforms in the economic and social system.

These reforms included a land reform: according to this reform the traditional landlords, many of them still holding that position from shogun days, were expropriated at low price and the land sold, also at low price, to small tenants: this made the structure of society much more equalitarian, creating a vast agricultural middle class and eliminating the old dominating class. They also included a labour reform: workers could now organise in unions and engage in collective bargaining – the occupation authorities stimulated the creation of various unions, which gained an increasing role in company management. They also included a company reform, by destruction of the old zaibatsu (or business groups), which came to dominate most of the economy during the authoritarian period, and by introducing anti-monopolist laws.

All of these reforms made of Japan a truly democratic country for which there was no equivalent outside the Western world – this means that, after the transformations of the Meiji period, Japan gave one step further by the reforms (a true revolution, in reality) brought by the American occupation authorities.

In 1952 full sovereignty was given back to Japan (San Francisco Peace Treaty) and the country was ready for its economic “miracle” which made of it the first non- Western country to reach the same level of wealth and development typical of Western countries. Again, this is very much connected with the fact that, although through a very convoluted story, Japan acquired really inclusive institutions at the beginning of the post-war period.

Since the 1990s some distressing signs have appeared,with a slowdown that worries the Japanese, but things have to be put in perspective: Japan is still much on top – slowing down is not colapse. Standards of development in Japan are still among the highest in the world.
