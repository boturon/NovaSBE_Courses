*The Black Death: an European critical juncture*

Main topics:
- Critical junctures are moments in history that lead parts of the world to change radically: the result is indeterminate and can be positive or negative
- The plague known as the Black death in the mid-fourteenth century was one of those critical junctures: in Western Europe it led to the improvement of the conditions and power of peasants, to the detriment of feudal lords; in Eastern Europe it led to the opposite
- The fifteenth century European overseas expansion was another critical juncture: in Iberian countries it led to the reinforcement of the crown’s power; in Britain to the reinforcement of civil society
- This determined the future of these different areas of Europe.

The situation of Europe at the point where we started analysing Chinese history, i.e. about the year 1,000, was one of delayed recovery from the disappearance of the Roman Empire in the fourth century, the chaos that followed and the various foreign invasions that made of it the prey of outsiders. There were no clear centralised political units (such as the current nation-states) and the stronger political groups were still the feudal lords that gained ascendancy in the previous centuries.

The situation of medieval Europe (the period ranging basically from the year 1,000 to the fifteenth century) was, thus, one of moveable frontiers and constant military conflict between fragile kings and warrying feudal lords. All of this was very different from China’s Golden Age of the Song Dynasty.

The institutions of Europe were not, of course, inclusive political institutions.

The basic feudal structure prevailing in Europe was more or less the following (with some variations depending on the geographical area we are talking about): On top we had a king (or queen), who normally was the head of the most powerful noble family in the territory – the power of the king was not clear and he had to fight against other kings but also against the nobles existing in the territory he ruled.

When not fighting, the power of the king rested on an hierarchical structure: below the king there were the big noble houses, and the king related to them, and they related between themselves, along oaths and the concession of territory: conceding territory forced the beneficiaries to remain faithful to the conceding power in military services.

The lords then allocated lands to peasants, who had to perform various tasks to the lords: pay tributes and, many times, unpaid labour. Unpaid labour received the name of serfdom, and the serfs (a large fraction of the population) were tied to the land, as they were unable to move elsewhere without the lord’s permission. The lord was, furthermore, the judge, the jury, and the police force of the territory.

It was in this context that the Black Death appeared.


# Black Death

The Black Death was a plague that spread throughout Europe in the second half of the fourteenth century: its first manifestations in Europe occurred in 1348.

The Black Death claimed the life of about one third of the European population, but in many places the death toll was of about 50% and in some cases 100%: there is no doubt of the enormity of its impact.

Besides many other impacts, the Back Death had a drastic impact in the society of the time: the massive scarcity of labour created by the plague undermined the social order:
- peasants gained power as lords needed them for work;
- peasants started asking for the reduction or even suppression of the many tributes they had to pay as well as increased wages.

In England, for instance, “feudal labour services dwindled away, and inclusive labour markets began to emerge, and wages rose” (Acemoglu and Robinson,2012, pp. 99-100).

This is where we have a critical juncture: despite having had a similar impact across Europe, the truth is that the Black Death did not have the same outcome in the various parts of Europe.

The outcome in Eastern Europe was quite different: much as in Western Europe peasants asked for the amelioration of their conditions; but the result was not the same • Workers were not as strong, so, after the plague, landlords increased their land possessions, and serfdom, rather than retract, increased – this is the so-called “second serfdom” age in Europe, separating West from East.

In the West, workers became progressively free from feudal tributes and inserted in a booming market economy, in the East they became even more exploited: similar initial conditions led to radically different outcomes, just because workers were better organised in one place while landlords were in another.

"The Black Death is a vivid example of a critical juncture, a major event or confluence of factors disrupting the existing economic or political balance in society. A critical juncture is a double-edged sword that can cause a sharp turn in the trajectory of a nation. On the one hand it can open the way for breaking the cycle of extractive institutions and enable more inclusive ones to emerge, as in England. Or it can intensify the emergence of extractive institutions, as was the case with the Second Serfdom in Eastern Europe" (Acemoglu and Robinson, 2012, p. 101).

But this was not the end of the story, for if a differentiation between West and East was formed, another differentiation between North and South within the West also appeared. One of the most important processes generating in Europe just a few years after the Black Death was the Discoveries, pioneered by the Portuguese, closely followed by the Spanish.


# Discoveries

It all started in 1415, when the Portuguese reached Ceuta – and in a matter of just a few years the Portuguese had explored all the coast of Africa and reached Asia and South America (india: 1498; Brazil: 1500); the Spanish reached North America in 1492. This was no doubt a display of dynamism on the part of Southern Europe.

But this was again a critical juncture and a double-edged sword, which ended up by reinforcing the king’s power to the detriment of the rest of society: feudal lords never regained their former power but the king, instead, acquired an enormous new amount of resources that centralised too much power in the king, thanks to the regal monopoly over overseas transactions. Thanks to this, the Iberian monarchs were able to increase their power over the rest of society.

A situation (the overseas expansion) that could have given the Iberian countries an edge over the rest of Europe turned out to be the moment in which they also lost their position in terms of wealth and development, in favour of Northern powers.

It is also the moment in which such Northern powers overtake the great power of old: China.
