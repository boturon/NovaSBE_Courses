*Aftermath of World War I, Wikipedia*


The Aftermath of World War I saw drastic political, cultural, economic, and social change across Eurasia (Europe and Asia), Africa, and even in areas outside those that were directly involved. Four empires collapsed due to the war, old countries were abolished, new ones were formed, boundaries were redrawn, international organizations were established, and many new and old ideologies took a firm hold in people's minds.

World War I also had the effect of bringing political transformation to most of the principal parties involved in the conflict, transforming them into electoral democracies by bringing near-universal suffrage for the first time in history, as in Germany (1919 German federal election), Great Britain (1918 United Kingdom general election), and Turkey (1923 Turkish general election).


# Blockade of Germany

Through the period from the armistice on 11 November 1918 until the signing of the peace treaty with Germany on 28 June 1919, the Allies maintained the naval blockade of Germany that had begun during the war. As Germany was dependent on imports, it is estimated that 523,000 civilians had lost their lives.[1] N. P. Howard, of the University of Sheffield, claims that a further quarter of a million more died from disease or starvation in the eight-month period following the conclusion of the conflict.[2] The continuation of the blockade after the fighting ended, as author Robert Leckie wrote in Delivered From Evil, did much to "torment the Germans ... driving them with the fury of despair into the arms of the devil." The terms of the Armistice did allow food to be shipped into Germany, but the Allies required that Germany provide the means (the shipping) to do so. The German government was required to use its gold reserves, being unable to secure a loan from the United States.

Marks states that despite the problems facing the Allies, from the German government, "Allied food shipments arrived in Allied ships before the charge made at Versailles".[3] This position is also supported by Elisabeth Gläser who notes that an Allied task force, to help feed the German population, was established in early 1919 and that by May 1919 " Germany [had] became the chief recipient of American and Allied food shipments". Gläser further claims that during the early months of 1919, while the main relief effort was being planned, France provided food shipments to Bavaria and the Rhineland. She further claims that the German government delayed the relief effort by refusing to surrender their merchant fleet to the Allies. Finally, she concludes that "the very success of the relief effort had in effect deprived the [Allies] of a credible threat to induce Germany to sign the Treaty of Versailles.[4] However, it is also the case that for eight months following the end of hostilities, the blockade was continually in place, with some estimates that a further 100,000 casualties among German civilians due to starvation were caused, on top of the hundreds of thousands which already had occurred. Food shipments, furthermore, had been entirely dependent on Allied goodwill, causing at least in part the post-hostilities irregularity.


# Treaty of Versailles

After the Paris Peace Conference of 1919, the signing of the Treaty of Versailles on 28 June 1919, between Germany on the one side and France, Italy, Britain and other minor allied powers on the other, officially ended war between those countries. Other treaties ended the relationships of the United States and the other Central Powers. Included in the 440 articles of the Treaty of Versailles were the demands that Germany officially accept responsibility for starting the war and pay economic reparations. The treaty drastically limited the German military machine: German troops were reduced to 100,000 and the country was prevented from possessing major military armaments such as tanks, warships, armored vehicles and submarines.


# Ethnic minorities

The dissolution of the German, Russian, Austro-Hungarian and Ottoman empires created a number of new countries in eastern Europe and the Middle East.[11] Some of them, such as Czechoslovakia and Poland, had substantial ethnic minorities who were sometimes not fully satisfied with the new boundaries that cut them off from fellow ethnics.


# Political upheavals

## New nations break free

German and Austrian forces in 1918 defeated the Russian armies, and the new communist government in Moscow signed the Treaty of Brest-Litovsk in March 1918. In that treaty, Russia renounced all claims to Estonia, Finland, Latvia, Lithuania, Ukraine, and the territory of Congress Poland, and it was left to Germany and Austria-Hungary "to determine the future status of these territories in agreement with their population."

## Revolutions

A far-left and often explicitly Communist revolutionary wave occurred in several European countries in 1917–1920, notably in Germany and Hungary. The single most important event precipitated by the privations of World War I was the Russian Revolution of 1917.

## Germany

On 28 June 1919 the Weimar Republic was forced, under threat of continued Allied advance, to sign the Treaty of Versailles. Germany viewed the one-sided treaty as a humiliation and as blaming it for the entire war. While the intent of the treaty was to assign guilt to Germany to justify financial reparations, the notion of blame took root as a political issue in German society and was never accepted by nationalists, although it was argued by some, such as German historian Fritz Fischer. The German government disseminated propaganda to further promote this idea.

In order to finance the purchases of foreign currency required to pay off the reparations, the new German republic printed tremendous amounts of money – to disastrous effect. Hyperinflation plagued Germany between 1921 and 1923. In this period the worth of fiat Papiermarks with respect to the earlier commodity Goldmarks was reduced to one trillionth (one million millionth) of its value.

Germany saw relatively small amounts of territory transferred to Denmark, Czechoslovakia, and Belgium, a larger amount to France (including the temporary French occupation of the Rhineland) and the greatest portion as part of a reestablished Poland. Germany's overseas colonies were divided between a number of Allied countries, most notably the United Kingdom in Africa, but it was the loss of the territory that composed the newly independent Polish state, including the German city of Danzig and the separation of East Prussia from the rest of Germany, that caused the greatest outrage. Nazi propaganda would feed on a general German view that the treaty was unfair – many Germans never accepted the treaty as legitimate, and lent their political support to Adolf Hitler.

## Russian Empire

The Soviet Union benefited from Germany's loss, as one of the first terms of the armistice was the abrogation of the Treaty of Brest-Litovsk. At the time of the armistice Russia was in the grips of a civil war which left more than seven million people dead and large areas of the country devastated. The nation as a whole suffered socially and economically. As to her border territories, Lithuania, Latvia and Estonia gained independence. They were occupied again by the Soviet Union in 1940.

## Great Britain

Britain, funding the war had a severe economic cost. From being the world's largest overseas investor, it became one of its biggest debtors with interest payments forming around 40% of all government spending. Inflation more than doubled between 1914 and its peak in 1920, while the value of the Pound Sterling (consumer expenditure[17]) fell by 61.2%. Reparations in the form of free German coal depressed local industry, precipitating the 1926 General Strike.

Less concrete changes include the growing assertiveness of Commonwealth nations. Battles such as Gallipoli for Australia and New Zealand, and Vimy Ridge for Canada led to increased national pride and a greater reluctance to remain subordinate to Britain, leading to the growth of diplomatic autonomy in the 1920s. These battles were often decorated in propaganda in these nations as symbolic of their power during the war. Colonies such as the British Raj (India) and Nigeria also became increasingly assertive because of their participation in the war. The populations in these countries became increasingly aware of their own power and Britain's fragility.

After World War I women gained the right to vote as, during the war, they had had to fill-in for what were previously categorized as "men's jobs", thus showing the government that women were not as weak and incompetent as they thought. Also, there were several significant developments in medicine and technology as the injured had to be cared for and there were several new illnesses that medicine had to deal with.

## United States

While disillusioned by the war, it having not achieved the high ideals promised by President Woodrow Wilson, American commercial interests did finance Europe's rebuilding and reparation efforts in Germany, at least until the onset of the Great Depression. American opinion on the propriety of providing aid to Germans and Austrians was split.

Gott argued that relief should first go to citizens of countries that had suffered at the hands of the Central Powers, while Osner made an appeal for a more universal application of humanitarian ideals.[21] The American economic influence allowed the Great Depression to start a domino effect, pulling Europe in as well.

## France

Also extremely important in the War was the participation of French colonial troops, including the Senegalese tirailleurs, and troops from Indochina, North Africa, and Madagascar. When these soldiers returned to their homelands and continued to be treated as second class citizens, many became the nuclei of pro-independence groups.

Furthermore, under the state of war declared during the hostilities, the French economy had been somewhat centralized in order to be able to shift into a "war economy", leading to a first breach with classical liberalism.

Finally, the socialists' support of the National Union government (including Alexandre Millerand's nomination as Minister of War) marked a shift towards the French Section of the Workers' International's (SFIO) turn towards social democracy and participation in "bourgeois governments", although Léon Blum maintained a socialist rhetoric.

## Italy

In Italy, the discontent was relevant: Irredentism (see: irredentismo) claimed Fiume and Dalmatia as Italian lands; many felt the Country had taken part in a meaningless war without getting any serious benefits. This idea of a "mutilated victory" (vittoria mutilata) was the reason which led to the Impresa di Fiume ("Fiume Exploit"). On September 12, 1919, the nationalist poet Gabriele d'Annunzio led around 2,600 troops from the Royal Italian Army (the Granatieri di Sardegna), nationalists and irredentists, into a seizure of the city, forcing the withdrawal of the inter-Allied (American, British and French) occupying forces.

The "mutilated victory" (vittoria mutilata) became an important part of Italian Fascism propaganda

## China

The western Allies' substantial accession to Japan's territorial ambitions at China's expense led to the May Fourth Movement in China, a social and political movement that had profound influence over subsequent Chinese history. The May Fourth Movement is often cited as the birth of Chinese nationalism, and both the Kuomintang and Chinese Communist Party consider the Movement to be an important period in their own histories.

## Social trauma

This social trauma made itself manifest in many different ways. Some people were revolted by nationalism and what they believed it had caused, so they began to work toward a more internationalist world through organizations such as the League of Nations. Pacifism became increasingly popular. Others had the opposite reaction, feeling that only military strength could be relied upon for protection in a chaotic and inhumane world that did not respect hypothetical notions of civilization. Certainly a sense of disillusionment and cynicism became pronounced. Nihilism grew in popularity. Many people believed that the war heralded the end of the world as they had known it, including the collapse of capitalism and imperialism. Communist and socialist movements around the world drew strength from this theory, enjoying a level of popularity they had never known before. These feelings were most pronounced in areas directly or particularly harshly affected by the war, such as central Europe, Russia and France. 
