*Cold War, Wikipedia*

The Cold War was a period of geopolitical tension between the Soviet Union with its satellite states (the Eastern Bloc), and the United States with its allies (the Western Bloc) after World War II. A common historiography of the conflict begins between 1946, the year U.S. diplomat George F. Kennan's "Long Telegram" from Moscow cemented a U.S. foreign policy of containment of Soviet expansionism threatening strategically vital regions, and the Truman Doctrine of 1947, and ending between the Revolutions of 1989, which ended communism in Eastern Europe as well as in other areas, and the 1991 collapse of the USSR, when nations of the Soviet Union abolished communism and restored their independence. The term "cold" is used because there was no large-scale fighting directly between the two sides, but they each supported major regional conflicts known as proxy wars. The conflict split the temporary wartime alliance against Nazi Germany and its allies, leaving the USSR and the US as two superpowers with profound economic and political differences. 

The capitalist West was led by the United States, a federal republic with a two-party presidential system, as well as the other First World nations of the Western Bloc that were generally liberal democratic with a free press and independent organizations, but were economically and politically entwined with a network of banana republics and other authoritarian regimes, most of which were the Western Bloc's former colonies.

Some major Cold War frontlines such as Indochina, Indonesia, and the Congo were still Western colonies in 1947.

The Soviet Union, on the other hand, was a self-proclaimed Marxist–Leninist state that imposed a totalitarian regime that was led by a small committee, the Politburo. The Party had full control of the state, the press, the military, the economy, and local organizations throughout the Second World, including the Warsaw Pact and other satellites. The Kremlin funded communist parties around the world, but was challenged for control by Mao's China following the Sino-Soviet split of the 1960s. As nearly all the colonial states achieved independence 1945-1960, they became Third World battlefields in the Cold War.

India, Indonesia and Yugoslavia took the lead in promoting neutrality with the Non-Aligned Movement, but it never had much power in its own right. The Soviet Union and the United never engaged directly in full-scale armed combat. However, both were heavily armed in preparation for a possible all-out nuclear world war.

The first phase of the Cold War began in the first two years after the end of the Second World War in 1945. The USSR consolidated its control over the states of the Eastern Bloc, while the United States began a strategy of global containment to challenge Soviet power, extending military and financial aid to the countries of Western Europe (for example, supporting the anti-communist side in the Greek Civil War) and creating the NATO alliance.

The Berlin Blockade (1948–49) was the first major crisis of the Cold War. With the victory of the Communist side in the Chinese Civil War and the outbreak of the Korean War (1950–1953), the conflict expanded. The USSR and the US competed for influence in Latin America and the decolonizing states of Africa and Asia. The Soviets suppressed the Hungarian Revolution of 1956. The expansion and escalation sparked more crises, such as the Suez Crisis (1956), the Berlin Crisis of 1961, and the Cuban Missile Crisis of 1962, which was perhaps the closest the two sides came to nuclear war.

The peace movement, and in particular the anti-nuclear movement, gained pace and popularity from the late 1950s and early 1960s, and continued to grow through the '70s and '80s with large protest marches, demonstrations, and various non-parliamentary activism opposing war and calling for global nuclear disarmament.

Following the Cuban Missile Crisis, a new phase began that saw the Sino-Soviet split complicate relations within the Communist sphere, while US allies, particularly France, demonstrated greater independence of action. The USSR crushed the 1968 Prague Spring liberalization program in Czechoslovakia, while the US experienced internal turmoil from the civil rights movement and opposition to the Vietnam War (1955–75), which ended with the defeat of the US-backed Republic of Vietnam, prompting further adjustments.

By the 1970s, both sides had become interested in making allowances in order to create a more stable and predictable international system, ushering in a period of détente that saw Strategic Arms Limitation Talks and the US opening relations with the People's Republic of China as a strategic counterweight to the Soviet Union. Détente collapsed at the end of the decade with the beginning of the Soviet–Afghan War in 1979. The early 1980s were another period of elevated tension, with the Soviet downing of KAL Flight 007 and the "Able Archer" NATO military exercises, both in 1983. The United States increased diplomatic, military, and economic pressures on the Soviet Union, at a time when the communist state was already suffering from economic stagnation.

In the mid-1980s, the new Soviet leader Mikhail Gorbachev introduced the liberalizing reforms of perestroika ("reorganization", 1987) and glasnost ("openness", c. 1985) and ended Soviet involvement in Afghanistan. Pressures for national independence grew stronger in Eastern Europe, especially Poland. Gorbachev meanwhile refused to use Soviet troops to bolster the faltering Warsaw Pact regimes as had occurred in the past. The result in 1989 was a wave of revolutions that peacefully (with the exception of the Romanian Revolution) overthrew all of the communist regimes of Central and Eastern Europe. The Communist Party of the Soviet Union itself lost control and was banned following an abortive coup attempt in August 1991. This in turn led to the formal dissolution of the USSR in December 1991 and the collapse of communist regimes in other countries such as Mongolia, Cambodia, and South Yemen. The United States remained as the world's only superpower.

Meanwhile, a renewed state of tension between the Soviet Union's successor state, Russia, and the United States in the 2010s (including its Western allies) and growing tension between an increasingly powerful China and the U.S. and its Western Allies have each been referred to as the Second Cold War.


# Background

## Russian Revolution

While most historians trace the origins of the Cold War to the period immediately following World War II, others argue that it began with the October Revolution in Russia in 1917 when the Bolsheviks took power.[10] In 1919 Vladimir Lenin stated that his new state was surrounded by a "hostile capitalist encirclement", and he viewed diplomacy as a weapon that should be used to keep the Soviet Union's enemies divided, beginning with the establishment of the Communist International, which called for revolutionary upheavals abroad.

## Beginnings of World War II

In the late 1930s, Stalin had worked with Foreign Minister Maxim Litvinov to promote popular fronts with capitalist parties and governments to oppose fascism. The Soviets were embittered when Western governments chose to practice appeasement with Nazi Germany instead. In March 1939 Britain and France—without consulting the USSR—granted Hitler control of much of Czechoslovakia at the Munich Agreement. Facing an aggressive Japan at Russia's borders as well, Stalin changed directions and replaced Litvinov with Vyacheslav Molotov, who negotiated closer relations with Germany.

In June 1940, the Soviet Union forcibly annexed Estonia, Latvia, and Lithuania,[23] and the disputed Romanian regions of Bessarabia, Northern Bukovina, and Hertza. But after the German Army invaded the Soviet Union in June 1941 and the Japanese bombed Pearl Harbor in December 1941, the Soviet Union and the Allied powers formed an alliance of convenience. Britain signed a formal alliance and the United States made an informal agreement. In wartime, the United States supplied Britain, the Soviet Union and other Allied nations through its Lend-Lease Program.[27] However, Stalin remained highly suspicious, and he believed that the British and the Americans had conspired to ensure that the Soviets bore the brunt of the fighting against Germany. According to this view, the Western Allies had deliberately delayed opening a second anti-German front in order to step in at the last minute and shape the peace settlement. Thus, Soviet perceptions of the West left a strong undercurrent of tension and hostility between the Allied powers.


# End of World War II (1945–1947)

## Wartime conferences regarding post-war Europe

The Allies disagreed about how the European map should look, and how borders would be drawn, following the war.

The Soviet Union sought to dominate the internal affairs of countries in its border regions.[29][32] During the war, Stalin had created special training centers for communists from different countries so that they could set up secret police forces loyal to Moscow as soon as the Red Army took control. Soviet agents took control of the media, especially radio; they quickly harassed and then banned all independent civic institutions, from youth groups to schools, churches and rival political parties.[33] Stalin also sought continued peace with Britain and the United States, hoping to focus on internal reconstruction and economic growth.

In the American view, Stalin seemed a potential ally in accomplishing their goals, whereas in the British approach Stalin appeared as the greatest threat to the fulfillment of their agenda. With the Soviets already occupying most of Central and Eastern Europe, Stalin was at an advantage, and the two western leaders vied for his favors.

Some historians have argued that the Cold War began when the US negotiated a separate peace with Nazi SS General Karl Wolff in northern Italy. The Soviet Union was not allowed to participate and the dispute led to heated correspondence between Franklin Roosevelt and Stalin. General Wolff, a war criminal, appears to have been guaranteed immunity at the Nuremberg trials by Office of Strategic Services (OSS) commander (and later CIA director) Allen Dulles when they met in March 1945. Wolff and his forces were being considered to help implement Operation Unthinkable, a secret plan to invade the Soviet Union which Winston Churchill advocated during this period.

## Potsdam Conference and surrender of Japan

At the Potsdam Conference, which started in late July after Germany's surrender, serious differences emerged over the future development of Germany and the rest of Central and Eastern Europe.[47] Moreover, the participants' mounting antipathy and bellicose language served to confirm their suspicions about each other's hostile intentions, and to entrench their positions.[48] At this conference Truman informed Stalin that the United States possessed a powerful new weapon. The US had invited Britain into its atomic bomb project but kept it secret from the Soviet Union.


# Containment and the Truman Doctrine (1947–1953)

## Iron Curtain, Iran, Turkey, and Greece

In late February 1946, George F. Kennan's "Long Telegram" from Moscow to Washington helped to articulate the US government's increasingly hard line against the Soviets, which would become the basis for US strategy toward the Soviet Union for the duration of the Cold War. The Truman Administration was receptive to the telegram due to broken promises by Stalin concerning Europe and Iran.[

## Marshall Plan and Czechoslovak coup d'état

In early 1947, France, Britain and the United States unsuccessfully attempted to reach an agreement with the Soviet Union for a plan envisioning an economically self-sufficient Germany, including a detailed accounting of the industrial plants, goods and infrastructure already removed by the Soviets.[88] In June 1947, in accordance with the Truman Doctrine, the United States enacted the Marshall Plan, a pledge of economic assistance for all European countries willing to participate, including the Soviet Union.

The plan's aim was to rebuild the democratic and economic systems of Europe and to counter perceived threats to Europe's balance of power, such as communist parties seizing control through revolutions or elections.

Stalin believed that economic integration with the West would allow Eastern Bloc countries to escape Soviet control, and that the US was trying to buy a pro-US re-alignment of Europe.[92] Stalin therefore prevented Eastern Bloc nations from receiving Marshall Plan aid.[92] The Soviet Union's alternative to the Marshall Plan, which was purported to involve Soviet subsidies and trade with central and eastern Europe, became known as the Molotov Plan (later institutionalized in January 1949 as the Council for Mutual Economic Assistance).

## Beginnings of NATO and Radio Free Europe

Britain, France, the United States, Canada and other eight western European countries signed the North Atlantic Treaty of April 1949, establishing the North Atlantic Treaty Organization (NATO).[63] That August, the first Soviet atomic device was detonated in Semipalatinsk, Kazakh SSR.[81] Following Soviet refusals to participate in a German rebuilding effort set forth by western European countries in 1948,[110][119] the US, Britain and France spearheaded the establishment of West Germany from the three Western zones of occupation in April 1949.[120] The Soviet Union proclaimed its zone of occupation in Germany the German Democratic Republic that October.

## Chinese Civil War and SEATO

In 1949, Mao Zedong's People's Liberation Army defeated Chiang Kai-shek's United States-backed Kuomintang (KMT) Nationalist Government in China, and the Soviet Union promptly created an alliance with the newly formed People's Republic of China.

## Korean War

One of the more significant examples of the implementation of containment was US intervention in the Korean War. In June 1950, Kim Il-sung's North Korean People's Army invaded South Korea.

To Stalin's surprise,[81] the UN Security Council backed the defense of South Korea, though the Soviets were then boycotting meetings in protest that Taiwan and not Communist China held a permanent seat on the Council.


# Crisis and escalation (1953–1962)

## Berlin ultimatum and European integration

During November 1958, Khrushchev made an unsuccessful attempt to turn all of Berlin into an independent, demilitarized "free city". He gave the United States, Great Britain, and France a six-month ultimatum to withdraw their troops from the sectors they still occupied in West Berlin, or he would transfer control of Western access rights to the East Germans. Khrushchev earlier explained to Mao Zedong that "Berlin is the testicles of the West. Every time I want to make the West scream, I squeeze on Berlin."[163] NATO formally rejected the ultimatum in mid-December and Khrushchev withdrew it in return for a Geneva conference on the German question.[164]

More broadly, one hallmark of the 1950s was the beginning of European integration—a fundamental by-product of the Cold War. Truman and Eisenhower promoted the concept politically, economically, and militarily, but later administrations viewed it ambivalently, fearful that an independent Europe would forge a separate détente with the Soviet Union, which would use this to exacerbate Western disunity.

## Competition in the Third World

Nationalist movements in some countries and regions, notably Guatemala, Indonesia and Indochina, were often allied with communist groups or perceived in the West to be allied with communists.[91] In this context, the United States and the Soviet Union increasingly competed for influence by proxy in the Third World as decolonization gained momentum in the 1950s and early 1960s.[166] Additionally, the Soviets saw continuing losses by imperial powers as presaging the eventual victory of their ideology.[167] Both sides were selling armaments to gain influence.

## Sino-Soviet split

The period after 1956 was marked by serious setbacks for the Soviet Union, most notably the breakdown of the Sino-Soviet alliance, beginning the Sino-Soviet split. Mao had defended Stalin when Khrushchev criticized him in 1956, and treated the new Soviet leader as a superficial upstart, accusing him of having lost his revolutionary edge.[182] For his part, Khrushchev, disturbed by Mao's glib attitude toward nuclear war, referred to the Chinese leader as a "lunatic on a throne".

## Berlin Crisis of 1961

he Berlin Crisis of 1961 was the last major incident in the Cold War regarding the status of Berlin and post–World War II Germany. By the early 1950s, the Soviet approach to restricting emigration movement was emulated by most of the rest of the Eastern Bloc.[194] However, hundreds of thousands of East Germans annually emigrated to West Germany through a "loophole" in the system that existed between East Berlin and West Berlin, where the four occupying World War II powers governed movement.


# Confrontation through détente (1962–1979)

In the course of the 1960s and 1970s, Cold War participants struggled to adjust to a new, more complicated pattern of international relations in which the world was no longer divided into two clearly opposed blocs.[91] From the beginning of the post-war period, Western Europe and Japan rapidly recovered from the destruction of World War II and sustained strong economic growth through the 1950s and 1960s, with per capita GDPs approaching those of the United States, while Eastern Bloc economies stagnated.


# "Second Cold War" (1979–1985)

The term second Cold War refers to the period of intensive reawakening of Cold War tensions and conflicts in the late 1970s and early 1980s. Tensions greatly increased between the major powers with both sides becoming more militaristic.[267] Diggins says, "Reagan went all out to fight the second cold war, by supporting counterinsurgencies in the third world."[268] Cox says, "The intensity of this 'second' Cold War was as great as its duration was short."


# Final years (1985–1991)

## Gorbachev's reforms

By the time the comparatively youthful Mikhail Gorbachev became General Secretary in 1985,[278] the Soviet economy was stagnant and faced a sharp fall in foreign currency earnings as a result of the downward slide in oil prices in the 1980s.[311] These issues prompted Gorbachev to investigate measures to revive the ailing state.

Despite initial skepticism in the West, the new Soviet leader proved to be committed to reversing the Soviet Union's deteriorating economic condition instead of continuing the arms race with the West.[148][313] Partly as a way to fight off internal opposition from party cliques to his reforms, Gorbachev simultaneously introduced glasnost, or openness, which increased freedom of the press and the transparency of state institutions.[314] Glasnost was intended to reduce the corruption at the top of the Communist Party and moderate the abuse of power in the Central Committee.[315] Glasnost also enabled increased contact between Soviet citizens and the western world, particularly with the United States, contributing to the accelerating détente between the two nations.
