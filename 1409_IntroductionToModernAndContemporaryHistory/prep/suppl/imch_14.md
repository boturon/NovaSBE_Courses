*Nazism, Wikipedia*

National Socialism (German: Nationalsozialismus), more commonly known as Nazism (/ˈnɑːtsiɪzəm, ˈnæt-/),[1] is the ideology and practices associated with the Nazi Party—officially the National Socialist German Workers' Party (Nationalsozialistische Deutsche Arbeiterpartei or NSDAP)—in Nazi Germany, and of other far-right groups with similar aims.

Nazism is a form of fascism and showed that ideology's disdain for liberal democracy and the parliamentary system, but also incorporated fervent antisemitism, anti-communism, scientific racism, and eugenics into its creed. Its extreme nationalism came from Pan-Germanism and the Völkisch movement prominent in the German nationalism of the time, and it was strongly influenced by the Freikorps paramilitary groups that emerged after Germany's defeat in World War I, from which came the party's "cult of violence" which was "at the heart of the movement."

Nazism subscribed to pseudo-scientific theories of racial hierarchy and Social Darwinism, identifying the Germans as a part of what the Nazis regarded as an Aryan or Nordic master race.[3] It aimed to overcome social divisions and create a German homogeneous society based on racial purity which represented a people's community (Volksgemeinschaft). The Nazis aimed to unite all Germans living in historically German territory, as well as gain additional lands for German expansion under the doctrine of Lebensraum and exclude those who they deemed either community aliens or "inferior" races.

The term "National Socialism" arose out of attempts to create a nationalist redefinition of "socialism", as an alternative to both Marxist international socialism and free market capitalism. Nazism rejected the Marxist concepts of class conflict and universal equality, opposed cosmopolitan internationalism, and sought to convince all parts of the new German society to subordinate their personal interests to the "common good", accepting political interests as the main priority of economic organization.[4]

The Nazi Party's precursor, the Pan-German nationalist and antisemitic German Workers' Party, was founded on 5 January 1919. By the early 1920s the party was renamed the National Socialist German Workers' Party—to attract workers away from left-wing parties such as the Social Democrats (SPD) and the Communists (KPD)—and Adolf Hitler assumed control of the organization. The National Socialist Program or "25 Points" was adopted in 1920 and called for a united Greater Germany that would deny citizenship to Jews or those of Jewish descent, while also supporting land reform and the nationalization of some industries. In Mein Kampf ("My Struggle"; 1924–1925), Hitler outlined the anti-Semitism and anti-Communism at the heart of his political philosophy, as well as his disdain for representative democracy and his belief in Germany's right to territorial expansion.

The Nazi Party won the greatest share of the popular vote in the two Reichstag general elections of 1932, making them the largest party in the legislature by far, but still short of an outright majority.

After the death of President Hindenburg, political power was concentrated in Hitler's hands and he became Germany's head of state as well as the head of the government, with the title of Führer, meaning "leader". From that point, Hitler was effectively the dictator of Nazi Germany, which was also known as the "Third Reich", under which Jews, political opponents and other "undesirable" elements were marginalized, imprisoned or murdered. Many millions of people were eventually exterminated in a genocide which became known as the Holocaust during World War II, including around two-thirds of the Jewish population of Europe.

Following Germany's defeat in World War II and the discovery of the full extent of the Holocaust, Nazi ideology became universally disgraced. It is widely regarded as immoral and evil, with only a few fringe racist groups, usually referred to as neo-Nazis, describing themselves as followers of National Socialism.


# Origins

## Response to World War I and Italian Fascism

During World War I, German sociologist Johann Plenge spoke of the rise of a "National Socialism" in Germany within what he termed the "ideas of 1914" that were a declaration of war against the "ideas of 1789" (the French Revolution).[107] According to Plenge, the "ideas of 1789" which included the rights of man, democracy, individualism and liberalism were being rejected in favour of "the ideas of 1914" which included the "German values" of duty, discipline, law and order.[107] Plenge believed that ethnic solidarity (Volksgemeinschaft) would replace class division and that "racial comrades" would unite to create a socialist society in the struggle of "proletarian" Germany against "capitalist" Britain.

Hitler spoke of Nazism being indebted to the success of Fascism's rise to power in Italy.[126] In a private conversation in 1941, Hitler said that "the brown shirt would probably not have existed without the black shirt", the "brown shirt" referring to the Nazi militia and the "black shirt" referring to the Fascist militia.[126] He also said in regards to the 1920s: "If Mussolini had been outdistanced by Marxism, I don't know whether we could have succeeded in holding out. At that period National Socialism was a very fragile growth".[126]

Other Nazis—especially those at the time associated with the party's more radical wing such as Gregor Strasser, Joseph Goebbels and Heinrich Himmler—rejected Italian Fascism, accusing it of being too conservative or capitalist.[127] Alfred Rosenberg condemned Italian Fascism for being racially confused and having influences from philosemitism.[128] Strasser criticised the policy of Führerprinzip as being created by Mussolini and considered its presence in Nazism as a foreign imported idea.[129] Throughout the relationship between Nazi Germany and Fascist Italy, a number of lower-ranking Nazis scornfully viewed fascism as a conservative movement that lacked a full revolutionary potential.


# Ideology

## Nationalism and racialism

**Irredentism and expansionism**

The German Nazi Party supported German irredentist claims to Austria, Alsace-Lorraine, the region now known as the Czech Republic and the territory known since 1919 as the Polish Corridor. A major policy of the German Nazi Party was Lebensraum ("living space") for the German nation based on claims that Germany after World War I was facing an overpopulation crisis and that expansion was needed to end the country's overpopulation within existing confined territory, and provide resources necessary to its people's well-being.[131] Since the 1920s, the Nazi Party publicly promoted the expansion of Germany into territories held by the Soviet Union.

**Racial theories**

In its racial categorization, Nazism viewed what it called the Aryan race as the master race of the world—a race that was superior to all other races.

**Social class**

National Socialist politics was based on competition and struggle as its organizing principle, and the Nazis believed that "human life consisted of eternal struggle and competition and derived its meaning from struggle and competition."[168] The Nazis saw this eternal struggle in military terms, and advocated a society organized like an army in order to achieve success.

**Sex and gender**

Nazi ideology advocated excluding women from political involvement and confining them to the spheres of "Kinder, Küche, Kirche" (Children, Kitchen, Church).

**Opposition to homosexuality**

According to Hitler's closest friend during their late teens, August Kubizek, Hitler was personally disgusted by homosexuality.[202] During the early 1930s, the prominent SA leader Ernst Röhm's homosexuality became a central topic among SA members who hated his leadership.

## Religion

The Nazi Party Programme of 1920 guaranteed freedom for all religious denominations which were not hostile to the State and it also endorsed Positive Christianity in order to combat "the Jewish-materialist spirit". Positive Christianity was a modified version of Christianity which emphasized racial purity and nationalism.

## Economics

Generally speaking, Nazi theorists and politicians blamed Germany's previous economic failures on political causes like the influence of Marxism on the workforce, the sinister and exploitative machinations of what they called international Jewry and the vindictiveness of the western political leaders' war reparation demands. Instead of traditional economic incentives, the Nazis offered solutions of a political nature, such as the elimination of organised trade unions, rearmament (in contravention of the Versailles Treaty) and biological politics.[220] Various work programs designed to establish full-employment for the German population were instituted once the Nazis seized full national power. Hitler encouraged nationally supported projects like the construction of the Autobahn highway system, the introduction of an affordable people's car (Volkswagen) and later the Nazis bolstered the economy through the business and employment generated by military rearmament.[221] The Nazis benefited early in the regime's existence from the first post–Depression economic upswing, and this combined with their public works projects, job-procurement program and subsidised home repair program reduced unemployment by as much as 40 percent in one year. This development tempered the unfavourable psychological climate caused by the earlier economic crisis and encouraged Germans to march in step with the regime.

Upon being appointed Chancellor in 1933, Hitler promised measures to increase employment, protect the German currency, and promote recovery from the Great Depression. These included an agrarian settlement program, labor service, and a guarantee to maintain health care and pensions.[223] But above all, his priority was rearmament, and the buildup of the German military in preparation for an eventual war to conquer Lebensraum in the East.

The German concept of Lebensraum, "living space") comprises policies and practices of settler colonialism which proliferated in Germany from the 1890s to the 1940s. First popularized around 1901,[2] Lebensraum became a geopolitical goal of Imperial Germany in World War I (1914–1918) originally, as the core element of the Septemberprogramm of territorial expansion.[3] The most extreme form of this ideology was supported by the Nazi Party (NSDAP) and Nazi Germany until the end of World War II. Following Adolf Hitler's rise to power, Lebensraum became an ideological principle of Nazism and provided justification for the German territorial expansion into Central and Eastern Europe.[5] The Nazi Generalplan Ost policy (the Master Plan for the East) was based on its tenets.

In spite of their rhetoric condemning big business prior to their rise to power, the Nazis quickly entered into a partnership with German business from as early as February 1933. That month, after being appointed Chancellor but before gaining dictatorial powers, Hitler made a personal appeal to German business leaders to help fund the Nazi Party for the crucial months that were to follow. He argued that they should support him in establishing a dictatorship because "private enterprise cannot be maintained in the age of democracy" and because democracy would allegedly lead to communism.

Agrarian policies were also important to the Nazis since they corresponded not just to the economy but to their geopolitical conception of Lebensraum as well. For Hitler, the acquisition of land and soil was requisite in moulding the German economy.

The Nazis were hostile to the idea of social welfare in principle, upholding instead the social Darwinist concept that the weak and feeble should perish.

## Anti-communism

The Nazis claimed that communism was dangerous to the well-being of nations because of its intention to dissolve private property, its support of class conflict, its aggression against the middle class, its hostility towards small business and its atheism.[249] Nazism rejected class conflict-based socialism and economic egalitarianism, favouring instead a stratified economy with social classes based on merit and talent, retaining private property and the creation of national solidarity that transcends class distinction.

In Mein Kampf, Hitler stated his desire to "make war upon the Marxist principle that all men are equal."[251] He believed that "the notion of equality was a sin against nature."[252] Nazism upheld the "natural inequality of men," including inequality between races and also within each race.[52] The National Socialist state aimed to advance those individuals with special talents or intelligence, so they could rule over the masses.[52] Nazi ideology relied on elitism and the Führerprinzip (leadership principle), arguing that elite minorities should assume leadership roles over the majority, and that the elite minority should itself be organized according to a "hierarchy of talent," with a single leader—the Führer—at the top.

## Anti-capitalism

The Nazis argued that free market capitalism damages nations due to international finance and the worldwide economic dominance of disloyal big business, which they considered to be the product of Jewish influences.[249] Nazi propaganda posters in working class districts emphasised anti-capitalism, such as one that said: "The maintenance of a rotten industrial system has nothing to do with nationalism. I can love Germany and hate capitalism".[264]

Both in public and in private, Hitler expressed disdain for capitalism, arguing that it holds nations ransom in the interests of a parasitic cosmopolitan rentier class.[265] He opposed free market capitalism because it "could not be trusted to put national interests first," and he desired an economy that would direct resources "in ways that matched the many national goals of the regime," such as the buildup of the military, building programs for cities and roads, and economic self-sufficiency.

## Totalitarianism

Under Nazism, with its emphasis on the nation, individualism was denounced and instead importance was placed upon Germans belonging to the German Volk and "people's community" (Volksgemeinschaft).


# Post-war Nazism

Following Nazi Germany's defeat in World War II and the end of the Holocaust, overt expressions of support for Nazi ideas were prohibited in Germany and other European countries. Nonetheless, movements which self-identify as National Socialist or which are described as adhering to National Socialism continue to exist on the fringes of politics in many western societies. Usually espousing a white supremacist ideology, many deliberately adopt the symbols of Nazi Germany.
