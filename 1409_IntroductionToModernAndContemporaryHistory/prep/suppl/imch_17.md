*Xinhai Revolution, Wikipedia*

The Xinhai Revolution, also known as the Chinese Revolution or the Revolution of 1911, was a revolution that overthrew China's last imperial dynasty (the Qing dynasty) and established the Republic of China (ROC). The revolution was named Xinhai (Hsin-hai) because it occurred in 1911, the year of the Xinhai (辛亥; 'metal pig') stem-branch in the sexagenary cycle of the Chinese calendar.

The revolution consisted of many revolts and uprisings. The turning point was the Wuchang uprising on 10 October 1911, which was the result of the mishandling of the Railway Protection Movement. The revolution ended with the abdication of the six-year-old Last Emperor, Puyi, on 12 February 1912, that marked the end of 2,000 years of imperial rule and the beginning of China's early republican era.

The revolution arose mainly in response to the decline of the Qing state, which had proven ineffective in its efforts to modernize China and confront foreign aggression. Many underground anti-Qing groups, with the support of Chinese revolutionaries in exile, tried to overthrow the Qing. The brief civil war that ensued was ended through a political compromise between Yuan Shikai, the late Qing military strongman, and Sun Yat-sen, the leader of the Tongmenghui (United League). After the Qing court transferred power to the newly founded republic, a provisional coalition government was created along with the National Assembly. However, political power of the new national government in Beijing was soon thereafter monopolized by Yuan and led to decades of political division and warlordism, including several attempts at imperial restoration.

The Republic of China in Taiwan and the People's Republic of China on the mainland both consider themselves the legitimate successors to the Xinhai Revolution and honor the ideals of the revolution including nationalism, republicanism, modernization of China and national unity. 10 October is commemorated in Taiwan as Double Ten Day, the National Day of the ROC. In mainland China, Hong Kong, and Macau, the day is celebrated as the Anniversary of the Xinhai Revolution.


# Background

After suffering its first defeat to the West in the First Opium War in 1842, the Qing imperial court struggled to contain foreign intrusions into China. Efforts to adjust and reform the traditional methods of governance were constrained by a deeply conservative court culture that did not want to give away too much authority to reform. Following defeat in the Second Opium War in 1860, the Qing tried to modernize by adopting certain Western technologies through the Self-Strengthening Movement from 1861.

In 1895, China suffered another defeat during the First Sino-Japanese War.[6] This demonstrated that traditional Chinese feudal society also needed to be modernized if the technological and commercial advancements were to succeed.

In 1898 the Guangxu Emperor was guided by reformers like Kang Youwei and Liang Qichao for a drastic reform in education, military and economy under the Hundred Days' Reform.[6] The reform was abruptly cancelled by a conservative coup led by Empress Dowager Cixi.[7] The Guangxu Emperor, who had always been a puppet dependent on Cixi, was put under house arrest in June 1898. The Boxer Rebellion prompted another foreign invasion of Beijing in 1900 and the imposition of unequal treaty terms, which carved away territories, created extraterritorial concessions and gave away trade privileges. Under internal and external pressure, the Qing court began to adopt some of the reforms. The Qing managed to maintain its monopoly on political power by suppressing, often with great brutality, all domestic rebellions.


# Organization for revolution

## Views

Many revolutionaries promoted anti-Qing/anti-Manchu sentiments and revived memories of conflict between the ethnic minority Manchu and the ethnic majority Han Chinese from the late Ming dynasty (1368–1644). Leading intellectuals were influenced by books that had survived from the last years of the Ming dynasty, the last dynasty of Han Chinese. In 1904, Sun Yat-sen announced that his organization's goal was "to expel the Tatar barbarians, to revive Zhonghua, to establish a Republic, and to distribute land equally among the people." Many of the underground groups promoted the ideas of "Resist Qing and restore Ming" that had been around since the days of the Taiping Rebellion.[28] Others, such as Zhang Binglin, supported straight-up lines like "slay the manchus" and concepts like "Anti-Manchuism".


# Strata and groups

The Xinhai Revolution was supported by many groups, including students and intellectuals who returned from abroad, as well as participants of the revolutionary organizations, overseas Chinese, soldiers of the new army, local gentry, farmers and others.


# Uprisings and incidents

The central focus of the uprisings were mostly connected with the Tongmenghui and Sun Yat-sen, including subgroups. Some uprisings involved groups that never merged with the Tongmenghui. Sun Yat-sen may have participated in 8–10 uprisings; all uprisings failed prior to the Wuchang Uprising.

## Wuchang Uprising

The Literary Society and the Progressive Association were revolutionary organizations involved in the uprising that mainly began with a Railway Protection Movement protest.[24] In the late summer, some Hubei New Army units were ordered to neighboring Sichuan to quell the Railway Protection Movement, a mass protest against the Qing government's seizure and handover of local railway development ventures to foreign powers.[71] Banner officers like Duanfang, the railroad superintendent,[72] and Zhao Erfeng led the New Army against the Railway Protection Movement.

Revolutionaries intent on overthrowing the Qing dynasty had built bombs, and on 9 October, one accidentally exploded.[73] Sun Yat-sen himself had no direct part in the uprising and was traveling in the United States at the time in an effort to recruit more support from among overseas Chinese. The Qing Viceroy of Huguang, Rui Cheng, tried to track down and arrest the revolutionaries.[74] Squad leader Xiong Bingkun and others decided not to delay the uprising any longer and launched the revolt on 10 October 1911, at 7 pm.[74] The revolt was a success; the entire city of Wuchang was captured by the revolutionaries on the morning of 11 October. That evening, they established a tactical headquarters and announced the establishment of the "Military Government of Hubei of Republic of China".[74] The conference chose Li Yuanhong as the governor of the temporary government.[74] Qing officers like the bannermen Duanfang and Zhao Erfeng were killed by the revolutionary forces.


# Provincial Uprisings

After the success of the Wuchang Uprising, many other protests occurred throughout the country for various reasons. Some of the uprisings declared restoration (光復) of the Han Chinese rule. Other uprisings were a step toward independence, and some were protests or rebellions against the local authorities.[citation needed] Regardless the reason for the uprising the outcome was that all provinces in the country renounced the Qing dynasty and joined the ROC.


# Republican government in Beijing

On 10 March 1912, Yuan Shikai was sworn as the second Provisional President of the Republic of China in Beijing.[151] The government based in Beijing, called the Beiyang Government, was not internationally recognized as the legitimate government of the Republic of China until 1928, so the period from 1912 until 1928 was known simply as the "Beiyang Period". The first National Assembly election took place according to the Provisional Constitution. While in Beijing, the Kuomintang was formed on 25 August 1912.[152] The KMT held the majority of seats after the election. Song Jiaoren was elected as premier. However, Song was assassinated in Shanghai on 20 March 1913, under the secret order of Yuan Shikai.[153]


# Legacy

## Social influence

After the revolution, there was a huge outpouring of anti-Manchu sentiment through China, but particularly in Beijing where thousands died in anti-Manchu violence as Imperial restrictions on Han residency and behavior within the city crumbled as Manchu Imperial power crumbled.

During the abdication of the last emperor, Empress Dowager Longyu, Yuan Shikai and Sun Yat-sen both tried to adopt the concept of "Manchu and Han as one family".[163] People started exploring and debating with themselves on the root cause of their national weakness. This new search of identity was the New Culture Movement.[165] Manchu culture and language, on the contrary, has become virtually extinct by 2007.

*Unlike revolutions in the West, the Xinhai Revolution did not restructure society.* The participants of the Xinhai Revolution were mostly military personnel, old-type bureaucrats, and local gentries. These people still held regional power after the Xinhai Revolution. Some became warlords. There were no major improvements in the standard of living. Writer Lu Xun commented in 1921 during the publishing of The True Story of Ah Q, ten years after the Xinhai Revolution, that basically nothing changed except "the Manchus have left the kitchen".[167] The economic problems were not addressed until the governance of Chiang Ching-kuo in Taiwan and Deng Xiaoping on the mainland.

The Xinhai Revolution mainly got rid of feudalism (fengjian) from Late Imperial China. In the usual view of historians, there are two restorations of feudal power after the revolution: the first was Yuan Shikai; the second was Zhang Xun.[169] Both were unsuccessful, but the "feudal remnants" returned to China with the Cultural Revolution in a concept called guanxi, where people relied not on feudal relationships, but personal relationships, for survival.[170] While guanxi is helpful in Taiwan, on the mainland, guanxi is necessary to get anything done.[171]

Due to the effects of anti-Manchu sentiment after the revolution, the Manchus of the Metropolitan Banners were driven into deep poverty, with Manchu men too impoverished to marry so Han men married Manchu women, Machus stop dressing in Manchu clothing and stopped practicing Manchu traditions.[

## Historical significance

The Xinhai Revolution overthrew the Qing government and two thousand years of monarchy.[3] Throughout Chinese history, old dynasties had always been replaced by new dynasties. The Xinhai Revolution, however, was the first to overthrow a monarchy completely and attempt to establish a republic to spread democratic ideas throughout China. Though in 1911 at the provisional government welcome ceremony, Sun Yat-sen said, "The revolution is not yet successful, the comrades still need to strive for the future."

Since the 1920s, the two dominant parties–the ROC and PRC–see the Xinhai Revolution quite differently.[174] Both sides recognize Sun Yat-sen as the Father of the Nation, but in Taiwan, they mean "Father of the Republic of China".[174] On the mainland, Sun Yat-sen was seen as the man who helped bring down the Qing, a pre-condition for the Communist state founded in 1949.[174] The PRC views Sun's work as the first step towards the real revolution in 1949, when the communists set up a truly independent state that expelled foreigners and built a military and industrial power.[174] The father of New China is seen as Mao Zedong.[174] In 1954, Liu Shaoqi was quoted as saying that the "Xinhai Revolution inserted the concept of a republic into common people".[175][176] Zhou Enlai pointed out that the "Xinhai Revolution overthrew the Qing rule, ended 2000 years of monarchy, and liberated the mind of people to a great extent, and opened up the path for the development of future revolution. This is a great victory."

## Modern evaluation

The success of the democracy gained from the revolution can vary depending on one's view. Even after the death of Sun Yat-sen in 1925, for sixty years, the KMT controlled all five branches of the government; none were independent.[168] Yan Jiaqi, founder of the Federation for a Democratic China, has said that Sun Yat-sen is to be credited as founding China's first republic in 1912, and the second republic is the people of Taiwan and the political parties there now democratizing the region.[169]

Meanwhile, the ideals of democracy are far from realised on the mainland. For example, former Chinese premier Wen Jiabao once said in a speech that without real democracy, there is no guarantee of economic and political rights; but he led a 2011 crackdown against the peaceful Chinese jasmine protests.[179] Liu Xiaobo, a pro-democracy activist who received the global 2010 Nobel Peace Prize, died in prison.[180] Others, such as Qin Yongmin (秦永敏) of the Democracy Party of China, who was only released from prison after twelve years, do not praise the Xinhai Revolution.[181][182] Qin Yongmin said the revolution only replaced one dictator with another, that Mao Zedong was not an emperor, but he is worse than the emperor.
