*Spain in the 17th century, Wikipedia*

Habsburg Spain was at the height of its power and cultural influence at the beginning of the 17th century, but military, political, and economic difficulties were already being discussed within Spain. In the coming decades these difficulties grew and saw France gradually taking Spain's place as Europe's leading power through the latter half of the century. Many different factors, including the decentralized political nature of Spain, inefficient taxation, a succession of weak kings, power struggles in the Spanish court and a tendency to focus on the American colonies instead of Spain's domestic economy, all contributed to the decline of the Habsburg rule of Spain.

The end of the 17th century also brought the end of Habsburg rule. The 18th century began with the War of the Spanish Succession, which concluded in the establishment of the Bourbon dynasty in Spain.


# Background: Spain from 1469 to the reign of Philip II of Spain

In 1469, Ferdinand II of Aragon and Isabella I of Castile united the Crowns of Aragon and Castile into one, creating the early modern Spanish state. Although this ensured future Spanish rulers would rule over both Aragon and Castile, both regions had their own administration and legal systems. In addition, Aragon itself was divided into Aragon proper, Valencia, and Catalonia - each with its own institutions, customs, and regional identity.[2] As Ferdinand and Isabella both outlived their only son to survive infancy, they were succeeded by their grandson, Charles I upon Ferdinand's death in 1516. Being Flemish born and not a native Spanish speaker, Charles never completely assimilated into Spanish society and as Holy Roman Emperor, had to rule over two empires at once. Determining it to be too difficult for one person to rule over all of his domains, Charles gave the Holy Roman Empire to his younger brother and the Spanish Empire to son, Philip II.

Although the Spanish Empire was at the height of its power under Philip II, a number of factors foreshadowed its eventual, gradual decline. There was a revolt in the Netherlands which started in 1568 and lasted the rest of Philip's reign. Also, the moriscos in Andalusia rebelled in 1570 against Philip's imposition of Spanish language and customs on them. Philip was at war with the Dutch republic, France and England during the last 10 years of his reign. These and other wars and difficulties in maintaining the vast Spanish Empire led to four bankruptcies during Philip's reign.


# Reign of Philip III of Spain (1598-1621)

Signs that Habsburg Spain was declining became visible during the reign of Philip III. Throughout Philip III's reign the main currency was a copper-based coin called vellon, which was minted in response to the fall in imports of silver. Ironically, the copper needed to make vellon was purchased in Amsterdam with silver.[3] Imports of silver bullion from the Americas fell by half during Philip III's reign. In 1599, a year after Philip took the throne, a bubonic plague killed about half a million people (1/10 of the Spanish population at the time).


# Reign of Charles II of Spain (1665-1700)

During the long regency for Charles II, the last of the Spanish Habsburgs, validos milked Spain's treasury, and Spain's government operated principally as a dispenser of patronage. Plague, famine, floods, drought, and renewed war with France wasted the country. The Peace of the Pyrenees (1659) had ended fifty years of warfare with France which had achieved some minor territorial gains at the expense of the Spanish Crown. As part of the peace settlement, the Spanish infanta Maria Teresa, had become the wife of Louis XIV. However, Louis XIV, found the temptation to exploit a weakened Spain too great. Using Spain's failure to pay her dowry as a pretext, Louis instigated the War of Devolution (1667–68) to acquire the Spanish Netherlands in lieu of the dowry. Most of the European powers were ultimately involved in the wars that Louis fought in the Netherlands.


# Spanish society during the 17th century

Spanish society in the 17th century Habsburg Spain was extremely inegalitarian. The nobility, being wealthier than ordinary people, also had the privilege of being exempt from taxes, which the lower classes did not have. Spanish society associated social status with leisure and thus work was undignified for nobles. Even wealthy merchants invested in land, titles and juros. Two acceptable careers for the nobility were the church and education. In 1620, there were 100,000 Spaniards in the clergy, by the late 17th century there were 150,000. Many Spaniards spent long years in universities, taking advantage of the increase in the number of universities. By 1660, there were about 200,000 Spaniards in the clergy and the Church owned 20% of all the land in Spain.


# Economic situation in Spain during the 17th century

Commentators in Spain known as arbitristas proposed a number of measures to reverse the decline of the Spanish economy but they had little effect. Aristocractic contempt for trade was reinforced by its association with conversos and moriscos who were distrusted by the Spanish general population because of their Jewish or Muslim background. More importantly, many arbitristas believed that the influx of silver from the American mines was the cause of inflation which hurt Spanish manufactures. A European price revolution was first fed by silver from central Europe, but then by that from the Spanish American mines. Spain's increasing dependence on resources from the New World over the last century reduced incentives to develop or stimulate domestic production and to create a more efficient tax bureaucracy.[10] Spanish rulers were forced to borrow from capital markets to offset this unstable source of revenue, and interest payments drained the annual budget. Another prominent internal factor was the Spanish economy's dependence on the export of luxurious Merino wool, the demand of which was replaced by cheaper textiles from England and the Netherlands. The most prosperous region of Spain was Catalonia, where peasants had more rights than the rest of Spain. That changed in 1650, when a plague broke out in Catalonia, undermining the local economy. A reliance on revenue from resources obtained from the New World and Merino wool was not sustainable, and ultimately the economy stagnated.


****

*Spain in decline, J.P.SOMMERVILLE*

The seventeenth century was one of decline for Spain. Many factors contributed to the failure of Spanish society to answer the economic and political challenges it faced.

# Institutions

## Disunity

The Spanish state had been created by the marriage of Ferdinand of Aragon to Isabella of Castile in 1469. Although future monarchs of Spain ruled both Aragon and Castile, little was done to unify the administration or legal systems of the two. Indeed, Aragon itself was divided into Aragon proper, Valencia, and Catalonia - each with its own institutions, customs, and identity.

During the later sixteenth century Spain acquired a massive overseas empire, chiefly in the Americas, but also in the Far East (the Philippines were named for Philip II). Both the Spanish and the Portuguese established trading posts on the coasts of Africa and Asia that maintained a degree of independence from local rulers.

Spain also controlled large parts of Italy (including Naples, Sicily, Sardinia and Milan), and much of the southern Netherlands (Flanders) as well as Franche Comté (bordering east central France).
In 1580, Philip II successfully invaded Portugal, claiming to have inherited it along with its possessions in the East Indies and Brazil.

This vast array of possessions was administered by viceroys, governors, the Council of the Indies, and various other officials created piecemeal as each territory was acquired.

## Taxation

The empire had considerable resources but it was often difficult to exploit them, not least because local interests were reluctant to pay for the costs of other territories.

The basic Castilian tax was the alcabala - a sales tax of 10% - but many towns had agreed to a fixed sum which fell far below the theoretical 10% as a result of inflation. Moreover, in many places the receipts went to local nobles not the crown. As its yield fell, the Cortes of Castile agreed to the servicios ordinario y extraordinarios (which also fell in value) and from late in the sixteenth century to the millones (a tax on basic foodstuffs, including meat and wine) became more important. The crown was also able to obtain some income from the church.

Income from the silver mined in the New World fell after a peak in the late sixteenth century.

Imports declined still further during the later seventeenth century. Remittances of silver from the Lima (Peru) treasury, for example, fell gradually from 14.8 million pesos in 1631-40 to 1.2 million in 1681-90.

The Spanish tax burden was very unevenly distributed: it fell more on the poor than the rich, heavily on the agricultural sector, and on Castile far more than Aragon or the Basque country. But the Spanish government's expenditure continued to climb: - in the first twelve years of Philip III's reign, he spent over 40 million ducats on the Low Countries' wars alone (a ducat is a small gold coin weighing about 3.5 grams.)

To cover the shortfall, the Spanish government both borrowed money - by the issue of juros (interest-bearing state bonds) - and assigned the revenues from future years to the bankers if they would pay the defense contracts for the present year. By 1607 the government had a debt of almost 23 million ducats and had assigned away all its revenue for four years ahead. By 1644 the crown's income was pledged to 1648; and by 1664 the crown owed more than 21 million ducats Lacking any real money the government periodically had to pay the interest owed on the juros by issuing more juros - but people were not overly keen to buy these new juros since they were none too confident that the interest would be paid.

Another extreme expedient was the issue of "vellón" coins - vellón was worthless copper, sometimes with a tiny amount of silver added. The government forced its employees and creditors to accept this token currency, and the issues of 1636, 1641, and 1654 each made about five million ducats profit for the crown. But the debased coins simply produced inflation and confusion in the economy.

With Spanish finances in so parlous a state, a misfortune such as Piet Heyn's seizure of the Spanish treasure fleet in 1628 became a disaster. The only beneficiaries from the weakness of Spanish government finances were Spanish noblemen, from whom the crown borrowed so heavily that they held an effective stranglehold on the monarchy.

# Society

Seventeenth- century Spanish society was extremely inegalitarian. The nobility were not only richer than ordinary people but had special legal privileges. They were, for example, exempt from many taxes.

In Spain high social status was closely associated with leisure. Work - particularly manual labor - was considered undignified and demeaning. Even merchants who became wealthy tried to buy land and titles and invest in juros, so that they would no longer have to work for a living. Large numbers of Spaniards entered the church: - there were perhaps 100,000 clergy in the 1620s and 150,000 or more in the later seventeenth century. Others spent long years in college education - 21 new universities were founded during the sixteenth century. This increased the proportion of Spaniards in economically unproductive activities.

Political commentators, "arbitristas", bemoaned the inverse correlation between productive work and high status, but could do little to change such a widespread social attitude. The prevalent contempt for retail trading was reinforced by its associations with Spain's ethnic minority groups - Marranos (Jewish converts to Christianity) and Moriscos (Muslim converts to Christianity.)

During the Middle Ages, Spain had been conquered by Muslims invading from Africa. The Christian Spanish re-conquered all of Spain by the end of the fifteenth century - finally retaking Granada in 1492. Initially tolerant towards the Jews and Muslims, the Spanish government had grown increasingly wary and in the sixteenth century had offered a hard choice between expulsion and conversion.  Many Spaniards were highly suspicious of how genuine the "conversion" of Moriscos had been. They feared that the Moriscos were secretly in favor of Islamic reconquest. The Spanish Inquisition was established in order to ascertain whether converts or their children were backsliding. All those with Jewish or Moorish blood tended to be suspected - this emphasis on limpieza de sangre (purity of blood) had a pernicious influence on Spanish society.  Suspicion of Moriscos increased after the Rebellion of the Alpujarras in 1570, when Andalusian Moriscos had rebelled against attempts to make them abandon the use of the Arabic language and of Muslim customs. Spain was almost continuously at war with the Ottoman Turks in the Mediterranean at that time, and the Spanish government saw the Moriscos as a seditious fifth column.

In Valencia, where Moriscos made up about one third of the population, and in Castile where many Moriscos displaced after the Alpujarras rebellion had drifted into casual labor, there was strong popular feeling against them.  On 9 April 1609, Philip III issued a decree of expulsion and over the course of the next five years about 275,000 Moriscos were forcibly deported (of a total Morisco population of 319,000). The expulsion was as widely popular as it was economically disastrous.

# Economy

The exactions of noble landlords and the high levels of taxation drove the Spanish peasantry into poverty. The peasantry were typically tenants on estates who had to give a large proportion of their output to the landowners. This discouraged hard work and innovation in agricultural methods, as the rewards went to the landlord not the peasant.

Rural poverty provoked migration to Spain's cities and to the New World.

Another group of migrants to the New World consisted of the younger sons of noble families - primogeniture commonly gave all the real estate to the eldest son.

Another reason for the decline of agriculture was the sheep. Spain's merino sheep produced some of the highest quality wool in the world, and from it was made expensive, luxury cloth. The grandees dominated the Mesta - an organization representing sheep-owners. They were able to obtain special privileges from the crown: - sheep-farming was less labor intensive than arable production, and this meant more men were available for the Spanish army. Furthermore, the profits from wool and cloth production were easily taxed by the government.

Unfortunately, during the seventeenth century these profits began to fall. The English and Dutch began to sell the new draperies - lighter cheaper textiles that soon cut into the demand for Spanish products.

The debasement of the coinage also contributed to inflation and industrial decline.

A contrast to the general story of economic weakness was Catalonia, where the peasants held secure tenure and property rights, and the Cortes showed a real determination to protect farmers' rights. Here agriculture prospered, until the Catalan revolt and the 1650-54 plague in Barcelona (killing 36,000 of its inhabitants) undermined the local economy.


****

*Napoleon: why escape from Elba anniversary is a bigger deal than Waterloo, The Conversation*

Publishers love a good anniversary, so a 200-year anniversary is gold dust. Yet, however useful a commercial hook, it’s hardly sufficient to warrant such an outpouring of scholarly and popular writing. Yes, Waterloo did draw a final decisive curtain on Napoleon’s military career. But the battle itself can be thought to have little historical significance. The commemoration of Wellington’s triumph by Britain’s political elite (despite the decisive role played by Blücher) is often little more than a jingoistic appeal to popular anti-French sentiment.

Because even if this battle had not finished Napoleon, then the combined armies of the four powers (Britain, Prussia, Russia, and Austro-Hungary) would have done so sooner or later. On March 13, the representatives at the Congress of Vienna had responded to Napoleon’s return from Elba by declaring him an outlaw who could not be tolerated in power. This was a rare moment of agreement between the four powers and it made war inevitable.

Meanwhile, things were looking pretty bleak on Napoleon’s side. The French National Assembly’s refusal to re-introduce conscription left him dependent on an inevitably dwindling supply of men and support for him more generally ebbed as war approached. The die had been wholly cast. It was not a question of whether he fell, but when – and at what cost to whom. So why the fuss about Waterloo?

# Revolution eradicated

Napoleon had been forced to abdicate in March 1814 and was subsequently exiled to Elba. Louis XVIII was restored. After this, the Congress of Vienna sought to redraw the borders of Europe’s states and to restore autocratic and royal government across the board. The establishment of a stable balance of power between the great powers was one objective (and was one reason for keeping France whole), but so too was the eradication of the remnants of the French Revolution.

In France, oppressed by more than a decade of war and seeing little liberty under the Empire, the stability of a settled monarchy was initially attractive to many. Yet, after only a few months of Bourbon rule, that consensus evaporated.

Napoleon’s return in 1815 inevitably messed this balance up. And so one justification for the commemoration lies not in the final battle, or in the tragic hero’s final exile, but in the significance of his return for France and for Europe more widely.

When Napoleon slipped the lax security of Elba and landed on French soil on February 26 1815, he offered himself not as an autocrat but as a popular hero: one who could, as Balzac later put it, “gain an empire simply by showing his hat!” Whether his initial aspirations were at all popular in character is unclear – he claimed to be coming to resume his rights, which he saw also as those of the people. But there is no doubt that his return re-kindled the spirit of 1789 and 1793: the Marseilles was sung again on the streets of Paris (having been sidelined in Napoleon’s earlier reign and banned under Louis).

And, if only tactically, he endorsed the sovereignty of the French people and linked his cause to a liberal (but not radical) reading of the original principles of the French Revolution. He managing to bring to his side many liberals, such as Benjamin Constant, who had previously opposed him.

# Power from the people

So this time round he came not on his own terms, but on those of the people and in their name. He was embraced for redeeming their standing as a glorious, modern and reforming nation. Support for him hemorrhaged whenever he seemed to assume imperial standing. Not everybody bought his apparent commitment to popular sovereignty in his claim that “My will is that of the people”. But his ability to sweep back into France and into the hearts of the French military, who deserted Louis en masse to serve their old commander, is impossible to account for without accepting that he appeared to speak for the French people in a way Louis XVIII could not.

This role proved unsustainable following the Allies’ rejection of his rule and the resumption of military conflict. But it encapsulates, in dramatic terms, the debate over who determines state legitimacy: popular support or the autocratic decisions of the Congress of Vienna and the Courts of Europe.

While most of Europe thought the liberté and popular sovereignty of the French Revolution had finally been quashed by France’s humiliation in 1814, the 100 Days revealed the fragility of the Great Power politics that sought to contain France as a military, but more essentially, as an ideological force.

Yes, the Congress of Vienna resumed its task after Napoleon’s defeat and second exile. But the final flourish of the 100 Days did much to canonise Napoleon as the people’s hero and also to signify the inevitable fragility of monarchical and aristocratic orders that could not speak in the name of those they ruled with any confidence at all.

The heroic version of Napoleon could be seen in Britain in the years following Waterloo and his death on St Helena six years later. Several popular ballads emerged commemorating his ability to inspire commitment and sacrifice in his army and his people. One of the most enduring was The Grand Conversation on Napoleon, whose title and refrain might best be understood as underscoring the fact that, in the modern world inaugurated by the Revolution, political orders and their leadership might hold their power by their monopoly of violence – but they could derive their legitimacy only from their people.
