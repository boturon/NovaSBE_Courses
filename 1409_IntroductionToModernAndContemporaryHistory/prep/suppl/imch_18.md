*Japan’s Economic Miracle, Masahiro Takada*

Japan’s defeat in World War II enabled the Japanese people to start a new economy from a fresh start since everything they had built during the years were destroyed from the war. The U.S. Occupation of Japan set forth series of reform policies to reconstruct and recover the devastated nation and ultimately creating the opportunity to become the economic superpower. The three major reform policies implemented by the American forces were breakup of the zaibatsu, land reform, and labor democratization. These reform policies had a great impact in democratizing and modernizing the nation and affected the nation’s ability to grow.

The recovery of the Japanese economy was achieved through the implementation of the Dodge Plan and the effect it had from the outbreak of the Korean War. The so called Korean War boom caused the economy to experience a rapid increase in production and marked the beginning of the economic miracle.

The ability of the Japanese people to imitate and apply the knowledge and skill learned from the Western countries is the single most important factor for Japan’s amazing growth. Import of technologies and improved business condition were some of other factors for growth. Also, economic policies and strategies carried out by political characters greatly influenced and accelerated the economy.


# Introduction

To understand Japan’s postwar economic growth, we must consider its economic development and history during the 1800s to early 1900s. At the starting point of modern economic growth when Japan became an open economy in the late 1800s, a huge gap existed between the Western Powers and Japan, due to Japan’s historical isolation from the rest of the world. However, Japan was able to make the gap smaller by agricultural technology developed during the Tokugawa Period and also, the central government during the Meiji Period carried out a series of modernization measures and it enabled Japan to import technologies and ideas from the Western countries easier. The heritage from the Tokugawa Period, together with the foundations for economic growth developed during the early Meiji Period, enabled Japan to propel the economy on the road to modern economic growth over a period of about twenty years starting from the mid- 1880s.2 By 1900, Japan was ready to expand its empire in a larger scale since Japan being heavily populated with small resources had to rely on trade to supply its basic needs it lacked.


# Impact of WWII

## Occupation of Japan

Since Japan was in need for help to reconstruct its economy, the Allied Powers, who comprised mostly of American forces occupied Japan. The Occupation of Japan took the form of indirect rule due to the existence of the Japanese government, but the reform policies set forth by the Allied Powers was more of a direct rule to Japan. General Douglas MacArthur as the Supreme Commander for the Allied Powers (SCAP) led the Occupation and the first step MacArthur took before implementing the reform policies were to establish economic demilitarization and to ensure that all production of military materials had stopped and closed down. This led to the formation of the Constitution of 1947, where Japan gave up the right to use any military force forever and relied on the United States for their protection from outside forces. The decreased spending on military and defense forces are clearly one of the main reasons for Japan’s economic miracle. In addition to the demilitarization, series of reform policies were set forth by the SCAP during the occupation, which was aimed to democratize the country. There were three major reforms set forth during the Occupation of Japan, and they are the breakup of the zaibatsu, land reform, and labor democratization.

The first reform, the breakup of the zaibatsu was carried out for the purpose of destroying Japan’s military power both psychologically and institutionally.6 These zaibatsu are business conglomerates that were often treated preferably by the government through lower taxes and receiving huge funds for expansion of the company. Moreover, the concentration of industrial control promoted the continuation of a semi-feudal relationship between labor and management, held down wages, blocked the development of labor unions, obstructed the creation of firms by independent entrepreneurs, and hindered the rise of a middle class in Japan. The dissolution of the zaibatsu was implemented by breaking up the holding companies, which were the core of zaibatsu control, and to sell their stock to the public. The democratization policies concerning the industrial associations were able to achieve success by the creation of two laws, which were Anti-Monopoly Law and Decentralization Law. Anti-Monopoly Law was formed to prohibit all cartel activities

The next reform imposed by SCAP was land reform, in which land, occupied and farmed by the landlords were forced to sell their holdings of land. These lands were bought up by the government for redistribution to tenant farmers. The land reform was necessary to democratize the country, because, before the war, about two-thirds of all Japanese farmers rented all or part of the land they cultivated, and the land system was characterized by many factors of a feudalistic state. Due to this problem, the democratization policy of SCAP had two major objectives, (1) to transfer land ownership to farmers who actually tilled the soil, and (2) to improve farm tenancy practices for those who continued as tenants.8 Before the beginning of the program, only 54 percent of the


Another major reform conducted by the Occupation forces was aimed for labor democratization. The major achievement from this reform was that it enabled the Japanese to form labor unions.


# From Reform to Recovery

## The Dodge Plan in 1948

With the three major reforms introduced and put into effect, the Japanese economy was on its way to recovery. However, by early as summer 1947, the Cold War tensions had started to build up in East Asia and the United States revised its policies towards Japan for the purpose of accelerating its economic recovery. The Dodge Plan in 1948, conducted by a Detroit bank president Joseph Dodge was implemented for the solution of bringing back Japan to full strength and ultimately removing American aid from Japan to prepare for the Cold War. He introduced three basic policies to mainly resolve the serious problem of inflation and to establish stabilization in Japan. The first was a balanced budget. The second was the suspension of new loans from the Reconstruction Finance Bank. This measure was aimed at cutting off at their source the supplies of new currency, which were seen as the fundamental cause of inflation. The third policy was reduction and abolition of subsidies.

The goal to accelerate the economy in such a short period of time with these three policies were almost impossible to achieve due to sudden adjustments, but with the start of the Korean War in 1950, the economy boomed.

## The Korean War Boom

The early period of the Dodge Plan caused Japan to enter into recession and labor unrest increased, and a full-scale depression was feared. However, with the outbreak of the Korean War soon afterwards, the economic condition and situation changed completely.

In addition, the Korean War had increased the effect on plant and equipment investment and technological innovation. Since Japan was still behind the international competition, many industries imported technology mainly from the Western countries and expanded capacity. Due to these rapid increases in production and economy as a whole, full recovery and stabilization was obviously achieved well beyond the end of the war and Japan restored its independence with the signing of the Japanese-American Security Pact in 1952.


# Economic Miracle

## Factors for Growth

One of the factors that the Japanese made use of their unique characteristic to expand the economy was to improve and make practical use of technologies and technological know-how’s imported from foreign countries.

Technological improvements in Japan contributed greatly to its economic growth, because improvements of technologies in one industry influenced the growth of many other industries. For example, Japan’s steel industry successfully improved the

## Political Factors for Growth

There are two major policies that led to Japan’s rapid growth. The first policy was the Yoshida Doctrine, in which shaped the postwar economy in Japan to recovery. Prime Minister Yoshida Shigeru developed this policy during the early period of the Korean War, and he is often called on as the father of modern Japanese economy. The policy was aimed to set economic reconstruction and development as the nation’s immediate goals while saving on military expenses by leaving defense to the U.S. army.

In addition to the Yoshida Doctrine, Ikeda, who is seen to be the most important figure in Japan’s rapid growth, implemented the Income Doubling Plan in 1960. As the name of the plan implies, it was aimed to double the income earned by the Japanese workers and set a high living standard from the period of 1961 to 1970 by greatly increasing the amount of investments made by the central government to both private and public firms. Although few problems arose from heavy industrialization, this plan has contributed greatly to the later half of Japan’s rapid growth with an average growth rate of 10.8 percent in the late 1960s and drove the economy to become the second largest in the world by the year 1968.

In addition to policies and economic planning set forth by leaders of the country, another political factor that greatly influenced the growth was the role taken by the Ministry of International Trade and Industry (MITI). MITI, which was regarded as the most powerful government organization during the time of rapid expansion, was mostly responsible for the industrial growth in Japan. The Ministry’s approach was one of providing encouragement and guidance to the initiatives of private business: creating a suitable un-level playing field which would give that critical advantage to industries identified by government as having potential for long-term success.17 The main targets that MITI focused for growth were in the industries of steel, shipbuilding, chemicals, and machinery.


# The Road to Stable Growth

The economy after the oil crisis stabilized due to government’s quick respons and high technological level already achieved. The key to recovery was the boom in exports of cars, electronics, and other products, which grew far more rapidly than imports. By 1977 Japan’s burgeoning trade surplus had become a global issue.
