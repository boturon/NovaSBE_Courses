*Globalization and World Order, London Conference*

# Globalization and World Order: 1914 vs 2014

The world in 2014 is undergoing a profound rebalancing of economic power and wealth. Not surprisingly, it is witnessing many of the same insecurities as it did 100 years ago. A critical difference from 1914, however, is the nature of today’s economic globalization. Foreign investment and global supply chains are interconnecting governments and nations as much as markets.

## The changing balance of world power

Globalization – the opening of national markets to trade, international capital and foreign investment, and the resultant global flows of technology – has been the engine of this economic rebalancing. Globalization is re-establishing the connection between the size of a country’s population and the size of its GDP across large swathes of the world, potentially leading to the recovery by China and India of the relative shares of world economic output that they enjoyed for hundreds of years until the mid-19th century.

Globalization means countries are interconnected to an extent never before experienced. The idea that the globalization of 2014 resembles that of 1914 is misleading. World trade as a percentage of world GDP in 2010, at 27.7%,3 was indeed similar to its level in 1914, when it stood at 21%. But in 1914 the terms of trade were fundamentally different. Britain, France, the Netherlands and others used their industrial ‘first-mover’ advantage to create empires and dominate markets, ensuring they remained at the top of the wealth pyramid. Trade and mercantilism went hand in hand.4 Now, companies from Britain, the US, Japan and elsewhere have invested across the world to create complex global supply chains, taking advantage of wage differentials, proximity to markets and resource considerations to make their operations truly international.

Globalization is a brutal process. Societies accustomed to being at the top of the pyramid are being forced to make harsh structural adjustments.

Two other related factors reveal the dissimilarity between 2014 and 1914: the vulnerability of today’s rising powers, which must overcome the middle-income trap if they are to convert industrialization into sustainable welfare domestically and influence internationally; and the seeming resilience of existing powers, such as the US and Germany, where both corporate and social organization (as well as abundant resource endowments in the US case) appear to make up for apparent weaknesses in domestic governance.

## Risks to international order

Instead, 21st century interdependence creates its own vulnerabilities. First, interdependence to some can mean dependence to others. Japanese leaders highlight the illogic of rising tensions with China by pointing to the extensive investments they have made in China and the growth in bilateral trade. From a Chinese perspective, however, Japan – with its weakening domestic drivers of growth – has made itself dependent on China and potentially open to economic coercion. In a similar vein, President Vladimir Putin hopes that European dependence on Russian oil and gas will outweigh Russia’s dependence on the income from its sales of both, allowing Russia to subjugate Ukraine.

Fourth, the persistent pull of history and emotion sharpens the risk of conflict. Nationalism remains a powerful force in international affairs. As countries regain influence or sense relative decline, unresolved aspects of national identity can surface. The propaganda used by Russian leaders over Crimea and Ukraine bears witness to the political power of revisionist narratives. The same dynamic is at play in East Asia, where Chinese leaders are using rising economic and military power to assert their claims to islands in the South and East China Seas that they believe were unjustly taken from them in the first half of the 20th century.

## Conclusions

The world order during the 21st century will be determined by whether governments and societies can find together a productive balance between the simultaneous increase in levels of state competition and transnational interdependence. They will have to do so without formal structures of global governance or the hegemonic leadership that the US provided across much of the Western world in the second half of the 20th century.

It will be important, therefore, to engage rising powers inside existing international institutions as equal partners. Some institutions and agreements, such as the UN Security Council and Non-Proliferation Treaty, are probably unreformable, given the vested interests of their privileged members. But others, such as the International Energy Agency, the World Bank and the IMF, can be reformed, with the G20 often playing an enabling role.

Western governments need to invest in the resources necessary to deter aggression and manage security threats, just as rising powers are doing.


# Globalization: Winners and Losers

The past two decades have seen a huge increase in global trade and wealth, a changing distribution of global GDP and a redrawing of the industrial landscape. China’s rise is the defining economic feature of the age.

It is difficult to measure the effects of globalization on inequality. In terms of individual incomes, the biggest ‘winners’ from globalization are the very wealthy – the 1% – and the emerging global middle class, based mainly in emerging economies.

The biggest losers are the very poor and those between the 75% and the 90% percentile – spread between former communist states, Latin America and the poor in advanced economies – whose real incomes have not increased at all.

Globalization poses challenges to traditional forms of political order. There is tension between global markets and the nation-states that are the regulators and legitimate political authorities over those markets.

## Some characteristics of global economic integration

Supply chains are becoming global. Trade in intermediate products is estimated to constitute 56% of total trade in goods and 73% of total trade in services. Most of what is traded is used to produce other things, rather than for consumption.

## Globalization and inequality

Advocates of globalization say it produces a rising economic tide that lifts all boats, and that consumers have benefited from falling prices. Critics say it increases inequality and serves the wealthiest at the expense of the poor.

Inequality between states measured by the Gini coefficient of average incomes increased steadily between 1980 and the 2000s before beginning to decline; Inequality between states adjusted for population size shows a marked decline in inequality, fuelled predominantly by high growth in populous China and India; and Global inequality based on individual incomes rather than state averages is persistently high, but appears to be on a slightly downward trend, decreasing by 1.4 Gini points between 2002 and 2008.

The biggest ‘winners’ were the very wealthy – the top 1% of the global income distribution – and the middle classes of emerging economies; the 50th and 60th percentile of global income distribution saw the biggest rise of all (almost 80%). The biggest losers were the very poor (bottom 5%) and those between the 75% and the 90% percentile – spread between former communist states, Latin America and the poor in advanced economies, whose real incomes did not increase at all, and in some cases actually shrank. However, with the exception of these two groups, there was a general increase in the real incomes of the majority of the world’s population, including the poorest third of the world’s population (above the bottom 5%).


# Power and Governance in the Digital Age

Initially, new technologies tend to disrupt the status quo and threaten the establishment’s hold on power. As these technologies mature, however, elites eventually learn how to harness them for their own gain. The question is whether the internet will follow this same pattern. The battle for control is currently under way.

At present, internet governance follows a ‘multi-stakeholder model’ that involves input from a variety of interest groups. However, splits are emerging between states favouring ‘national sovereignty models’ (more state control over their populations’ access to information) and states wanting to broaden the current multi-stakeholder model.

Upholding ‘net neutrality’ – the principle that all internet traffic should be treated equally – can prevent large-scale corporations from gaining dominance over the internet. This will be vital to preserve innovation and prosperity – helping start- ups and entrepreneurs in poorer communities to compete with these larger companies.

## Impact of the digital revolution on government, corporations and political power

However, in other Arab countries the arrest – and alleged killing – of bloggers critical of the regimes has so far been effective in subduing protest. The government of President Bashar al-Assad in Syria is known as a ‘tech-savvy foe’ whose use of false Facebook login pages to steal the passwords of online dissidents allows it to monitor their activities closely.29 It thus remains to be seen whether activists or governments will retain the technological advantage.


# What Now? First Steps Towards a Rebalanced World

Many of the pillars of the 20th-century international order are struggling to meet the challenges of the 21st century, suffering from structural weaknesses such as unrepresentative membership, inflexible design and too great a focus on state actors.

Some reform of existing international organizations may be possible, including pruning obsolete forums, but any genuine revival would require widespread recognition of the nature and urgency of international problems, supported by domestic political opinion. Structural reform of international organizations will follow rather than bring about such a consensus.

Ad hoc groupings of states united by interest or region will operate as caucuses or advance guards within existing international structures. The role of large organizations will increasingly be to provide the institutional framework for such caucuses rather than to mobilize joint action by the full membership. 

Such ad hoc groupings may, however, lack the legitimacy of larger organizations, may not be as resilient in unexpected crises, and may undercut the ability of smaller and weaker nations to have their voices heard.

States, and state-based organizations, will find it increasingly difficult to do more than merely respond to changes triggered by technological and commercial developments. A new form of hybrid international organization may become necessary, involving state governments, transnational corporations and civil society groups. In the long term, any joint effort to deal with global problems may be based more on informal networks within or across borders, may be issue-focused and may be more commercial, cultural or religious than governmental.

The future shape of ‘global governance’ will therefore depend on whether it is possible to build a new consensus and how far it would be shared.
