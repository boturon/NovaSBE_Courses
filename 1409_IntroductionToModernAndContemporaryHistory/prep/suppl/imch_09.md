*American Revolution, Wikipedia*

The American Revolution was a colonial revolt that took place between 1765 and 1783. The American Patriots in the Thirteen Colonies won independence from Great Britain, becoming the United States of America. They defeated the British in the American Revolutionary War (1775–1783) in alliance with France.

Members of American colonial society argued the position of "no taxation without representation", starting with the Stamp Act Congress in 1765. They rejected the authority of the British Parliament to tax them because they lacked members in that governing body. Protests steadily escalated to the Boston Massacre in 1770 and the burning of the Gaspee in Rhode Island in 1772, followed by the Boston Tea Party in December 1773, during which Patriots destroyed a consignment of taxed tea. The British responded by closing Boston Harbor, then followed with a series of legislative acts which effectively rescinded Massachusetts Bay Colony's rights of self-government and caused the other colonies to rally behind Massachusetts. In late 1774, the Patriots set up their own alternative government to better coordinate their resistance efforts against Great Britain; other colonists preferred to remain aligned to the Crown and were known as Loyalists or Tories.

Tensions erupted into battle between Patriot militia and British regulars when the king's army attempted to capture and destroy Colonial military supplies at Lexington and Concord on April 19, 1775. The conflict then developed into a global war, during which the Patriots (and later their French, Spanish, and Dutch allies) fought the British and Loyalists in what became known as the American Revolutionary War (1775–1783). Each of the thirteen colonies formed a Provincial Congress that assumed power from the old colonial governments and suppressed Loyalism, and from there they built a Continental Army under the leadership of General George Washington. The Continental Congress determined King George's rule to be tyrannical and infringing the colonists' rights as Englishmen, and they declared the colonies free and independent states on July 2, 1776. The Patriot leadership professed the political philosophies of liberalism and republicanism to reject monarchy and aristocracy, and they proclaimed that all men are created equal.

The Patriots unsuccessfully attempted to invade Canada during the winter of 1775–76, but successfully captured a British army at the Battle of Saratoga in October 1777. France now entered the war as an ally of the United States with a large army and navy that threatened Great Britain itself. The war turned to the American South where the British under the leadership of Charles Cornwallis captured an army at Charleston, South Carolina in early 1780 but failed to enlist enough volunteers from Loyalist civilians to take effective control of the territory. A combined American–French force captured a second British army at Yorktown in the fall of 1781, effectively ending the war. The Treaty of Paris was signed September 3, 1783, formally ending the conflict and confirming the new nation's complete separation from the British Empire. The United States took possession of nearly all the territory east of the Mississippi River and south of the Great Lakes, with the British retaining control of Canada and Spain taking Florida.

Among the significant results of the revolution was the creation of the United States Constitution, establishing a relatively strong federal national government that included an executive, a national judiciary, and a bicameral Congress that represented states in the Senate and the population in the House of Representatives.[1][2] The Revolution also resulted in the migration of around 60,000 Loyalists to other British territories, especially British North America (Canada).


# Origin

## 1651–1748: Early seeds

As early as 1651, the English government had sought to regulate trade in the American colonies. On October 9, the Navigation Acts were passed pursuant to a mercantilist policy intended to ensure that trade enriched only Great Britain, and barring trade with foreign nations.

In the 1680s, King Charles II determined to bring the New England colonies under a more centralized administration in order to regulate trade more effectively.[9] His efforts were fiercely opposed by the colonists, resulting in the abrogation of their colonial charter by the Crown.[10] Charles' successor James II finalized these efforts in 1686, establishing the Dominion of New England. Dominion rule triggered bitter resentment throughout New England; the enforcement of the unpopular Navigation Acts and the curtailing of local democracy angered the colonists.

Subsequent English governments continued in their efforts to tax certain goods, passing acts regulating the trade of wool,[16] hats,[17] and molasses.[18] The Molasses Act of 1733 in particular was egregious to the colonists, as a significant part of colonial trade relied on the product.

Historians typically begin their histories of the American Revolution with the British coalition victory in the Seven Years' War in 1763. The North American theater of the Seven Years' War is commonly known as the French and Indian War in the United States; it removed France as a major player in North American affairs and led to the territory of New France being ceded to Great Britain.

The Royal Proclamation of 1763 may also have[weasel words] played a role in the separation of the Thirteen Colonies from England, as colonists wanted to continue migrating west to lands awarded by the Crown for their wartime service.[citation needed] The Proclamation, however, cut them off. The lands west of Quebec and west of a line running along the crest of the Allegheny Mountains became Indian territory, barred to settlement for two years. The colonists protested, and the boundary line was adjusted in a series of treaties with the Indians. In 1768, Indians agreed to the Treaty of Fort Stanwix and the Treaty of Hard Labour, followed in 1770 by the Treaty of Lochaber. The treaties opened most of Kentucky and West Virginia to colonial settlement. The new map was drawn up at the Treaty of Fort Stanwix in 1768 which moved the line much farther to the west, from the green line to the red line on the map at right.

## 1764–1766: Taxes imposed and withdrawn

In 1764, Parliament passed the Currency Act to restrain the use of paper money, fearing that otherwise the colonists might evade debt payments.[23] Parliament also passed the Sugar Act, imposing customs duties on a number of articles. Parliament finally passed the Stamp Act in March 1765 which imposed direct taxes on the colonies for the first time. All official documents, newspapers, almanacs, and pamphlets were required to have the stamps—even decks of playing cards.

The colonists did not object that the taxes were high; they were actually low.[24] They objected to the fact that they had no representation in the Parliament, and thus no voice concerning legislation that affected them.

The Parliament at Westminster saw itself as the supreme lawmaking authority throughout all British possessions and thus entitled to levy any tax without colonial approval.[29] They argued that the colonies were legally British corporations that were completely subordinate to the British parliament and pointed to numerous instances where Parliament had made laws binding on the colonies in the past.[30] They did not see anything in the unwritten British constitution that made taxes special[31] and noted that they had taxed American trade for decades. Parliament insisted that the colonies effectively enjoyed a "virtual representation" as most British people did, as only a small minority of the British population elected representatives to Parliament.[32] Americans such as James Otis maintained that the Americans were not in fact virtually represented.[33]

In London, the Rockingham government came to power (July 1765) and Parliament debated whether to repeal the stamp tax or to send an army to enforce it. Parliament agreed and repealed the tax (February 21, 1766), but insisted in the Declaratory Act of March 1766 that they retained full power to make laws for the colonies "in all cases whatsoever".[34] The repeal nonetheless caused widespread celebrations in the colonies.

## 1767–1773: Townshend Acts and the Tea Act

In 1767, the Parliament passed the Townshend Acts which placed duties on a number of essential goods, including paper, glass, and tea, and established a Board of Customs in Boston to more rigorously execute trade regulations. The new taxes were enacted on the belief that Americans only objected to internal taxes and not to external taxes such as custom duties.

On March 5, 1770, a large crowd gathered around a group of British soldiers. The crowd grew threatening, throwing snowballs, rocks, and debris at them. One soldier was clubbed and fell.[36] There was no order to fire, but the soldiers fired into the crowd anyway. They hit 11 people; three civilians died at the scene of the shooting, and two died after the incident. The event quickly came to be called the Boston Massacre. The soldiers were tried and acquitted (defended by John Adams), but the widespread descriptions soon began to turn colonial sentiment against the British. This, in turn, began a downward spiral in the relationship between Britain and the Province of Massachusetts.

A new ministry under Lord North came to power in 1770, and Parliament withdrew all taxes except the tax on tea, giving up its efforts to raise revenue while maintaining the right to tax. This temporarily resolved the crisis, and the boycott of British goods largely ceased, with only the more radical patriots such as Samuel Adams continuing to agitate.

Parliament passed the Tea Act to lower the price of taxed tea exported to the colonies in order to help the East India Company undersell smuggled Dutch tea. On December 16, 1773, a group of men, led by Samuel Adams and dressed to evoke the appearance of American Indians, boarded the ships of the British East India Company and dumped £10,000 worth of tea from their holds (approximately £636,000 in 2008) into Boston Harbor. Decades later, this event became known as the Boston Tea Party and remains a significant part of American patriotic lore.

## 1774–1775: Intolerable Acts and the Quebec Act

The British government responded by passing several Acts which came to be known as the Intolerable Acts, which further darkened colonial opinion towards the British. In response, Massachusetts patriots issued the Suffolk Resolves and formed an alternative shadow government known as the "Provincial Congress" which began training militia outside British-occupied Boston.


# Military hostilities begin

Massachusetts was declared in a state of rebellion in February 1775 and the British garrison received orders to disarm the rebels and arrest their leaders, leading to the Battles of Lexington and Concord on 19 April 1775. The Patriots laid siege to Boston, expelled royal officials from all the colonies, and took control through the establishment of Provincial Congresses. The Second Continental Congress was divided on the best course of action, but eventually produced the Olive Branch Petition, in which they attempted to come to an accord with King George. The king, however, issued a Proclamation of Rebellion which stated that the states were "in rebellion" and the members of Congress were traitors.


# Creating new state constitutions

In all 13 colonies, Patriots had overthrown their existing governments, closing courts and driving away British officials. They had elected conventions and "legislatures" that existed outside any legal framework; new constitutions were drawn up in each state to supersede royal charters. They declared that they were states, not colonies.

On January 5, 1776, New Hampshire ratified the first state constitution. In May 1776, Congress voted to suppress all forms of crown authority, to be replaced by locally created authority. Virginia, South Carolina, and New Jersey created their constitutions before July 4. Rhode Island and Connecticut simply took their existing royal charters and deleted all references to the crown.[50] The new states were all committed to republicanism, with no inherited offices.


# Independence and Union

In April 1776, the North Carolina Provincial Congress issued the Halifax Resolves explicitly authorizing its delegates to vote for independence.[54] In May, Congress called on all the states to write constitutions and eliminate the last remnants of royal rule. By June, nine colonies were ready for independence; one by one, the last four fell into line: Pennsylvania, Delaware, Maryland, and New York.

The Declaration of Independence was drafted largely by Thomas Jefferson and presented by the committee; it was unanimously adopted by the entire Congress on July 4,[55] and each of the colonies became independent and autonomous. The next step was to form a union to facilitate international relations and alliances.

The Second Continental Congress approved the "Articles of Confederation" for ratification by the states on November 15, 1777; the Congress immediately began operating under the Articles' terms, providing a structure of shared sovereignty during prosecution of the war and facilitating international relations and alliances with France and Spain. The articles were ratified on March 1, 1781. At that point, the Continental Congress was dissolved and a new government of the United States in Congress Assembled took its place on the following day, with Samuel Huntington as presiding officer.


# Defending the Revolution

## British return: 1776–1777

Washington forced the British out of Boston in the spring of 1776, and neither the British nor the Loyalists controlled any significant areas. The British, however, were massing forces at their naval base at Halifax, Nova Scotia. They returned in force in July 1776, landing in New York and defeating Washington's Continental Army in August at the Battle of Brooklyn. Following that victory, they requested a meeting with representatives from Congress to negotiate an end to hostilities.

A delegation including John Adams and Benjamin Franklin met British admiral Richard Howe on Staten Island in New York Harbor on September 11 in what became known as the Staten Island Peace Conference. Howe demanded that the Americans retract the Declaration of Independence, which they refused to do, and negotiations ended. The British then seized New York City and nearly captured Washington's army. They made New York their main political and military base of operations, holding it until November 1783. The city became the destination for Loyalist refugees and a focal point of Washington's intelligence network.

## American alliances after 1778

The capture of a British army at Saratoga encouraged the French to formally enter the war in support of Congress, and Benjamin Franklin negotiated a permanent military alliance in early 1778; France thus became the first foreign nation to officially recognize the Declaration of Independence.

The Spanish and the Dutch became allies of the French in 1779 and 1780 respectively, forcing the British to fight a global war without major allies and requiring it to slip through a combined blockade of the Atlantic.

The northern war subsequently became a stalemate, as the focus of attention shifted to the smaller southern theater.

## The British move South, 1778–1783

The British strategy in America now concentrated on a campaign in the southern states. With fewer regular troops at their disposal, the British commanders saw the "southern strategy" as a more viable plan, as they perceived the south as strongly Loyalist with a large population of recent immigrants and large numbers of slaves who might be captured or run away to join the British.

The British set up a network of forts inland, hoping that the Loyalists would rally to the flag.[71] Not enough Loyalists turned out, however, and the British had to fight their way north into North Carolina and Virginia with a severely weakened army. Behind them, much of the territory that they had already captured dissolved into a chaotic guerrilla war, fought predominantly between bands of Loyalists and American militia, which negated many of the gains that the British had previously made.

## The end of the war

Support for the conflict had never been strong in Britain, where many sympathized with the Americans, but now it reached a new low.[76] King George personally wanted to fight on, but his supporters lost control of Parliament and they launched no further offensives in America.[69][77] War erupted between America and Britain three decades later with the War of 1812, which firmly established the permanence of the United States and its complete autonomy.  


# Paris peace treaty

During negotiations in Paris, the American delegation discovered that France supported American independence but no territorial gains, hoping to confine the new nation to the area east of the Appalachian Mountains. The Americans opened direct secret negotiations with London, cutting out the French. British Prime Minister Lord Shelburne was in full charge of the British negotiations, and he saw a chance to make the United States a valuable economic partner.[81] The US obtained all the land east of the Mississippi River, south of Canada, and north of Florida.

The British largely abandoned their American Indian allies, who were not a party to this treaty and did not recognize it until they were defeated militarily by the United States. However, the British did sell them munitions and maintain forts in American territory until the Jay Treaty of 1795.


# Finance

Britain's war against the Americans, the French, and the Spanish cost about £100 million, and the Treasury borrowed 40-percent of the money that it needed.[87] Heavy spending brought France to the verge of bankruptcy and revolution, while the British had relatively little difficulty financing their war, keeping their suppliers and soldiers paid, and hiring tens of thousands of German soldiers.

Beginning in 1777, Congress repeatedly asked the states to provide money, but the states had no system of taxation and were of little help. By 1780, Congress was making requisitions for specific supplies of corn, beef, pork, and other necessities, an inefficient system which barely kept the army alive.[95][96] Starting in 1776, the Congress sought to raise money by loans from wealthy individuals, promising to redeem the bonds after the war. The bonds were in fact redeemed in 1791 at face value, but the scheme raised little money because Americans had little specie, and many of the rich merchants were supporters of the Crown. The French secretly supplied the Americans with money, gunpowder, and munitions in order to weaken Great Britain; the subsidies continued when France entered the war in 1778, and the French government and Paris bankers lent large sums to the American war effort. These loans were repaid in full in the 1790s.


# Concluding the Revolution

## Creating a "more perfect union" and guaranteeing rights

The war ended in 1783 and was followed by a period of prosperity. The national government was still operating under the Articles of Confederation and was able to settle the issue of the western territories, which the states ceded to Congress. American settlers moved rapidly into those areas, with Vermont, Kentucky, and Tennessee becoming states in the 1790s.

Nationalists led by Washington, Alexander Hamilton, and other veterans feared that the new nation was too fragile to withstand an international war, or even internal revolts such as the Shays' Rebellion of 1786 in Massachusetts. They convinced Congress to call the Philadelphia Convention in 1787 and named their party the Federalist party.[99] The Convention adopted a new Constitution which provided for a much stronger federal government, including an effective executive in a check-and-balance system with the judiciary and legislature.[100] The Constitution was ratified in 1788, after a fierce debate in the states over the nature of the proposed new government. The new government under President George Washington took office in New York in March 1789.[101] James Madison spearheaded Congressional amendments to the Constitution as assurances to those who were cautious about federal power, guaranteeing many of the inalienable rights that formed a foundation for the revolution, and Rhode Island was the final state to ratify the Constitution in 1791.

## National debt

In 1790, Congress combined the remaining state debts with the foreign and domestic debts into one national debt totaling $80 million at the recommendation of first Secretary of the Treasury Alexander Hamilton. Everyone received face value for wartime certificates, so that the national honor would be sustained and the national credit established.


# Ideology and factions

## Ideology behind the Revolution

The American Enlightenment was a critical precursor of the American Revolution. Chief among the ideas of the American Enlightenment were the concepts of Natural Law, Natural Rights, Consent of the Governed, Individualism, Property Rights, Self-Ownership, Self-Determination, liberalism, republicanism, and fear of corruption. A growing number of American colonists embraced these views and fostered an intellectual environment which led to a new sense of political and social identity.

**Liberalism**

Liberalism is a political and moral philosophy based on liberty, consent of the governed, and equality before the law.[1][2][3] Liberals espouse a wide array of views depending on their understanding of these principles, but they generally support limited government, individual rights (including civil rights and human rights), capitalism (free markets), democracy, secularism, gender equality, racial equality, internationalism, freedom of speech, freedom of the press and freedom of religion.

John Locke's (1632–1704) ideas on liberty influenced the political thinking behind the revolution, especially through his indirect influence on English writers such as John Trenchard, Thomas Gordon, and Benjamin Hoadly, whose political ideas in turn had a strong influence on the American Patriots.[106] Locke is often referred to as "the philosopher of the American Revolution" due to his work in the Social Contract and Natural Rights theories that underpinned the Revolution's political ideology.[107] Locke's Two Treatises of Government published in 1689 was especially influential. He argued that all humans were created equally free, and governments therefore needed the "consent of the governed".[108] In late eighteenth-century America, belief was still widespread in "equality by creation" and "rights by creation".

**Republicanism**

Modern republicanism[1] is a guiding political philosophy of the United States that has been a major part of American civic thought since its founding.[2] It stresses liberty and unalienable individual rights as central values, making people sovereign as a whole; rejects monarchy, aristocracy and inherited political power, expects citizens to be virtuous and faithful in their performance of civic duties, and vilifies corruption.

The American ideology called "republicanism" was inspired by the Whig party in Great Britain which openly criticized the corruption within the British government.[112] Americans were increasingly embracing republican values, seeing Britain as corrupt and hostile to American interests.[113] The colonists associated political corruption with luxury and inherited aristocracy, which they condemned. Men had a civic duty to be prepared and willing to fight for the rights and liberties of their countrymen.

**Protestant Dissenters and the Great Awakening**

Protestant churches that had separated from the Church of England (called "dissenters") were the "school of democracy."

Throughout the colonies, dissenting Protestant ministers (Congregational, Baptist, and Presbyterian) preached Revolutionary themes in their sermons, while most Church of England clergymen preached loyalty to the king, the titular head of the English state church.[123] Religious motivation for fighting tyranny transcended socioeconomic lines to encompass rich and poor, men and women, frontiersmen and townsmen, farmers and merchants.

The Declaration of Independence also referred to the "Laws of Nature and of Nature's God" as justification for the Americans' separation from the British monarchy. Most eighteenth-century Americans believed that the entire universe ("nature") was God's creation[124] and he was "Nature's God". Everything was part of the "universal order of things" which began with God and was directed by his providence.[

## Patriots

Those who fought for independence were called "Patriots", "Whigs", "Congress-men", or "Americans" during and after the war. They included a full range of social and economic classes but were unanimous regarding the need to defend the rights of Americans and uphold the principles of republicanism in terms of rejecting monarchy and aristocracy, while emphasizing civic virtue on the part of the citizens. Newspapers were strongholds of patriotism (although there were a few Loyalist papers) and printed many pamphlets, announcements, patriotic letters, and pronouncements.

## Loyalists

he consensus of scholars is that about 15– to 20-percent of the white population remained loyal to the British Crown.[145] Those who actively supported the king were known at the time as "Loyalists", "Tories", or "King's men". The Loyalists never controlled territory unless the British Army occupied it. They were typically older, less willing to break with old loyalties, and often connected to the Church of England; they included many established merchants with strong business connections throughout the Empire, as well as royal officials such as Thomas Hutchinson of Boston.


# Other participants

## France and Spain

In early 1776, France set up a major program of aid to the Americans, and the Spanish secretly added funds. Each country spent one million "livres tournaises" to buy munitions.

## American Indians

Most American Indians rejected pleas that they remain neutral and instead supported the British Crown. The great majority of the 200,000 Indians east of the Mississippi distrusted the Colonists and supported the British cause, hoping to forestall continued colonial expansion into their territories.[159] Those tribes that were more closely involved in trade tended to side with the Patriots, although political factors were important, as well.

Most Indians did not participate directly in the war, except for warriors and bands associated with four of the Iroquois tribes in New York and Pennsylvania which allied with the British. The British did have other allies, especially in the upper Midwest. They provided Indians with funding and weapons to attack American outposts. Some Indians tried to remain neutral, seeing little value in joining what they perceived to be a European conflict, and fearing reprisals from whichever side they opposed. The Oneida and Tuscarora tribes among the Iroquois of central and western New York supported the American cause.[160] The British provided arms to Indians who were led by Loyalists in war parties to raid frontier settlements from the Carolinas to New York. They killed many settlers on the frontier, especially in Pennsylvania and New York's Mohawk Valley.

In 1776, Cherokee war parties attacked American Colonists all along the southern frontier of the uplands throughout the Washington District, North Carolina (now Tennessee) and the Kentucky wilderness area.[162] They would launch raids with roughly 200 warriors, as seen in the Cherokee–American wars; they could not mobilize enough forces to invade Colonial areas without the help of allies, most often the Creek. The Chickamauga Cherokee under Dragging Canoe allied themselves closely with the British, and fought on for an additional decade after the Treaty of Paris was signed. Joseph Brant of the powerful Mohawk tribe in New York was the most prominent Indian leader against the Patriot forces. In 1778 and 1780, he led 300 Iroquois warriors and 100 white Loyalists in multiple attacks on small frontier settlements in New York and Pennsylvania, killing many settlers and destroying villages, crops, and stores.[163] The Seneca, Onondaga, and Cayuga of the Iroquois Confederacy also allied with the British against the Americans.

In 1779, the Americans forced the hostile Indians out of upstate New York when Washington sent an army under John Sullivan which destroyed 40 empty Iroquois villages in central and western New York. The Battle of Newtown proved decisive, as the Patriots had an advantage of three-to-one, and it ended significant resistance; there was little combat otherwise. Sullivan systematically burned the empty villages and destroyed about 160,000 bushels of corn that composed the winter food supply. Facing starvation and homeless for the winter, the Iroquois fled to Canada. The British resettled them in Ontario, providing land grants as compensation for some of their losses.[165]

At the peace conference following the war, the British ceded lands which they did not really control, and they did not consult their Indian allies. They transferred control to the United States of all the land east of the Mississippi and north of Florida. Calloway concludes:

    Burned villages and crops, murdered chiefs, divided councils and civil wars, migrations, towns and forts choked with refugees, economic disruption, breaking of ancient traditions, losses in battle and to disease and hunger, betrayal to their enemies, all made the American Revolution one of the darkest periods in American Indian history.[166]

The British did not give up their forts until 1796 in the eastern Midwest, stretching from Ohio to Wisconsin; they kept alive the dream of forming a satellite Indian nation there, which they called a Neutral Indian Zone. That goal was one of the causes of the War of 1812.

## Black Americans

Free blacks in the North and South fought on both sides of the Revolution, but most fought for the Patriots. Gary Nash reports that there were about 9,000 black Patriots, counting the Continental Army and Navy, state militia units, privateers, wagoneers in the Army, servants to officers, and spies.[169] Ray Raphael notes that thousands did join the Loyalist cause, but "a far larger number, free as well as slave, tried to further their interests by siding with the patriots."[170] Crispus Attucks was shot dead by British soldiers in the Boston Massacre in 1770 and is considered the first American casualty of the Revolutionary War.

Many black slaves sided with the Loyalists. Tens of thousands in the South used the turmoil of war to escape, and the southern plantation economies of South Carolina and Georgia were disrupted in particular. During the Revolution, the British tried to turn slavery against the Americans.

The effects of the war were more dramatic in the South. In Virginia, royal governor Lord Dunmore recruited black men into the British forces with the promise of freedom, protection for their families, and land grants. Tens of thousands of slaves escaped to British lines throughout the South, causing dramatic losses to slaveholders and disrupting cultivation and harvesting of crops. For instance, South Carolina was estimated to have lost about 25,000 slaves to flight, migration, or death—amounting to one third of its slave population. From 1770 to 1790, the black proportion of the population (mostly slaves) in South Carolina dropped from 60.5 percent to 43.8 percent, and from 45.2 percent to 36.1 percent in Georgia.[178]

British forces gave transportation to 10,000 slaves when they evacuated Savannah and Charleston, carrying through on their promise.[179] They evacuated and resettled more than 3,000 Black Loyalists from New York to Nova Scotia, Upper Canada, and Lower Canada. Others sailed with the British to England or were resettled as freedmen in the West Indies of the Caribbean. But slaves who were carried to the Caribbean under control of Loyalist masters generally remained slaves until British abolition in its colonies in 1834. More than 1,200 of the Black Loyalists of Nova Scotia later resettled in the British colony of Sierra Leone, where they became leaders of the Krio ethnic group of Freetown and the later national government. Many of their descendants still live in Sierra Leone, as well as other African countries.


# Effects of the Revolution

## Inspiring all colonies

After the Revolution, genuinely democratic politics became possible in the former colonies.[192] The rights of the people were incorporated into state constitutions. Concepts of liberty, individual rights, equality among men and hostility toward corruption became incorporated as core values of liberal republicanism. The greatest challenge to the old order in Europe was the challenge to inherited political power and the democratic idea that government rests on the consent of the governed. The example of the first successful revolution against a European empire, and the first successful establishment of a republican form of democratically elected government, provided a model for many other colonial peoples who realized that they too could break away and become self-governing nations with directly elected representative government.

The American Revolution was the first wave of the Atlantic Revolutions: the French Revolution, the Haitian Revolution, and the Latin American wars of independence. Aftershocks reached Ireland in the Irish Rebellion of 1798, in the Polish–Lithuanian Commonwealth, and in the Netherlands.

The Revolution had a strong, immediate influence in Great Britain, Ireland, the Netherlands, and France. Many British and Irish Whigs spoke in favor of the American cause. In Ireland, there was a profound impact; the Protestants who controlled Ireland were demanding more and more self-rule. Under the leadership of Henry Grattan, the so-called "Patriots" forced the reversal of mercantilist prohibitions against trade with other British colonies. The King and his cabinet in London could not risk another rebellion on the American model, and made a series of concessions to the Patriot faction in Dublin. Armed Protestant volunteer units were set up to protect against an invasion from France. As in America, so too in Ireland the King no longer had a monopoly of lethal force.[196]

The Revolution, along with the Dutch Revolt (end of the 16th century) and the 17th century English Civil War, was among the examples of overthrowing an old regime for many Europeans who later were active during the era of the French Revolution, such as the Marquis de Lafayette. The American Declaration of Independence influenced the French Declaration of the Rights of Man and the Citizen of 1789.[197][198] The spirit of the Declaration of Independence led to laws ending slavery in all the Northern states and the Northwest Territory, with New Jersey the last in 1804. States such as New Jersey and New York adopted gradual emancipation, which kept some people as slaves for more than two decades longer.
