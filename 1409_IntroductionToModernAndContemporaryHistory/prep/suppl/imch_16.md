*Why is Africa Poor?, University of Groningen*

Africa is the poorest continent in the world in terms of GDP per-capita and the majority of the world’s poor live in Africa.

Good institutional explanations for this today at the macro level:
- predatory and kleptocratic rule, weak states unable to enforce rules, order or provide public goods, lack of mechanisms of national accountability allowing rent extraction.
- at the micro level: mechanisms for allocating land, lack of mechanisms of local accountability (chiefs), social institutions of mutual obligation.

When and how did institutions become antithetical to development - extractive - in Africa? Most crucial aspect is the relative lack of political centralization compared to Eurasia.

When did African Institutions Become Relatively Extractive? Although it may have been true that the potential physical productivity of labor would have been high because of the factor proportions, the appropriable product would have been much lower because institutions were extractive.

One economic and political institutions are extractive this can easily lead to a vicious circle. Imagine economic institutions lead to slave exports. The dynamics this set up perpetuated and even intensified extractive institutions. 1) They made slavery more intense in Africa and human rights very insecure. 2) They destroyed states and created others which were based on rent (slave) seeking. 3) They created political instability.

There are at least three views on the impact of colonialism:
1. Too short to influence anything (Jeffrey Herbst)
2. The Eccentric Consensus: colonial officials and academics such as Peter Bauer and Niall Ferguson agree with Marxists like Lenin and Bill Warren’s Imperialism: Pioneer of Capitalism that colonialism stimulated economic growth in Africa: it brought modern institutions, technologies and organizational forms and Europeans would not have bothered understanding what caused tropical diseases if they had not been intent on colonization.
3. Most important source of African underdevelopment - the locus classicus of which is Walter Rodney’s How Europe Underdeveloped Africa.

# Positive Arguments

Europeans ended slavery, introducing modern legal systems and methods of administration and eventually constructing modern democratic institutions.

Economic institutions improved, not just because slavery was abolished but also because Europeans made property rights secure and brought to an end conflicts within African society (which they had previously heavily exacerbated by generously supplying anyone who could pay with firearms, Inikori, 1977).

They also started schools where there were none.


# Negative Arguments

Perverse e¤ects of particular colonial institutions, such as agricultural marketing boards (Bates, 1981).

Colonialism created an arbitrary state system which has led to political con‡icts, instability and dictatorship (Engelbert, 2000).

Colonial authorities created “gate-keeper states” which were only interested in ruling rather than in developing the countries and these have left a path dependent legacy (Cooper, 2002).

The political authoritarianism of the colonial state is a direct source of the authoritarianism that has plagued Africa (Young, 1994) (Smith and Mugabe).

Structure of colonial indirect rule warped local institutions of accountability making them more autocratic (Mamdani, 1996).

Colonialism created and shaped identities and cleavages in dysfunctional ways (Hutu and Tutsi).


# Conclusions on Colonialism

Africa was the poor and technologically backward in the first half of the 19th century. Nevertheless, so were other parts of the world, Japan, Thailand, most of Latin America, and they are much more prosperous today.

Colonialism did bring some benefits in terms of technology, peace and an access to and implantation of modern institutions. Yet little attempt was really made to make such benefits endure, and many were restricted to the colonial period. They also brought racism, discrimination, inequality and seriously warped many African political and economic institutions.

Once the European powers left, much of what was positive was ephemeral and went into reverse while many of the negatives endured.


# What Set this Vicious Circle in Motion?

But colonialism and post-colonial economic decline are only the most recent incarnations of a long vicious circle of extractive institutions in Africa. Why did Africa get onto this path? Let me end by presenting three different hypotheses:

## A Vansinerian Model - Fundamental Differences

Africa differed in fundamental ways from the rest of the world that set it on a different path of economic and political development. Which fundamentals? Consider political centralization. A simple hypothesis would relate this to geography - agricultural productivity was low, population density low, state formation retarded.

Problem with this view: Political centralization is uncorrelated with population density in Africa!

Alternative Hypothesis: African social structures such as Age Sets make it very difficult to concentrate political power. Political centralization involves one clan/lineage dominating another (Percys versus Nevilles in England’s Wars of the Roses, the clan of Ndori as described by Jan Vansina is the creation of the Nyiginya Kingdom (Rwandan state) in the 17th century).

## A Brennerian Model - Slight Initial Di¤erences

Extractive institutions have been the norm in world history. Western Europe diverged because of a series of idiosyncratic shocks interacting with small institutional differences. Robert Brenner hypothesized that the divergence between Western and Eastern Europe in the aftermath of a common shock - the Black Death - was because of slight differences in institutions - in the West peasants were a bit more organized, landed estates smaller, Lords less powerful.

## An Gibbensian Model - Different Shocks

An alternative is that Africa experienced very different shocks than Eurasia did. The basic extractive economic institutions of the absolutist Ethiopian empire, such as gult, and the feudalism created after the decline of Aksum, lasted until they were abolished after the 1974 revolution. Africa as Ethiopia (or maybe Bhutan)? Sufficiently isolated from global shocks that extractive institutions remained unperturbed?

****


*Class structure in Latin America, Encyclopedia.com*

The class structures of Latin America are determined by the social relationships of basic economic activities. These relationships include property ownership, labor arrangements, forms and sources of income, and patterns of supervision and subordination, among others. In addition, some groups of people may be confined to certain jobs or discriminated against on the basis of gender, race, ethnicity, and so on. All of these factors contribute to the formation and characteristics of contemporary social classes. Given the great diversity among Latin American countries, the following discussion should be considered mainly as a portrayal of general regional patterns.


# CLASS STRUCTURE IN HISTORICAL PERSPECTIVE

To understand modern-day class structures, it is imperative to review the historical forces that have shaped them since the mid-nineteenth century. Between roughly 1850 and 1930, national governments pursued an "outward-looking" development model based on the export of primary agricultural and mineral commodities and the import of manufactured goods from Europe (and later the United States). The creation of large-scale export economies entailed profound transformations of class relations.

In the countryside, landholding patterns were altered, legally and/or forcibly, to facilitate the creation of large enterprises devoted to export crops, such as coffee in Central America, northern South America, and Brazil, and wheat in the Southern Cone. Estate labor needs were met by transforming rural migrants into full-time or part-time laborers. Where labor was scarce, as in Argentina and southern Brazil, European immigrants were contracted. In the Caribbean, meanwhile, growth was driven by the creation of a banana export economy and the revival of sugar, mainly on foreign-owned plantations. Labor forces were largely recruited from among ex-slaves and their descendants.

These developments had several important effects on social structures. Landowning classes were greatly empowered, politically as well as economically. The industrial-style organization of plantation and mining economies facilitated the emergence of the first large labor unions in the region. In the cities, the largest urban merchants linked to export-import trade began to emerge among national elites. Manufacturing remained relatively small and largely artisanal in nature. Middle classes were weak, although by the turn of the century they were expanding in the larger countries with the growth of public sector employment.


# RURAL CLASS STRUCTURES SINCE THE 1990S

The traditional relationships of landlord and peasant that helped to de ne Latin American societies all but disappeared in the 1990s. Large estates are no longer so large, in many cases having been curtailed by agrarian reform in the 1960s, and farms devoted to exports are more likely to be run according to strictly capitalistic criteria. Moreover, rural society in general is much more closely linked to the major urban centers and national economic and political forces. The incidence of poverty, however, remains 60 percent of the rural population for Latin America as a whole.

The rural population has both gained and suffered as a result of these changes. On the one hand, rural people are freer to organize themselves, pursue political alliances, and negotiate with state agencies. The disappearance of military regimes has also contributed to the rise of organized rural activism in countries such as Bolivia, Brazil, and Peru. Rural educational levels and access to basic services have risen as well. On the other hand, the advancing modernization of agriculture has reduced employment opportunities. In the context of continuing population growth, the migratory out ow from the countryside has continued.


# URBAN CLASS STRUCTURES SINCE THE 1990S

The dominant classes are a very small proportion of the economically active population in modern Latin American societies. Their ranks are characterized by diverse interests, however, including the traditional export and mercantile sectors, transnational and domestic manufacturing, services (banking in particular), and even large state-owned enterprises. The crisis of the 1980s and the ascendance of neoliberalism have altered the dominant class profile. Privatization policies are gradually doing away with state enterprises; transnational rfims have acquired many of them. Among private firms, those best able to cope with prevailing economic conditions have been those least reliant on the domestic market and more geared toward nontraditional exports, as well as those with the capacity to spread their resources across borders and foreign currencies. Increasingly, transnational capital refers not just to U.S.-, European-, or Asian-based firms operating in Latin America but also to Latin American firms and investors who have looked abroad for greater security or greater returns.


# SIGNIFICANCE OF OTHER SOCIAL CLEAVAGES

It is important to recognize that other social divisions cut across class structure and help to determine the specific characteristics and identities associated with social classes.

The most important of these distinctions are gender and ethnicity. Between 1950 and 1980 Latin America witnessed a slow but steady expansion of women's participation in the labor force. This was largely the consequence of broader patterns of social change, including urbanization, higher educational achievement, evolving labor markets, and associated changes in cultural values. Women's employment was highly segmented, however, with jobs concentrated in the areas of personal (including domestic) services, office services, and sales.

Ethnic divisions represent a defining aspect of Latin American and Caribbean class structures. The colonial reliance on native Americans and African slaves as a labor force for European conquerors has been an enduring legacy. Despite the vast changes that have since occurred, it remains true that throughout the hemisphere, to be identifiably Indian or black is to rank lower in economic, political, and social status than those who are nonindigenous or non-black. There is great variation across countries and regions in how these inequalities are expressed, but a few major trends are worth noting.


# CONSOLIDATION OF HETEROGENEITY IN CLASS STRUCTURES

The Latin American class structure of the early twenty-first century is more heterogeneous than that of the 1950s–1980s.

In the first place, poverty, informality, and social exclusion have become a massive urban phenomenon.

A second characteristic of the class structure is the implicit duality of the formal and informal economy and society. Originally interpreted as a short-term under- and unemployment phenomenon, employment in the informal economy appears in the early twenty- first century to be consolidated, and the informal economy thus shapes a kind of informal society, partially inserted in the formal order and partially forming a parallel social structure with its internal social hierarchies. Ethnicity is a stratifying factor within the informal economy and society. Mechanisms for survival predominate: ties of ethnicity, religion, real or symbolic family relationships, closeness to the place of birth, local neighborhood relations. In the Andean countries, Central America, and Mexico, features of Quechua or Maya culture mix with elements of informal society. In Table 1 one can observe the over-time consolidation of the formal-cum-informal order.

There are some marked changes within the Latin American urban class structures. The chronically poor are now joined by the "new poor," descending from the former strata of the middle and industrial working classes. Old and new poor converge as informal micro-entrepreneurs and self-employed in search of survival and livelihood strategies.
