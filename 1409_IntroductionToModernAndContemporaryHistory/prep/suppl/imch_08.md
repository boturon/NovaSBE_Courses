*French Revolution, Wikipedia*

The French Revolution was a period of far-reaching social and political upheaval in France and its colonies beginning in 1789. The Revolution overthrew the monarchy, established a republic, catalyzed violent periods of political turmoil, and finally culminated in a dictatorship under Napoleon who brought many of its principles to areas he conquered in Western Europe and beyond. Inspired by liberal and radical ideas, the Revolution profoundly altered the course of modern history, triggering the global decline of absolute monarchies while replacing them with republics and liberal democracies.[1] Through the Revolutionary Wars, it unleashed a wave of global conflicts that extended from the Caribbean to the Middle East. Historians widely regard the Revolution as one of the most important events in human history.

The Seven Years' War was a global conflict fought between 1756 and 1763. It involved every European great power of the time and spanned five continents, affecting Europe, the Americas, West Africa, South Asia, and the Philippines. The conflict split Europe into two coalitions: one was led by the Kingdom of Great Britain and included the Kingdom of Prussia, the Kingdom of Portugal, the Electorate of Brunswick-Lüneburg, and other small German states; while the other was led by the Kingdom of France and included the Austrian-led Holy Roman Empire, the Russian Empire (until 1762), the Kingdom of Spain, and the Swedish Empire. Meanwhile, in India, some regional polities within the increasingly fragmented Mughal Empire, with the support of the French, tried to crush a British attempt to conquer Bengal.

The American Revolution was a colonial revolt that took place between 1765 and 1783. The American Patriots in the Thirteen Colonies won independence from Great Britain, becoming the United States of America. They defeated the British in the American Revolutionary War (1775–1783) in alliance with France.

Following the Seven Years' War and the American Revolution,[5] the French government was deeply in debt. It attempted to restore its financial status through unpopular taxation schemes, which were heavily regressive. Leading up to the Revolution, years of bad harvests worsened by deregulation of the grain industry and environmental problems also inflamed popular resentment of the privileges enjoyed by the aristocracy and the Catholic clergy of the established church. Some historians hold something similar to what Thomas Jefferson proclaimed: that France had "been awakened by our [American] Revolution."[6] Demands for change were formulated in terms of Enlightenment ideals and contributed to the convocation of the Estates General in May 1789. During the first year of the Revolution, members of the Third Estate (commoners) took control, the Bastille was attacked in July, the Declaration of the Rights of Man and of the Citizen was passed in August, and the Women's March on Versailles forced the royal court back to Paris in October. A central event of the first stage, in August 1789, was the abolition of feudalism and the old rules and privileges left over from the Ancien Régime.

The next few years featured political struggles between various liberal assemblies and right-wing supporters of the monarchy intent on thwarting major reforms. Such divisions led to massacres and various skirmishes. The Republic was proclaimed in September 1792 after the French victory at Valmy. In a momentous event that led to international condemnation, Louis XVI was executed in January 1793.

External threats closely shaped the course of the Revolution. The Revolutionary Wars beginning in 1792 ultimately featured French victories that facilitated the conquest of the Italian Peninsula, the Low Countries and most territories west of the Rhine – achievements that had eluded previous French governments for centuries. Internally, popular agitation radicalised the Revolution significantly, culminating in the rise of Maximilien Robespierre and the Jacobins. The dictatorship imposed by the Committee of Public Safety during the Reign of Terror, from 1793 until 1794, established price controls on food and other items, abolished slavery in French colonies abroad, de-established the Catholic church (dechristianised society) and created a secular Republican calendar, religious leaders were expelled, and the borders of the new republic were secured from its enemies.

After the Thermidorian Reaction, an executive council known as the Directory assumed control of the French state in 1795. They suspended elections, repudiated debts (creating financial instability in the process), persecuted the Catholic clergy, and made significant military conquests abroad.[7] Dogged by charges of corruption, the Directory collapsed in a coup led by Napoleon Bonaparte in 1799. Napoleon, who became the hero of the Revolution through his popular military campaigns, established the Consulate and later the First Empire, setting the stage for a wider array of global conflicts in the Napoleonic Wars

The modern era has unfolded in the shadow of the French Revolution. Almost all future revolutionary movements looked back to the Revolution as their predecessor.[8] Its central phrases and cultural symbols, such as La Marseillaise and Liberté, fraternité, égalité, ou la mort, became the clarion call for other major upheavals in modern history, including the Russian Revolution over a century later.[9]


# Causes

Historians have pointed to many events and factors within the Ancien Régime that led to the Revolution. Rising social and economic inequality,[13][14] new political ideas emerging from the Enlightenment,[15] economic mismanagement, environmental factors leading to agricultural failure, unmanageable national debt,[16] and political mismanagement on the part of King Louis XVI have all been cited as laying the groundwork for the Revolution.

The economy in the Ancien Régime during the years preceding the Revolution suffered from instability. The sequence of events leading to the Revolution included the national government's fiscal troubles caused by an unjust, inefficient and deeply hated tax system – the ferme générale – and by expenditure on numerous large wars.

Despite succeeding in gaining independence for the Thirteen Colonies, France was severely indebted by the American Revolutionary War. France's inefficient and antiquated financial system could not finance this debt.[29] Faced with a financial crisis, the king called an Estates General, recommended by the Assembly of Notables in 1787 for the first time in over a century.

As with most monarchies, the upper class was always ensured a stable living, so while the rich remained very wealthy, the majority of the French population was starving. Many were so destitute that they couldn't even feed their families and resorted to theft or prostitution to stay alive. Meanwhile, the royal court at Versailles was isolated from and indifferent to the escalating crisis.


# Ancien Régime

## Financial crisis

In 1774 Louis XVI ascended to the throne in the middle of a financial crisis in which the state was faced with a budget deficit and was nearing bankruptcy.[34] This was due in part to France's costly involvements in the Seven Years' War and later the American Revolutionary War.

Necker realised that the country's extremely regressive tax system subjected the lower classes to a heavy burden,[36] while numerous exemptions existed for the nobility and clergy.[37] He argued that the country could not be taxed higher; that tax exemptions for the nobility and clergy must be reduced; and proposed that borrowing more money would solve the country's fiscal shortages.

Charles Alexandre de Calonne was appointed to the Comptrollership.[36] Calonne initially spent liberally, but he quickly realised the critical financial situation and proposed a new tax code. The proposal included a consistent land tax, which would include taxation of the nobility and clergy. Faced with opposition from the parlements, Calonne organised the summoning of the Assembly of Notables. But the Assembly failed to endorse Calonne's proposals and instead weakened his position through its criticism. In response, the King announced the calling of the Estates-General for May 1789, the first time the body had been summoned since 1614. This was a signal that the Bourbon monarchy was in a weakened state and subject to the demands of its people.[39].

## Estates-General of 1789

The Estates-General was organised into three estates: the clergy, the nobility, and the rest of France. It had last met in 1614. Elections were held in the spring of 1789; suffrage requirements for the Third Estate were for French-born or naturalised males, aged 25 years or more, who resided where the vote was to take place and who paid taxes.

## National Assembly (1789)

On 10 June 1789 Abbé Sieyès moved that the Third Estate, now meeting as the Communes (English: "Commons") proceed with verifying its own powers and invite the other two estates to take part, but not to wait for them. They proceeded to do so two days later, completing the process on 17 June.[49] Then they voted a measure far more radical, declaring themselves the National Assembly, an assembly not of the Estates but of "the People". They invited the other orders to join them, but made it clear they intended to conduct the nation's affairs with or without them.

In an attempt to keep control of the process and prevent the Assembly from convening, Louis XVI ordered the closure of the Salle des États where the Assembly met, making an excuse that the carpenters needed to prepare the hall for a royal speech in two days. Weather did not allow an outdoor meeting, and fearing an attack ordered by Louis XVI, they met in a tennis court just outside Versailles, where they proceeded to swear the Tennis Court Oath (20 June 1789) under which they agreed not to separate until they had given France a constitution. A majority of the representatives of the clergy soon joined them, as did 47 members of the nobility. By 27 June, the royal party had overtly given in, although the military began to arrive in large numbers around Paris and Versailles. Messages of support for the Assembly poured in from Paris and other French cities.


# Constitutional monarchy

## National Constituent Assembly (1789-1791)

On 11 July 1789, after Necker published an inaccurate account of the government's debts and made it available to the public, the King fired him, and completely restructured the finance ministry at the same time.

Many Parisians presumed Louis' actions to be aimed against the Assembly and began open rebellion when they heard the news the next day. They were also afraid that arriving soldiers – mostly foreign mercenaries – had been summoned to shut down the National Constituent Assembly. The Assembly, meeting at Versailles, went into nonstop session to prevent another eviction from their meeting place. Paris was soon consumed by riots, chaos, and widespread looting. The mobs soon had the support of some of the French Guard, who were armed and trained soldiers.

On 14 July, the insurgents set their eyes on the large weapons and ammunition cache inside the Bastille fortress, which was also perceived to be a symbol of royal power. After several hours of combat, the prison fell that afternoon.

As civil authority rapidly deteriorated, with random acts of violence and theft breaking out across the country, members of the nobility, fearing for their safety, fled to neighbouring countries; many of these émigrés, as they were called, funded counter-revolutionary causes within France and urged foreign monarchs to offer military support to a counter-revolution.

By late July, the spirit of popular sovereignty had spread throughout France. In rural areas, many commoners began to form militias and arm themselves against a foreign invasion: some attacked the châteaux of the nobility as part of a general agrarian insurrection known as "la Grande Peur" ("the Great Fear"). In addition, wild rumours and paranoia caused widespread unrest and civil disturbances that contributed to the collapse of law and order.

On 4 and 11 August 1789 the National Constituent Assembly abolished privileges and feudalism (numerous peasant revolts had almost brought feudalism to an end) in the August Decrees, sweeping away personal serfdom,[58] exclusive hunting rights and other seigneurial rights of the Second Estate (nobility).

Also the tithe (a 10% tax for the Church, gathered by the First Estate (clergy)), which had been the main source of income for many clergymen, was abolished.[59] During the course of a few hours nobles, clergy, towns, provinces, companies and cities lost their special privileges.

On 26 August 1789 the Assembly published the Declaration of the Rights of Man and of the Citizen, which comprised a statement of principles rather than a constitution with legal effect. The Declaration was directly influenced by Thomas Jefferson working with General Lafayette, who introduced it. The National Constituent Assembly functioned not only as a legislature, but also as a body to draft a new constitution.

Fuelled by rumours of a reception for the King's bodyguards on 1 October 1789, at which the national cockade had been trampled upon, on 5 October 1789, crowds of women began to assemble at Parisian markets. The women first marched to the Hôtel de Ville, demanding that city officials address their concerns.[68] The women were responding to the harsh economic situations they faced, especially bread shortages. They also demanded an end to royal efforts to block the National Assembly, and for the King and his administration to move to Paris as a sign of good faith in addressing the widespread poverty.

On 6 October 1789, the King and the royal family moved from Versailles to Paris under the "protection" of the National Guards, thus legitimising the National Assembly.

The Revolution caused a massive shift of power from the Roman Catholic Church to the state.[70] Under the Ancien Régime, the Church had been the largest single landowner in the country, owning about 10% of the land in the kingdom.[71] The Church was exempt from paying taxes to the government, while it levied a tithe – a 10% tax on income, often collected in the form of crops – on the general population, only a fraction of which it then redistributed to the poor.

In autumn 1789, legislation abolished monastic vows and on 13 February 1790 all religious orders were dissolved.[76] Monks and nuns were encouraged to return to private life and a small percentage did eventually marry.

A new Republican Calendar was established in 1793, with 10-day weeks that made it very difficult for Catholics to remember Sundays and saints' days. Workers complained it reduced the number of first-day-of-the-week holidays from 52 to 37.  

During the Reign of Terror, extreme efforts of de-Christianisation ensued, including the imprisonment and massacre of priests and destruction of churches and religious images throughout France. An effort was made to replace the Catholic Church altogether, with civic festivals replacing religious ones. The establishment of the Cult of Reason was the final step of radical de-Christianisation.

This period also saw the rise of the political "clubs" in French politics. Foremost among these was the Jacobin Club; 152 members had affiliated with the Jacobins by 10 August 1790. The Jacobin Society began as a broad, general organisation for political debate, but as it grew in members, various factions developed with widely differing views. Several of these factions broke off to form their own clubs, such as the Club of '89.[89]

In late 1790, the Assembly continued to work on developing a constitution. A new judicial organisation made all magistracies temporary and independent of the throne. The legislators abolished hereditary offices, except for the monarchy itself. Jury trials started for criminal cases. The King would have the unique power to propose war, with the legislature then deciding whether to declare war.

The Assembly abolished all internal trade barriers and suppressed guilds, masterships, and workers' organisations: any individual gained the right to practise a trade through the purchase of a license; strikes became illegal.

On the night of 20 June 1791 the royal family fled the Tuileries Palace dressed as servants, while their servants dressed as nobles. However, late the next day, the King was recognised and arrested at Varennes and returned to Paris. The Assembly provisionally suspended the King. He and Queen Marie Antoinette remained held under guard.[91] The King's flight had a profound impact on public opinion, turning popular sentiment further against the clergy and nobility, and built momentum for the institution of a constitutional monarchy.

Meanwhile, in August 1791, a new threat arose from abroad: the King's brother-in-law Holy Roman Emperor Leopold II, King Frederick William II of Prussia, and the King's brother Charles-Philippe, comte d'Artois, issued the Declaration of Pillnitz, declaring their intention to bring the French king in the position "to consolidate the basis of a monarchical government" and that they were preparing their own troops for action,[94] hinting at an invasion of France on the King's behalf.[95]

Although Leopold himself sought to avoid war and made the declaration to satisfy the Comte d'Artois and the other émigrés, the reaction within France was ferocious. The French people expressed no respect for the dictates of foreign monarchs, and the threat of force merely hastened their militarisation.

Even before the Flight to Varennes, the Assembly members had determined to debar themselves from the legislature that would succeed them, the Legislative Assembly. They now gathered the various constitutional laws they had passed into a single constitution, and submitted it to the recently restored Louis XVI, who accepted it, writing "I engage to maintain it at home, to defend it from all attacks from abroad, and to cause its execution by all the means it places at my disposal". The King addressed the Assembly and received enthusiastic applause from members and spectators. With this capstone, the National Constituent Assembly adjourned in a final session on 30 September 1791.

## Legislative Assembly (1791-1792)

The Legislative Assembly first met on 1 October 1791, elected by those 4 million men – out of a population of 25 million – who paid a certain minimum amount of taxes.[98] Under the Constitution of 1791, France would function as a constitutional monarchy. The King had to share power with the elected Legislative Assembly, but he retained his royal veto and the ability to select ministers. Early on, the King vetoed legislation that threatened the émigrés with death and that decreed that every non-juring clergyman must take within eight days the civic oath mandated by the Civil Constitution of the Clergy. Over the course of a year, such disagreements would lead to a constitutional crisis.

Late in 1791, a group of Assembly members who propagated war against Austria and Prussia was, after some remark of politician Maximilien Robespierre, henceforth indicated as the 'Girondins', although not all of them really came from the southern province of Gironde. A group around Robespierre – later indicated as 'Montagnards' or 'Jacobins' – pleaded against that war; this opposition between those groups would harden and embitter in the next 11/2 years.

In response to the threat of war of August 1791 from Austria and Prussia, leaders of the Assembly saw such a war as a means to strengthen support for their revolutionary government, and the French people as well as the Assembly thought that they would win a war against Austria and Prussia. On 20 April 1792, France declared war on Austria.[94][99] Late April 1792, France invaded and conquered the Austrian Netherlands (roughly present-day Belgium and Luxembourg).

The Legislative Assembly degenerated into chaos before October 1792. The Constituent Assembly had liberal, rational, and individualistic goals that seem to have been largely achieved by 1791. However, it failed to consolidate the gains of the Revolution, which continued with increasing momentum and escalating radicalism until 1794. There are six reasons for this escalation:
1. the king did not accept the limitations on his powers, and mobilised support from foreign monarchs to reverse it.
2. the effort to overthrow the Roman Catholic Church, sell off its lands, close its monasteries and its charitable operations, and replace it with an unpopular makeshift system caused deep consternation among the pious and the peasants.
3. the economy was badly hurt by the issuance of ever increasing amounts of paper money (assignats), which caused more and more inflation; the rising prices hurt the urban poor who spent most of their income on food.
4. the rural peasants demanded liberation from the heavy system of taxes and dues owed to local landowners.
5. the working class of Paris and the other cities – the sans-culottes – resented the fact that the property owners and professionals had taken all the spoils of the Revolution.
6. foreign powers threatened to overthrow the Revolution, which responded with extremism and systematic violence in its own defence.

Chaos persisted until the Convention, elected by universal male suffrage and charged with writing a new constitution, met on 20 September 1792 and became the new de facto government of France. The next day it abolished the monarchy and declared a republic. The following day – 22 September 1792, the first morning of the new Republic – was later retroactively adopted as the beginning of Year One of the French Republican Calendar.


# French Revolutionary Wars and Napoleonic Wars

From 1793 to 1815 France was engaged almost continuously (with two short breaks) in wars with Britain and a changing coalition of other major powers. The many French successes led to the spread of the French revolutionary ideals into neighbouring countries, and indeed across much of Europe. However, the final defeat of Napoleon in 1814 (and 1815) brought a reaction that reversed some – but not all – of the revolutionary achievements in France and Europe. The Bourbons were restored to the throne, with the brother of King Louis XVI becoming King Louis XVIII.

The politics of the period inevitably drove France towards war with Austria and its allies. The King, many of the Feuillants, and the Girondins specifically wanted to wage war. The King (and many Feuillants with him) expected war would increase his personal popularity; he also foresaw an opportunity to exploit any defeat: either result would make him stronger. The Girondins wanted to export the Revolution throughout Europe and, by extension, to defend the Revolution within France. The forces opposing war were much weaker. Barnave and his supporters among the Feuillants feared a war they thought France had little chance to win and which they feared might lead to greater radicalisation of the revolution. On the other end of the political spectrum Robespierre opposed a war on two grounds, fearing that it would strengthen the monarchy and military at the expense of the revolution, and that it would incur the anger of ordinary people in Austria and elsewhere.

The new-born Republic followed up on this success with a series of victories in Belgium and the Rhineland in the fall of 1792. The French armies defeated the Austrians at the Battle of Jemappes on 6 November, and had soon taken over most of the Austrian Netherlands. This brought them into conflict with Britain and the Dutch Republic, which wished to preserve the independence of the southern Netherlands from France. After the French king's execution in January 1793, these powers, along with Spain and most other European states, joined the war against France. Almost immediately, French forces suffered defeats on many fronts, and were driven out of their newly conquered territories in the spring of 1793. At the same time, the republican regime was forced to deal with rebellions against its authority in much of western and southern France. But the allies failed to take advantage of French disunity, and by the autumn of 1793 the republican regime had defeated most of the internal rebellions and halted the allied advance into France itself.

This stalemate ended in the summer of 1794 with dramatic French victories. These victories led to the collapse of the anti-French coalition. Prussia, having effectively abandoned the coalition in the fall of 1794, made peace with revolutionary France at Basel in April 1795, and soon thereafter Spain also made peace with France. Britain and Austria were the only major powers to remain at war with France.

# First Republic

## National Convention (1792–1795)

Late in August 1792, elections were held, now under male universal suffrage, for the new National Convention,[109] which replaced the Legislative Assembly on 20 September 1792. From the start the Convention suffered from the bitter division between a group around Robespierre, Danton and Marat, referred to as 'Montagnards' or 'Jacobins' or the 'left', and a group referred to as 'Girondins' or the 'right'.

mmediately on 21 September the Convention abolished the monarchy, making France the French First Republic.[102] A new French Republican Calendar was introduced to replace the Christian Gregorian calendar, renaming the year 1792 as year 1 of the Republic.

In the Brunswick Manifesto, the Imperial and Prussian armies threatened retaliation on the French population if it were to resist their advance or the reinstatement of the monarchy. The former Louis XVI, now simply named Citoyen Louis Capet (Citizen Louis Capet) was executed by guillotine on 21 January 1793 on the Place de la Révolution, former Place Louis XV, now called the Place de la Concorde.[115] Conservatives across Europe were horrified and monarchies called for war against revolutionary France.

When war went badly, prices rose and the sans-culottes – poor labourers and radical Jacobins – rioted; counter-revolutionary activities began in some regions.  An alliance of Jacobin and sans-culottes elements thus became the effective centre of the new government. Policy became considerably more radical, as "The Law of the Maximum" set food prices and led to executions of offenders.[118]

The price control policy was coeval with the rise to power of the Committee of Public Safety and the Reign of Terror. The Committee first attempted to set the price for only a limited number of grain products, but by September 1793 it expanded the "maximum" to cover all foodstuffs and a long list of other goods.[119] Widespread shortages and famine ensued. The Committee reacted by sending dragoons into the countryside to arrest farmers and seize crops. This temporarily solved the problem in Paris, but the rest of the country suffered. By the spring of 1794, forced collection of food was not sufficient to feed even Paris, and the days of the Committee were numbered. When Robespierre went to the guillotine in July 1794, the crowd jeered, "There goes the dirty maximum!"

The Committee of Public Safety came under the control of Maximilien Robespierre, a lawyer, and the Jacobins unleashed the Reign of Terror (1793–94). According to archival records, at least 16,594 people died under the guillotine or otherwise after accusations of counter-revolutionary activities.[121] As many as 40,000 accused prisoners may have been summarily executed without trial or died awaiting trial.

The instalment of the Republican Calendar on 24 October 1793 caused an anti-clerical uprising. Hébert's and Chaumette's atheist movement campaigned to dechristianise society. The climax was reached with the celebration of the flame of Reason in Notre Dame Cathedral on 10 November.

After July 1794, the French government was dominated by 'Girondins', who indulged in revenge and violence and death sentences against people associated with the previous 'Jacobin'/'Montagnard' governments around Robespierre and Marat, in what was known as the White Terror.[154] The Jacobin Club was closed and banned.

After July 1794, most civilians henceforth ignored the Republican calendar and returned to the traditional seven-day weeks.

## The Directory (1795–1799)

The Convention on 22 August 1795 approved the new "Constitution of the Year III". A French plebiscite ratified the document, with about 1,057,000 votes for the constitution and 49,000 against.[161] The results of the voting were announced on 23 September 1795, and the new constitution took effect on 27 September 1795.[161] The new constitution created the Directoire (English: Directory) with a bicameral legislature.

The first chamber was called the 'Council of 500' initiating the laws, the second the 'Council of Elders' reviewing and approving or not the passed laws. Each year, one-third of the chambers was to be renewed. The executive power was in the hands of the five members (directors) of the Directory with a five-year mandate.

Although committed to Republicanism, the Directory distrusted democracy.

The allies scored a series of victories that rolled back French successes, retaking Italy, Switzerland and the Netherlands and ending the flow of payments from the conquered areas to France. The treasury was empty. Despite his publicity claiming many glorious victories, Napoleon's army was trapped in Egypt after the British sank the French fleet at the Battle of the Nile. Napoleon escaped by himself, returned to Paris and overthrew the Directory in November 1799.

The allies scored a series of victories that rolled back French successes, retaking Italy, Switzerland and the Netherlands and ending the flow of payments from the conquered areas to France. The treasury was empty. Despite his publicity claiming many glorious victories, Napoleon's army was trapped in Egypt after the British sank the French fleet at the Battle of the Nile. Napoleon escaped by himself, returned to Paris and overthrew the Directory in November 1799.

Napoleon conquered most of Italy in the name of the French Revolution in 1797–99. He consolidated old units and split up Austria's holdings. He set up a series of new republics, complete with new codes of law and abolition of old feudal privileges.


# Role of Women

Historians since the late 20th century have debated how women shared in the French Revolution and what long-term impact it had on French women. Women had no political rights in pre-Revolutionary France; they were considered "passive" citizens; forced to rely on men to determine what was best for them. That changed dramatically in theory as there seemingly were great advances in feminism. Feminism emerged in Paris as part of a broad demand for social and political reform.


# Economic policies

The French Revolution abolished many of the constraints on the economy that had slowed growth during the ancien regime. It abolished tithes owed to local churches as well as feudal dues owed to local landlords. The result hurt the tenants, who paid both higher rents and higher taxes.[210] It nationalised all church lands, as well as lands belonging to royalist enemies who went into exile. It planned to use these seized lands to finance the government by issuing assignats. It abolished the guild system as a worthless remnant of feudalism.[211] It also abolished the highly inefficient system of tax farming, whereby private individuals would collect taxes for a hefty fee. The government seized the foundations that had been set up (starting in the 13th century) to provide an annual stream of revenue for hospitals, poor relief, and education. The state sold the lands but typically local authorities did not replace the funding and so most of the nation's charitable and school systems were massively disrupted.

The economy did poorly in 1790–96 as industrial and agricultural output dropped, foreign trade plunged, and prices soared. The government decided not to repudiate the old debts. Instead it issued more and more paper money (called "assignat") that supposedly were grounded seized lands. The result was escalating inflation. The government imposed price controls and persecuted speculators and traders in the black market. People increasingly refused to pay taxes as the annual government deficit increased from 10% of gross national product in 1789 to 64% in 1793. By 1795, after the bad harvest of 1794 and the removal of price controls, inflation had reached a level of 3500%. The assignats were withdrawn in 1796 but the replacements also fuelled inflation. The inflation was finally ended by Napoleon in 1803 with the franc as the new currency.

Napoleon after 1799 paid for his expensive wars by multiple means, starting with the modernisation of the rickety financial system.[214] He conscripted soldiers at low wages, raised taxes, placed large-scale loans, sold lands formerly owned by the Catholic Church, sold Louisiana to the United States, plundered conquered areas and seized food supplies, and levied requisitions on countries he controlled, such as Italy.

# Long-term impact

## France

The Revolution meant an end to arbitrary royal rule and held out the promise of rule by law under a constitutional order, but it did not rule out a monarch. Napoleon as emperor set up a constitutional system (although he remained in full control), and the restored Bourbons were forced to go along with one. After the abdication of Napoleon III in 1871, the monarchists probably had a voting majority, but they were so factionalised they could not agree on who should be king, and instead the French Third Republic was launched with a deep commitment to upholding the ideals of the Revolution.[240][241] The conservative Catholic enemies of the Revolution came to power in Vichy France (1940–44), and tried with little success to undo its heritage, but they kept it a republic. Vichy denied the principle of equality and tried to replace the Revolutionary watchwords "Liberty, Equality, Fraternity" with "Work, Family, and Fatherland." However, there were no efforts by the Bourbons, Vichy or anyone else to restore the privileges that had been stripped away from the nobility in 1789. France permanently became a society of equals under the law.

## Outside France

According to Daron Acemoglu, Davide Cantoni, Simon Johnson, and James A. Robinson the French Revolution had long-term effects in Europe. They suggest that "areas that were occupied by the French and that underwent radical institutional reform experienced more rapid urbanization and economic growth, especially after 1850. There is no evidence of a negative effect of French invasion."

## Germany

German reaction to the Revolution swung from favourable to antagonistic. At first it brought liberal and democratic ideas, the end of gilds, serfdom and the Jewish ghetto. It brought economic freedoms and agrarian and legal reform. Above all the antagonism helped stimulate and shape German nationalism.

## United States

The Revolution deeply polarised American politics, and this polarisation led to the creation of the First Party System. In 1793, as war broke out in Europe, the Republican Party led by Thomas Jefferson favoured France and pointed to the 1778 treaty that was still in effect. George Washington and his unanimous cabinet, including Jefferson, decided that the treaty did not bind the United States to enter the war. Washington proclaimed neutrality instead.[262] Under President John Adams, a Federalist, an undeclared naval war took place with France from 1798 until 1799, often called the "Quasi War". Jefferson became president in 1801, but was hostile to Napoleon as a dictator and emperor. However, the two entered negotiations over the Louisiana Territory and agreed to the Louisiana Purchase in 1803, an acquisition that substantially increased the size of the United States.


# Historiography

Some historians argue that the French people underwent a fundamental transformation in self-identity, evidenced by the elimination of privileges and their replacement by rights as well as the growing decline in social deference that highlighted the principle of equality throughout the Revolution.[274] The Revolution represented the most significant and dramatic challenge to political absolutism up to that point in history and spread democratic ideals throughout Europe and ultimately the world.[275] Throughout the 19th century, the revolution was heavily analysed by economists and political scientists, who saw the class nature of the revolution as a fundamental aspect in understanding human social evolution itself. This, combined with the egalitarian values introduced by the revolution, gave rise to a classless and co-operative model for society called "socialism" which profoundly influenced future revolutions in France and around the world.
