*Communism vs. Fascism, Wikipedia*

While communism is a system based around a theory of economic equality and advocates for a classless society, fascism is a nationalistic, top­down system with rigid class roles that is ruled by an all­powerful dictator. Both communism and fascism originated in Europe and gained popularity in the early to mid 20th century.

# Comparison

## Philosophy

C: From each according to his ability, to each according to his needs. Free­access to the articles of consumption is made possible by advances in technology that allow for super­ abundance.

F: The state must gain glory through constant conquest and war. The past was glorious, and that the State can be renewed. The individual has no value outside of his or her role in promoting the glory of the State. Philosophies varied by country.

## Ideas

C: All people are the same and therefore classes make no sense. The government should own all means of production and land and also everything else. People should work for the government and the collective output should be redistributed equally.

F: Union between businesses and the State, with the state telling the business what to do, with nominally private ownership. Corporatism in Italy, National Socialism in Germany. Central planning of National economy. Redistribution of wealth (Nazi).

## Key Elements

C: Centralized government, planned economy, dictatorship of the "proletariat", common ownership of the tools of production, no private property. equality between genders and all people, international focus. Usually anti­ democratic with a 1­party system.

F: Actual idealism, centralized government, social Darwinism, planned economy, anti­ democratic, meritocratic, extreme nationalism, militarism, racism (Nazism). Traditional and/or exaggerated gender roles. One party system.

## Definition

C: International theory or system of social organization based on the holding of all property in common, with actual ownership ascribed to the community or state. Rejection of free markets and extreme distrust of Capitalism in any form.

F: An extremely nationalistic, authoritarian state usually led by one person at the head of one party. No democratic election of representatives. No free market. No individualism or individual glory. The State controls of the press and all other media.

## Political System

C: A communist society is stateless, classless and is governed directly by the people. This however has never been practised.

F: One charismatic leader has absolute authority. Often the symbol of the state. Advisers to Government are generally picked by merit rather than election. Cronyism common.

## Private Property

C: Abolished. The concept of property is negated and replaced with the concept of commons and ownership with "usership".

F: Nominally permitted. Contingent upon service, obedience, or usefulness to the State.

## Economic Coordination

C: Economic planning coordinates all decisions regarding investment, production and resource allocation. Planning is done in terms of physical units instead of money.

F: Businesses are nominally privately owned; the State dictates outputs and investments. Planning is based on projected labor output rather than money.

## Ownership Structure

C: The means of production are commonly­owned, meaning no entity or individual owns productive property. Importance is ascribed to "usership" over "ownership".

F: The means of production are nominally privately owned but directed by the State. Private ownership of business is contingent upon submission to the direction and interests of the State.

## Social Structure

C: All class distinctions are eliminated. A society in which everyone is both the owners of the means of production and their own employees.

F: Strict class structure believed necessary to prevent chaos (Italian Fascist). All class distinctions are eliminated (German Nazi). Nazism believes in a “superior” race. Italian Fascism was not racist in doctrine originally.

## Economic System

C: The means of production are held in common, negating the concept of ownership in capital goods. Production is organized to provide for human needs directly without any use for money. Communism is predicated upon a condition of material abundance.

F: Autarky (national self­sufficiency). Keynesian (mostly). Large public works, deficit spending. Anti trade union and syndicalism. Strongly against international financial markets and usury.

## Religion

C: Abolished ­ all religious and metaphysics is rejected. Engels and Lenin agreed that religion was a drug or “spiritual booze” and must be combated. To them, atheism put into practice meant a “forcible overthrow of all existing social conditions.

F: Fascism is a civic religion: citizens worship the state through nationalism. The state only supports religious organizations that are nationally/historically tied to that state; e.g., the Iron Guard in Romania supported the Romanian Orthodox church.

## Free Choice

C: Either the collective "vote" or the state's rulers make economic and political decisions for everyone else. In practice, rallies, force, propaganda etc. are used by the rulers to control the populace.

F: The individual is considered meaningless; they must submit to the decisions of the leadership. Traditional gender roles are upheld and/or exaggerated.

## Way of Change

C: Government in a Communist­ state is the agent of change rather than any market or desire on the part of consumers. Change by government can be swift or slow, depending on change in ideology or even whim.

F: Government in a fascist state is the agent of change rather than any market or desire on the part of consumers. Change by government can be swift or slow, depending on a change in labor output or even at the whim of the dictator.

## Examples

C: Ideally, there is no leader; the people govern directly. This has never been actually practiced, and has just used a one­party system. Examples 0f Communist states are the erstwhile Soviet Union, Cuba and North Korea.

F: Fascist governments are generally headed by one person: a dictator. This is not an aberration of doctrine, in fact it is an important component of it.

## Discrimination

C: In theory, all members of the state are considered equal to one another.

F: Belief in one superior race (Nazism). Belief in a superior nation (Fascism & Nazism). Gender (F & N). Mental or physical handicaps. Mental illness. Alcoholics. Homosexuals. Roma. Jews (Nazi). Ideological and political opposition, trade unions (F&N).

## Earliest Remnants

C: Theorized by Karl Marx and Frederick Engels in the mid­19th century as an alternative to capitalism and feudalism, communism was not tried out until after the revolution in Russia in the early 1910s.

F: The Roman Empire, which could be argued was a fascist entity. The earliest fascist theories were based on examples left behind by the Roman Empire.

## Means of control

C: Theoretically there is no state control.

F: Fascism employs direct force (secret police, government intimidation, concentration camps, and murder), propaganda (enabled by State­directed, heavily­censored media), rallies, etc.

## View of war

C: Communists believe that war is good for the economy by spurring production, but should be avoided.

F: War is good for the morale of the nation and therefore good for the State. Through the conquest of war, the State can attain glory. The Nation State is bolstered through subjugation of inferior nations. War has no negative effect on the economy.

## View of the world

C: Communism is an international movement; Communists in one country see themselves in solidarity with Communists in other countries. Communists distrust Nationalistic nations and leaders. Communists strongly distrust "big business."

F: Fascists are ultra­nationalists who identify strongly with other Nationalistic nations and leaders. Fascists distrust internationalism and rarely abide by international agreements. Fascists do not believe in the concept of international law.


# What are Communism and Fascism?

As a socioeconomic system, communism considers all property to be communal — tha is, owned by the community or by the state. This system also stresses the importance of a "classless" society, where there are no differences between the wealthy and the working classes, between men and women, or between races. While Marxist communism is the most common form of communism, there is also non­Marxist communism.

As is evident by multiple definitions of fascism, there is considerable variations in what social scientists call fascism. Nevertheless we will attempt to describe what it generally means. Fascism is also a political and economic system, but its focus is on the nation state, as ruled by a dictator, and on rigid social structure. Under fascism, hyper­ masculinity, youth, and even violence and militarism are held in high regard. Any "outside" idea that conflicts with the nation state is undesirable; as such, fascism often shuns conservatism, liberalism, democracy, and communism, alike, and is also generally hostile toward equality for women and different races and people.

## Communist Philosophy

Communism can be traced back to Thomas More, a prominent English Catholic who wrote about a society based around common ownership of property in Utopia in 1516. The origin of communism is most commonly associated with Karl Marx and Friedrich Engels in their 1848 book The Communist Manifesto. Marx was a critic of the Industrial Revolution and felt working classes were taken advantage of under capitalism.

In the book, Marx and Engels propose a communist system, wherein property is communally owned by an atheistic, classless society, thus eliminating differences between workers (proletariat) and wealthy elites (bourgeosie). They argue that achieving this state would eliminate nearly all societal problems caused by inequality and exploitation and place mankind on a higher level of progress. However, Marx and Engels never describe how such a society can be created, leaving essentially a blank slate for others to fill in.

From 1917 to 1924, Vladimir Lenin led the Communist Party in Russia, establishing the structure and direction the ideology would take. His vision of a global communist state was little more than an extension of Marx's "worker's revolution." To that end, Lenin sought to influence communism and its development throughout Europe. However, internal party struggles for power led to the dismissal or exile of key leaders, such as Leon Trotsky, and left Russia's communist regime at the mercy of opportunism upon Lenin's death. Into that vacuum stepped Joseph Stalin, who eschewed theoretical matters in favor of solidifying power.

The development of communism around the world was influenced after the 1930s by economic issues, especially in post­colonial territories, such as parts of Africa and Asia, and in politically unstable regions in Central and South America. Although Russia tried to take a leadership role through economic and military influence, as did China in Asia, the lack of true economic success has thus far limited the gains made by communism.

## Fascist Philosophy

Fascism is based around the glory of the nation state. Its origins can be traced to the nationalism movements of the late 19th century. Two Frenchmen, Charles Maurras and Georges Sorel, wrote about integral nationalism and radical syndicalist action as ways to create a more organic and prosperous society. These writings influenced Italian Enrico Corradini, who postulated a rationalist­syndicalist movement, led by aristocracy and anti­democratic forces. Combined with futurism, an early 20th century doctrine of forcing change (even resorting to violence), the seeds of fascism took root in Italy at the beginning of World War I. However, fascism formed in different ways in each country, succeeding (Italy, Germany, Spain, briefly in Portugal) or failing (France) in its own way.

Despite the different development processes, fascist regimes do share several characteristics in common, including extreme militaristic nationalism, opposition to parliamentary democracy, conservative economic policy that favors the wealthy, contempt for political and cultural liberalism, a belief in natural social hierarchy and the rule of elites, and the desire to create a Volksgemeinschaft (German for “people’s community”), in which individual interests are subordinated to the good of the nation. Two other characteristics emerged in practice: the binding of corporate interests to "the national will" and outright control of media leading to widespread propaganda.


# Social Structure and Class Hierarchies

Communists inspired by The Communist Manifesto believe class hierarchies must be abolished by the state seizing control of private property and industry, thereby abolishing the capitalist class. Likewise, they are often against other social constructs, such as rigid gender roles.

Contrary to communism's goal of a classless society, fascism upholds a strict class structure, ensuring that every member of society has a specific, unchangeable role. Often in fascist societies women are restricted to the home and child­rearing, and a certain racial or ethnic group is considered superior, with national and ethnic unity encouraged at the expense of individuality and diversity. For example, Hitler's fascist regime glorified the Aryan race and called for the extermination of Jews, Gypsies, and Poles during World War II. Moreover, other groups with actual or perceived differences, including homosexuals, the disabled, and communists, were targeted during the Holocaust.


# Political System

Both fascism and communism are against the democratic process but with some differences. Fascism looks down upon parliamentary democracy. Fascist leaders like Hitler and Mussolini participated in electoral politics before coming to power. But after seizing power, fascist leaders tended to abolish political parties, oppose universal suffrage and became dictators and rulers for life.

In communist countries, democracy might be the path to power (a communist majority is elected), but single­party rule is the prevailing tendency. Although elections may continue to be held, a country's Communist Party is often the only body eligible to place candidates on the ballot. Leadership in the party is usually based on seniority rather than merit. A central ruling committee within the party governs debate (allowing or disallowing it) and essentially establishes the "line" the party follows. Although communism preaches inclusion, the tendency is toward elitism and concentration of power within the party leadership alone.


# Economic System

Communism is based on the equal distribution of wealth. The tenet of Marxian communism was "From each according to his ability, to each according to his need." Everyone in society receives an equal share of the benefits derived from labor, e.g., food and money. In order to ensure that everyone receives an equal amount, all means of production are controlled by the state.

Fascism allows for private enterprise, but its economic system is focused entirely on strengthening and glorifying the state. Both Fascist Italy and Nazi Germany aimed for self­sufficiency, so that each country could survive entirely without trade with other nations. See fascist corporatism.


# Individual Rights

In both communism and fascism, individual choice or preference matter less than society as a whole. In communism, religion and private property are both abolished, the government controls all labor and wealth, and individual choices such as one's job or education tend to be dictated by the government. While private property is permitted in fascism, most other choices are also controlled to increase the strength of the state.


# History of Fascism and Communism in Practice

The first real­world example of Marxist Communism was in Russia in 1917, when the Bolshevik Party seized control in the October Revolution. Russian leaders of the time, such as Vladimir Lenin and Leon Trotsky, were seen as examples worthy of emulation in other countries, spearheading the growth of communist parties throughout Europe. In reaction to what was seen as a growing communist menace, fascism appeared in Italy and Germany.

Modern Fascism originated in Italy in the 1920s, when Benito Mussolini gained control and coined the term “fascism” to describe his form of government. The focus was on nationalism rather than inclusion in a "global communist state" that many feared would create puppets of Russia's communist party. To keep workers from seizing control of their workplaces, corporations and key economic engines were taken over by the government (nationalized), uniting business and government into monopolies. Fascism then spread throughout Europe, including to Germany beginning in 1933 with the Nazis, and Portugal in 1934.

Communism spread throughout Europe and Asia, establishing a constant presence in the political debates of leading countries such as England, France, and the U.S. In China, the rise of the Communist Party, led by Mao Zedong, was the result of a civil war. The "fall of China" to communism caused major concern in Europe and the U.S., one that was placed on hold with the outbreak of World War II.

After the war, the Soviet Union was formed, forcibly adding several countries to its communist coalition. China became active in its Asian sphere of influence, backing North Korea against U.S.­supported South Korea in the Korean War, eventually helping its ally to remain a communist nation. Vietnam was also a test case in a war that had the U.S. playing the role of "defender of democracy" against the specter of a communist­based "domino theory." The U.S. lost this war, and neighboring countries, Laos and Cambodia, established communist governments.

Fascism was defeated in World War II, but Spain, under Francisco Franco, continued a fascist regime until the 1970s. Other fascist regimes emerged in South America and Africa, but failed to remain in power for long. 

The spread of communism, though extensive, was probably less successful than it could have been due to the lack of collaboration between the Soviet Union and China, each espousing a different "true communist" philosophy. The collapse of the Soviet Union in 1989 and the economic depression of China that lasted over 50 years, added to the failure of other communist governments, led to a large­scale abandonment of communism as a political theory.


# Modern Examples

Modern Examples As of 2015, China, Cuba, and North Korea are the most prominent of about a dozen communist countries (out of over 210 in the world). However, China has adopted basic capitalist practices to develop the world's fastest­growing and largest economy, Cuba has agreed to normalize relations with the U.S. (including economic development), and the "theocratic communism" of North Korea, where the Kim family is seen as god­like, may end as discussions for reunification with South Korea are in the works. No countries are currently operate under a fascist philosophy, but neo­fascists (or neo­ Nazis) exist in many countries, including the U.S.


#Communism and Fascism in Capitalistic Systems

Many people consider capitalism, communism, and fascism to be entirely separate systems, but there are shared elements. In capitalist systems, the presence of "public domain" works, to be shared by all, follows a communist principle, as does a system of public education. Employee ­owned companies follow a communist model in giving workers the same rights and privileges as owners.

Lobbying is a fascist trait in capitalist systems, especially the U.S., for it allows, and even encourages, business wealth to influence legislation. This allows corporations to cement alliances with government power and supersede citizen's rights. An extension of this principle is seen in the Citizens United decision by the Supreme Court, which grants "free speech" rights to corporations.
