*Glorious Revolution, Wikipedia*

The Glorious Revolution, also called the Revolution of 1688, was the overthrow of King James II of England (James VII of Scotland) by a union of English politicians with the Dutch stadtholder William III, Prince of Orange, who was James's nephew and son-in-law. William's successful invasion of England with a Dutch fleet and army led to his ascension to the throne as William III of England jointly with his wife, Mary II, James's daughter, after the Declaration of Right, leading to the Bill of Rights 1689.

King James's policies of religious tolerance after 1685 met with increasing opposition from members of leading political circles, who were troubled by the King's Catholicism and his close ties with France. The crisis facing the King came to a head in 1688, with the birth of his son, James, on 10 June (Julian calendar).

After consolidating political and financial support, William crossed the North Sea and English Channel with a large invasion fleet in November 1688, landing at Torbay. After only two minor clashes between the two opposing armies in England, and anti-Catholic riots in several towns, James's regime collapsed, largely because of a lack of resolve shown by the king.

Following a defeat of his forces at the Battle of Reading on 9 December 1688, James and his wife Mary fled England; James, however, returned to London for a two-week period that culminated in his final departure for France on 23 December 1688. By threatening to withdraw his troops, William, in February 1689 (New Style Julian calendar),[a] convinced a newly chosen Convention Parliament to make him and his wife joint monarchs.

The Revolution permanently ended any chance of Catholicism becoming re-established in England. For British Catholics its effects were disastrous both socially and politically: For over a century Catholics were denied the right to vote and sit in the Westminster Parliament; they were also denied commissions in the army, and the monarch was forbidden to be Catholic or to marry a Catholic, this latter prohibition remaining in force until 2015. The Revolution led to limited tolerance for Nonconformist Protestants, although it would be some time before they had full political rights. It has been argued, mainly by Whig historians, that James's overthrow began modern English parliamentary democracy: the Bill of Rights 1689 has become one of the most important documents in the political history of Britain and never since has the monarch held absolute power.

# Background

## The destruction of James' political base

Stuart ideology was based on the idea of a centralised state, run by a monarch whose authority came from God and was not subject to 'interference' from Parliament, bishops or elders alike.[17] James' predecessors all ruled without Parliament for long periods; while offering tolerance measures to religious minorities for political purposes was also a well-tried approach. While in exile in 1654, James initiated negotiations with English Dissenters and in 1664 granted freedom of worship to the newly-founded Province of New York. However, tolerance did not extend to political affairs; these measures were passed using arbitrary powers and were often intended to sustain it, by granting rights that could be withdrawn as and when the king decided.

In May 1686, James decided to obtain from the English courts of the common law a ruling that affirmed his power to dispense with Acts of Parliament. He dismissed judges who disagreed with him on this matter as well as the Solicitor General Heneage Finch. Eleven out of the twelve judges ruled in favour of dispensing power.

## William seeks English commitment to an invasion

The Seven went on to claim that "much the greatest part of the nobility and gentry" were dissatisfied and would rally to William, and that James's army "would be very much divided among themselves; many of the officers being so discontented that they continue in their service only for a subsistence ... and very many of the common soldiers do daily shew such an aversion to the Popish religion, that there is the greatest probability imaginable of great numbers of deserters ... and amongst the seamen, it is almost certain, there is not one in ten who would do them any service in such a war".[44] The Seven believed that the situation would be much worse before another year due to James's plans to remodel the army by the means of a packed Parliament or, should the parliamentary route fail, through violent means which would "prevent all possible means of relieving ourselves".[45] The Seven also promised to rally to William upon his landing in England and would "do all that lies in our power to prepare others to be in as much readiness as such an action is capable of".

## The final decision to invade is taken

On 29 September the States of Holland, the government of the most important Dutch province, fearing a French-English alliance, gathered in secret session and approved the operation, agreeing to make the English "King and Nation live in a good relation, and useful to their friends and allies, and especially to this State".


# Invasion

## Embarkation of the army and the Declaration of The Hague

On 30 September/10 October (Julian/Gregorian calendars) William issued the Declaration of The Hague (actually written by Fagel), of which 60,000 copies of the English translation by Gilbert Burnet were distributed after the landing in England,[60][61] in which he assured that his only aim was to maintain the Protestant religion, install a free parliament and investigate the legitimacy of the Prince of Wales.

The Test Acts were a series of English penal laws that served as a religious test for public office and imposed various civil disabilities on Roman Catholics and nonconformists.

William also condemned James's attempt to repeal the Test Acts and the penal laws through pressuring individuals and waging an assault on parliamentary boroughs, as well as his purging of the judiciary. James's attempt to pack Parliament was in danger of removing "the last and great remedy for all those evils". "Therefore", William continued, "we have thought fit to go over to England, and to carry over with us a force sufficient, by the blessing of God, to defend us from the violence of those evil Counsellors ... this our Expedition is intended for no other design, but to have, a free and lawful Parliament assembled as soon as is possible".


# The collapse of James's rule

## Departure of King and Queen

In the night of 9/10 December, the Queen and the Prince of Wales fled for France. The next day saw James's attempt to escape, the King dropping the Great Seal in the Thames along the way, as no lawful Parliament could be summoned without it.[95] However, he was captured on 11 December by fishermen in Faversham opposite Sheerness, the town on the Isle of Sheppey. On the same day, 27 Lords Spiritual and Temporal, forming a provisional government, decided to ask William to restore order but at the same time asked the king to return to London to reach an agreement with his son-in-law. It was presided over initially by William Sancroft, Archbishop of Canterbury and, after it was learned that James was still in England, by George Savile, 1st Marquess of Halifax.[96][93] On the night of 11 December there were riots and lootings of the houses of Catholics and several foreign embassies of Catholic countries in London. The following night a mass panic gripped London during what was later termed the Irish night. False rumours of an impending Irish army attack on London circulated in the capital, and a mob of over 100,000 assembled ready to defend the city.

The lax guard on James and the decision to allow him so near the coast indicate that William may have hoped that a successful flight would avoid the difficulty of deciding what to do with him, especially with the memory of the execution of Charles I still strong. By fleeing, James ultimately helped resolve the awkward question of whether he was still the legal king or not, having created according to many a situation of interregnum.


# William and Mary made joint monarchs

## The Bill of Rights

The proposal to draw up a statement of rights and liberties and James's invasion of them was first made on 29 January in the Commons, with members arguing that the House "can not answer it to the nation or Prince of Orange till we declare what are the rights invaded" and that William "cannot take it ill if we make conditions to secure ourselves for the future" to "do justice to those who sent us hither". On 2 February a committee specially convened reported to the Commons 23 Heads of Grievances, which the Commons approved and added some of their own. However, on 4 February the Commons decided to instruct the committee to differentiate between "such of the general heads, as are introductory of new laws, from those that are declaratory of ancient rights". On 7 February the Commons approved this revised Declaration of Right, and on 8 February instructed the committee to put into a single text the Declaration (with the heads which were "introductory of new laws" removed), the resolution of 28 January and the Lords' proposal for a revised oath of allegiance. It passed the Commons without division.

The Coronation Oath Act 1688 had provided a new coronation oath, whereby the monarchs were to "solemnly promise and swear to govern the people of this kingdom of England, and the dominions thereunto belonging, according to the statutes in parliament agreed on, and the laws and customs of the same". They were also to maintain the laws of God, the true profession of the Gospel, and the Protestant Reformed faith established by law.


# Jacobite uprisings

James had cultivated support on the fringes of his Three Kingdoms – in Catholic Ireland and the Highlands of Scotland. Supporters of James, known as Jacobites, were prepared to resist what they saw as an illegal coup by force of arms.

In Ireland, Richard Talbot, 1st Earl of Tyrconnell led local Catholics, who had been discriminated against by previous English monarchs, in the conquest of all the fortified places in the kingdom except Derry, and so held the Kingdom for James. James himself landed in Ireland with 6,000 French troops to try to regain the throne in the Williamite War in Ireland. The war raged from 1689 to 1691. James fled Ireland following his defeat at the Battle of the Boyne in 1690, but Jacobite resistance was not ended until after the battle of Aughrim in 1691, when over half of their army was killed or taken prisoner.


# "Dutch invasion" hypothesis

After being revisited by historians in 1988—the third centennial of the event—several researchers have argued that the "revolution" was actually a successful Dutch invasion of Britain.[113] The events were unusual because the establishment of a constitutional monarchy (a de facto republic, see Coronation Oath Act 1688) and Bill of Rights meant that the apparently invading monarchs, legitimate heirs to the throne, were prepared to govern with the English Parliament. It is difficult to classify the entire proceedings of 1687–89 but it can be seen that the events occurred in three phases: conspiracy, invasion by Dutch forces and "Glorious Revolution". It has been argued that the invasion aspect had been downplayed as a result of a combination of British pride and successful Dutch propaganda, trying to depict the course of events as a largely internal English affair.

As the invitation was initiated by figures who had little influence themselves, the legacy of the Glorious Revolution has been described as a successful propaganda act by William to cover up and justify his successful invasion.[115] The claim that William was fighting for the Protestant cause in England was used to great effect to disguise the military, cultural and political impact that the Dutch regime had on England at the time.


# Legacy

The Glorious Revolution of 1688 is considered by some as being one of the most important events in the long evolution of the respective powers of Parliament and the Crown in England. With the passage of the Bill of Rights, it stamped out once and for all any possibility of a Catholic monarchy, and ended moves towards absolute monarchy in the British kingdoms by circumscribing the monarch's powers. These powers were greatly restricted; he or she could no longer suspend laws, levy taxes, make royal appointments, or maintain a standing army during peacetime without Parliament's permission – to this day the Army is known as the "British Army" not the "Royal Army" as it is, in some sense, Parliament's Army and not that of the King.

Since 1689, government under a system of constitutional monarchy in England, and later the United Kingdom, has been uninterrupted. Since then, Parliament's power has steadily increased while the Crown's has steadily declined. Unlike in the English civil war of the mid-seventeenth century, the "Glorious Revolution" did not involve the masses of ordinary people in England (the majority of the bloodshed occurred in Ireland). This fact has led many historians, including Stephen Webb,[121] to suggest that, in England at least, the events more closely resemble a coup d'état than a social revolution.[h] This view of events does not contradict what was originally meant by "revolution": the coming round of an old system of values in a circular motion, back to its original position, as Britain's constitution was reasserted, rather than formed anew.
