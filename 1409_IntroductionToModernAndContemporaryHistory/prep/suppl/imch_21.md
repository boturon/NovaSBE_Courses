*Crying Wolf: Democracies in Crisis, Boston Review*

Runciman identifies four challenges to democracy: war, finance run amok, environmental hazard, and international rivalry. The United States has faced all of these in the past decade, from the Iraq and Afghanistan wars, the Great Recession, the failure to respond to climate change, and the threat of China’s growth. Are these simply the next crises in the cycle to which we will adapt? Or is the U.S. and other democracies suffering from their own success, stuck in a rut and unable to learn from past experience? I corresponded with David Runciman recently about the securities and insecurities of democratic states.

I mean the ways that democracies like the U.S. have in the past suspended the normal rules in a crisis (above all in wartime) without abandoning democratic expectations of popular scrutiny and ultimate accountability. Autocracies can’t do this in reverse: they can’t experiment with democracy while retaining the expectation that basic autocratic controls are still intact. Once you open the door to democracy you can’t control what comes out. The current Chinese political system, for all its adaptability in other ways (being pragmatic, non-ideological, technocratic), is not adaptable in this way.

This adaptability does depend on a basic level of material and political security. Relatively long-standing democracies can do it. New democracies find it much harder. The re- introduction of military control in Egypt, so soon after the transition to democracy, is unlikely to be an experiment in democratic adaptability.

You ask what the limits of this ability to play with autocracy are. One limit is this: it needs to happen in the open and to be tied to a publicly acknowledged crisis.

In the 1930s democracy was menaced by fascism and communism. Now it is menaced by the lack of alternatives.

The present crisis is not a rerun of the 1930s. There are two reasons why. First, we have the example of the 1930s to draw on, which has helped to prevent a repeat of the worst mistakes of the Great Depression. Second, we are so much richer than we were back then. As a result the current crisis hasn't got bad enough to produce radical change. In the 1930s democracy was menaced by fascism and communism. Now it is menaced by the lack of alternatives (almost no one in the West is pushing for Chinese state capitalism as the solution to our problems). It seems very unlikely that Western democracy will fall apart. But that makes it more likely that it will get stuck in a rut.

This is what I call the confidence trap. People need to have confidence in democracy for it to work. They have to think that nothing is quite as bad as it seems since at any given moment democracy usually looks like it’s in a real mess.
