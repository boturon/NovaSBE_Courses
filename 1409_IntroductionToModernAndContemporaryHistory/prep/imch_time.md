#1300s
1348–1350 - Black Death

#1600s
1688–1689 - Glorious Revolution

# 1700s
1756–1763 - Seven Years' War
1765–1783 - American Revolution
1789–1799 - French Revolution

# 1900s
1870–1913 - First Phase of Globalization
1911–1912 - Xinhai Revolution
1914–1918 - World War I
1917–1920 - Communist Revolutionary Wave
1939–1945 - World War II
1947–1991 - Cold War
1970–.... - Second Phase of Globalization
